//
//  ViewController.h
//  TEST
//
//  Created by hemanth kp on 1/5/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AdWhirlDelegateProtocol.h"

@class AdWhirlView;

typedef enum
{
    kSearchTypeHospital = 300,
	kSearchTypePharmacy,
	kSearchTypeHeart,
	kSearchTypeEye,
    kSearchTypeGynecology,
    kSearchTypeDental,
    kSearchTypeInternalMedicine,
    kSearchTypeChild,
    kSearchTypeDiagonosticCenter,
	kSearchTypeBronchology,
    kSearchTypeUrology,
    kSearchTypeNephrology,
    kSearchTypeNeurology,
	kSearchTypeBone,
    kSearchTypeDiabetese,
    kSearchTypeCancer,
    kSearchTypeHair,
	kSearchTypeBlood,
    kSearchTypeENT,
    kSearchTypeEndocrinology,
    kSearchTypeAnesthesia,
    kSearchTypeCosmetology,
	kSearchTypeSexology,
    kSearchTypePsychology,
    kSearchTypeChiropractic,
    kSearchTypePhysiotherapy,
    kSearchTypeGas,
	kSearchTypeIvfCenter,
	kSearchTypeDiet,
	kSearchTypeRehabilitationCenters,
	kSearchTypeAyurveda,
	kSearchTypeHomeopathy,
	kSearchTypeNaturopathyYoga,
	kSearchTypeVeterinary,
	kSearchTypeRestrooms,
	kMailTo
}SearchType;

@interface ViewController : UIViewController <UIScrollViewDelegate,AdWhirlDelegate,MFMailComposeViewControllerDelegate>
{
	UIImage *img;
	NSString *imgName;
	AdWhirlView *adView;
	SearchType searchFor;
    UIScrollView * homeScrollView;
    UIPageControl * pageControl;
    BOOL pageControlUsed;
    NSMutableArray * viewArray;
	
	NSString *errStr;
}

@property (nonatomic, retain) UIImage *img;
@property (nonatomic, retain) NSString *imgName;
@property (nonatomic) SearchType searchFor;

@property (nonatomic, retain) UIScrollView * homeScrollView;
@property (nonatomic, retain) UIPageControl * pageControl;
@property (nonatomic, assign) BOOL pageControlUsed;
@property (nonatomic, retain) NSMutableArray * viewArray;
@property (nonatomic,retain) AdWhirlView *adView;

- (void)adjustAdSize;
- (void)adjustLayoutToOrientation:(UIInterfaceOrientation)newOrientation;

-(void) changedPageControl: (id) sender;
- (void)loadScrollViewWithPage:(int)page;


- (IBAction) startSearchForOption:(UIResponder *)sender;
- (IBAction) toggleSearchProviders:(id)sender;

@end
