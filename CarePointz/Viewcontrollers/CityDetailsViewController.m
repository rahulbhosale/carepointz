//
//  CityDetailsViewController.m
//  TEST
//
//  Created by hemanth kp on 3/15/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import "CityDetailsViewController.h"
#import <safeObjC.h>
#import "CarePointzAppDelegate.h"
#import "SearchResult.h"
#import "MapViewController.h"
#import "Settings.h"

NSURL *selectedURL;

@implementation CityDetailsViewController

@synthesize name;
@synthesize address;
@synthesize phoneNum;
@synthesize internationPhoneNum;
@synthesize website;
@synthesize rating;
@synthesize selectedResult;
@synthesize adView;
@synthesize parentVC;

-(void)dealloc
{
	self.selectedResult = nil;
	self.adView.delegate = nil;
	self.adView = nil;
	self.parentVC = nil;
	[super dealloc];
}

- (BOOL) cacheData: (id)data
{
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:placeID];
	
	BOOL isSucc = [[NSFileManager defaultManager] createFileAtPath: folderPath
														  contents: data
														attributes: NULL];
	return isSucc;
}

- (NSDictionary *) loadFromCache
{
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:placeID];
	NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:folderPath];
	return dict;
}

-(void) connectionManager :(ConnectionManager *)connectionManager didGetData:(NSData *) data
{
	NSError *jsonError = nil;
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
	NSDictionary *resultsDict = [[CJSONDeserializer deserializer] deserialize:data error:&jsonError];
	NSString *status = [resultsDict valueForKey:@"status"];
	if ([appDel isStatusError:status])
	{
		UIAlertView *noResultsAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"AlertTitle_Error", @"") message:@"There seems to be some problem with the server.. Please try again later!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[noResultsAlert show];
		[noResultsAlert release];
	}
	else
	{
		NSDictionary *resultDict = [resultsDict valueForKey:@"result"];
		NSMutableString *postalAddress = [[NSMutableString alloc] initWithString:[resultDict valueForKey:@"formatted_address"]];
		NSArray *addrs  = [resultDict objectForKey:@"address_components"];
		for (int i = 0; i < [addrs count]; i++) {
			NSDictionary *temDict = [addrs objectAtIndex:i];
			NSArray *typesArr = [temDict objectForKey:@"types"];
			if ([typesArr containsObject:@"postal_code"])
			{
				[postalAddress appendFormat:@", Pin: %@",[temDict objectForKey:@"long_name"]];
				break;
			}
		}
		if (postalAddress == nil|| [postalAddress length] <= 0)
		{
			self.address.text = @"Address not available.";
		}
		else
		{
			self.selectedResult.address = postalAddress;
			self.address.text = postalAddress;
		}
		
		if ([resultDict valueForKey:@"formatted_phone_number"] == nil|| [[resultDict valueForKey:@"formatted_phone_number"] length] <= 0)
		{
			self.phoneNum.text = @"Phone Number not available.";
		}
		else
		{
			self.selectedResult.phoneNumber = [resultDict valueForKey:@"formatted_phone_number"];
			self.phoneNum.text = [resultDict valueForKey:@"formatted_phone_number"];
		}
		if ([resultDict valueForKey:@"international_phone_number"] == nil|| [[resultDict valueForKey:@"international_phone_number"] length] <= 0)
		{
			self.internationPhoneNum.text = @"International number not available.";
		}
		else
		{
			self.selectedResult.internationalPhoneNumber = [resultDict valueForKey:@"international_phone_number"];
			self.internationPhoneNum.text = [resultDict valueForKey:@"international_phone_number"];
		}
		if ([resultDict valueForKey:@"website"] == nil|| [[resultDict valueForKey:@"website"] length] <= 0)
		{
			self.website.text = @"Website not available.";
		}
		else
		{
			self.selectedResult.websiteURLString = [resultDict valueForKey:@"website"];
			self.website.text = [resultDict valueForKey:@"website"];
		}
		
		NSNumber *ratings = [resultDict valueForKey:@"rating"];
		self.selectedResult.rating = [ratings floatValue];
		if (ratings == nil
			|| [ratings doubleValue] <= 0)
		{
			self.rating.text = @"No ratings available";
		}
		else
		{
			self.rating.text = [ratings stringValue];
		}
		
		[self.selectedResult updateDB];
		
		//Calculate the expected size based on the font and linebreak mode of your label
		CGSize maximumLabelSize = self.name.frame.size;
		[appDel dynamicallyResizeLabel:self.name withMaxSize:maximumLabelSize];
		
		maximumLabelSize = self.address.frame.size;
		[appDel dynamicallyResizeLabel:self.address withMaxSize:maximumLabelSize];
		
		maximumLabelSize = self.website.frame.size;
		[appDel dynamicallyResizeLabel:self.website withMaxSize:maximumLabelSize];
		
		//[self cacheData:resultDict];
	}
	[appDel hideActivityOnViewController:self];
}

-(void) connectionManager :(ConnectionManager *)connectionManager didFailWithError:(NSError *) error
{
	SafeLog(@"ERROR: %@",[error description]);
	if ([error code] ==  -1009)
	{
		UIAlertView *noResultsAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"AlertTitle_Connection_Error", @"") message:NSLocalizedString(@"No_Internet_Connection", @"") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[noResultsAlert show];
		[noResultsAlert release];
	}
	self.name.text = self.selectedResult.name;
	if (self.selectedResult.address == nil
		|| [self.selectedResult.address length] <= 0
		|| [self.selectedResult.address isEqualToString:@"(null)"])
	{
		self.address.text = self.selectedResult.vicinity;
	}
	else
	{
		self.address.text = self.selectedResult.address;
	}
	
	if (self.selectedResult.phoneNumber == nil
		|| [self.selectedResult.phoneNumber length] <= 0
		|| [self.selectedResult.phoneNumber isEqualToString:@"(null)"])
	{
		self.phoneNum.text = @"Phone Number not available.";
	}
	else
	{
		self.phoneNum.text = self.selectedResult.phoneNumber;
	}
	if (self.selectedResult.internationalPhoneNumber == nil
		|| [self.selectedResult.internationalPhoneNumber length] <= 0
		|| [self.selectedResult.internationalPhoneNumber isEqualToString:@"(null)"])
	{
		self.internationPhoneNum.text = @"International number not available.";
	}
	else
	{
		self.internationPhoneNum.text = self.selectedResult.internationalPhoneNumber;
	}
	if (self.selectedResult.websiteURLString == nil
		|| [self.selectedResult.websiteURLString length] <= 0
		|| [self.selectedResult.websiteURLString isEqualToString:@"(null)"])
	{
		self.website.text = @"Website not available.";
	}
	else
	{
		self.website.text = self.selectedResult.websiteURLString;
	}
	
	NSNumber *ratings = [NSNumber numberWithFloat:self.selectedResult.rating];
	if (ratings == nil
		|| [ratings floatValue] <= 0)
	{
		self.rating.text = @"No ratings available";
	}
	else
	{
		self.rating.text = [ratings stringValue];
	}
	
	
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
	
	//Calculate the expected size based on the font and linebreak mode of your label
	CGSize maximumLabelSize = self.name.frame.size;
	[appDel dynamicallyResizeLabel:self.name withMaxSize:maximumLabelSize];
	
	maximumLabelSize = self.address.frame.size;
	[appDel dynamicallyResizeLabel:self.address withMaxSize:maximumLabelSize];
	
	maximumLabelSize = self.website.frame.size;
	[appDel dynamicallyResizeLabel:self.website withMaxSize:maximumLabelSize];
	
	[appDel hideActivityOnViewController:self];
}

- (void)initData
{
	//NSDictionary *resultDict = [self loadFromCache];
	if (self.selectedResult.address == nil
		|| [self.selectedResult.address isEqualToString:@"(null)"])
	{
		CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
		appDel.searchDetailsURL = nil;
		appDel.searchDetailsURL = [NSMutableString stringWithCapacity:0];
		[appDel.searchDetailsURL appendString:MAPS_DETAILS_URL];
		[appDel.searchDetailsURL appendString:self.selectedResult.reference];
		NSString *str = [NSString stringWithString:appDel.searchDetailsURL];
		str = [str stringByAddingPercentEscapesUsingEncoding:NSStringEncodingConversionAllowLossy];
		SafeLog(@"%@",str);
		
		self.name.text = self.selectedResult.name;
		
		NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:str]];
		ConnectionManager *conn = [[ConnectionManager alloc] initWithRequest:req];
		conn.delegate = self;
		[conn startConnection];
		[appDel showActivityWithMessage:@"Fetching details of the place.." onViewController:self withSpinner:YES];
	}
	else
	{
		self.name.text = self.selectedResult.name;
		if (self.selectedResult.address == nil
			|| [self.selectedResult.address length] <= 0
			|| [self.selectedResult.address isEqualToString:@"(null)"])
		{
			self.address.text = @"Address not available.";
		}
		else
		{
			self.address.text = self.selectedResult.address;
		}
		
		if (self.selectedResult.phoneNumber == nil
			|| [self.selectedResult.phoneNumber length] <= 0
			|| [self.selectedResult.phoneNumber isEqualToString:@"(null)"])
		{
			self.phoneNum.text = @"Phone Number not available.";
		}
		else
		{
			self.phoneNum.text = self.selectedResult.phoneNumber;
		}
		if (self.selectedResult.internationalPhoneNumber == nil
			|| [self.selectedResult.internationalPhoneNumber length] <= 0
			|| [self.selectedResult.internationalPhoneNumber isEqualToString:@"(null)"])
		{
			self.internationPhoneNum.text = @"International number not available.";
		}
		else
		{
			self.internationPhoneNum.text = self.selectedResult.internationalPhoneNumber;
		}
		if (self.selectedResult.websiteURLString == nil
			|| [self.selectedResult.websiteURLString length] <= 0
			|| [self.selectedResult.websiteURLString isEqualToString:@"(null)"])
		{
			self.website.text = @"Website not available.";
		}
		else
		{
			self.website.text = self.selectedResult.websiteURLString;
		}
		
		NSNumber *ratings = [NSNumber numberWithFloat:self.selectedResult.rating];
		if (ratings == nil
			|| [ratings floatValue] <= 0.0)
		{
			self.rating.text = @"No ratings available";
		}
		else
		{
			self.rating.text = [ratings stringValue];
		}
		
		
		CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
		
		//Calculate the expected size based on the font and linebreak mode of your label
		CGSize maximumLabelSize = self.name.frame.size;
		[appDel dynamicallyResizeLabel:self.name withMaxSize:maximumLabelSize];
		
		maximumLabelSize = self.address.frame.size;
		[appDel dynamicallyResizeLabel:self.address withMaxSize:maximumLabelSize];
		
		maximumLabelSize = self.website.frame.size;
		[appDel dynamicallyResizeLabel:self.website withMaxSize:maximumLabelSize];
	}
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andSelectedResult:(SearchResult *) selResult
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
		self.title = @"Place Details";
		self.selectedResult = selResult;
		
		UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithTitle:@"Map" style:UIBarButtonItemStylePlain target:self action:@selector(popToMap)];
		self.navigationItem.rightBarButtonItem = barItem;
		ObjCRelease(barItem);
	}
	return self;
}

- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated
{
	selectedURL = nil;
	[self initData];
}

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
	[super viewDidLoad];
	/** AdWhril code **/
	self.adView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
	self.adView.autoresizingMask =
    UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
	CGRect adFrame = [adView frame];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
	adFrame.origin.y = screenBounds.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
	[adView setFrame:adFrame];
	if (getenv("ADWHIRL_FAKE_DARTS")) {
		// To make ad network selection deterministic
		const char *dartcstr = getenv("ADWHIRL_FAKE_DARTS");
		NSArray *rawdarts = [[NSString stringWithCString:dartcstr]
							 componentsSeparatedByString:@" "];
		NSMutableArray *darts
		= [[NSMutableArray alloc] initWithCapacity:[rawdarts count]];
		for (NSString *dartstr in rawdarts) {
			if ([dartstr length] == 0) {
				continue;
			}
			[darts addObject:[NSNumber numberWithDouble:[dartstr doubleValue]]];
		}
		self.adView.testDarts = darts;
	}
	
	UIDevice *device = [UIDevice currentDevice];
	if ([device respondsToSelector:@selector(isMultitaskingSupported)] &&
		[device isMultitaskingSupported]) {
		[[NSNotificationCenter defaultCenter]
		 addObserver:self
		 selector:@selector(enterForeground:)
		 name:UIApplicationDidBecomeActiveNotification
		 object:nil];
	}
	[self.view addSubview:self.adView];
	/** End: Adwhril Code **/
}


- (void)viewDidUnload
{
	[super viewDidUnload];
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown
            || interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = [touches anyObject];
	UIView *touchedView = [touch view];
	
	if ([touchedView isKindOfClass:[UILabel class]])
	{
		UILabel *lbl = (UILabel *)touchedView;
		if (lbl == self.phoneNum
			|| lbl == self.internationPhoneNum
			|| lbl == self.website)
		{
			[lbl setTextColor:[UIColor colorWithRed:255.0/255.0 green:80.0/255.0 blue:1.0/255.0 alpha:1]];//[UIColor greenColor]];
			UIDevice *dev = [UIDevice currentDevice];
			NSURL *finalURL = nil;
			// Invoke the website
			if (lbl == self.website)
			{
				if([lbl.text length] > 0
				   && ![lbl.text isEqualToString:@"Website not available."])
				{
					NSString *finalString = [NSString stringWithFormat:@"%@",lbl.text];
					if (finalString != nil && [finalString length] > 0 && ![finalString hasPrefix:@"http://"])
					{
						finalString = [NSString stringWithFormat:@"http://",finalString];
					}
					NSString *urlEncodedStr = [finalString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
					finalURL = [NSURL URLWithString:urlEncodedStr];
					urlEncodedStr = nil;
				}
			}			
			//Invoke call if available else show error.
			else
			{
				if([dev.model isEqualToString:@"iPhone"])
				{
					NSString *finalString;
					
					if([lbl.text length] > 0)
					{
						if (lbl == self.internationPhoneNum && ![lbl.text isEqualToString:@"International number not available."])
						{
							finalString = [NSString stringWithFormat:@"tel://%@",lbl.text]; 
						}
						else if (lbl == self.phoneNum && ![lbl.text isEqualToString:@"Phone Number not available."])
						{
							finalString = [NSString stringWithFormat:@"tel://00%@",lbl.text];
						}
						NSString *urlEncodedStr = [finalString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
						
						finalURL = [NSURL URLWithString:urlEncodedStr];
					}
					else
					{
					}
				}
				else
				{
					UIAlertView *al = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"AlertTitle_Error", @"")
																 message:NSLocalizedString(@"Feature_Not_Supported", @"")
																delegate:nil
													   cancelButtonTitle:@"OK"
													   otherButtonTitles:nil];
					[al show];
					[al release];
				}
			}
			if (finalURL != nil)
			{
				if ( ![[UIApplication sharedApplication] canOpenURL:finalURL] )
				{
					UIAlertView *al = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"AlertTitle_Error", @"")
																 message:NSLocalizedString(@"Feature_Not_Supported", @"")
																delegate:nil
													   cancelButtonTitle:@"OK"
													   otherButtonTitles:nil];
					[al show];
					[al release];
				}
				else
				{
					selectedURL = nil;
					selectedURL = [finalURL retain];
					UIAlertView *al = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"AlertTitle_Alert", @"")
																 message:NSLocalizedString(@"Leaving_App", @"")
																delegate:self
													   cancelButtonTitle:nil
													   otherButtonTitles:@"YES", @"NO",nil];
					[al show];
					[al release];
				}
			}
		}
	}
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
		[[UIApplication sharedApplication] openURL:selectedURL];
	}
}

- (void)popToMap
{
	[self.parentVC selectPoint:self.selectedResult];
	[self.navigationController popToViewController:self.parentVC animated:YES];
}

#pragma mark - AdWhril delegate methods
- (NSString *)admobPublisherID {
	return kAdModPublisherID;
}

- (NSString *)adWhirlApplicationKey {
	return kAdWhrilAppKey;
}

- (UIViewController *)viewControllerForPresentingModalView {
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[[UIApplication sharedApplication] delegate];
	return appDel.window.rootViewController;
}

- (NSURL *)adWhirlConfigURL {
	return [NSURL URLWithString:kConfigURL];
}

- (NSURL *)adWhirlImpMetricURL {
	return [NSURL URLWithString:kImpMetricURL];
}

- (NSURL *)adWhirlClickMetricURL {
	return [NSURL URLWithString:kClickMetricURL];
}

- (NSURL *)adWhirlCustomAdURL {
	return [NSURL URLWithString:kCustomAdURL];
}

- (void)adWhirlDidReceiveAd:(AdWhirlView *)adWhirlView {
	errStr = [NSString stringWithFormat:
			  @"Got ad from %@, size %@",
			  [adWhirlView mostRecentNetworkName],
			  NSStringFromCGSize([adWhirlView actualAdSize])];
	[self adjustAdSize];
}

- (void)adWhirlDidFailToReceiveAd:(AdWhirlView *)adWhirlView usingBackup:(BOOL)yesOrNo {
	errStr = [NSString stringWithFormat:
			  @"Failed to receive ad from %@, %@. Error: %@",
			  [adWhirlView mostRecentNetworkName],
			  yesOrNo? @"will use backup" : @"will NOT use backup",
			  adWhirlView.lastError == nil? @"no error" : [adWhirlView.lastError localizedDescription]];
	[self adjustAdSize];
}

- (void)adWhirlReceivedRequestForDeveloperToFufill:(AdWhirlView *)adWhirlView {
	UILabel *replacement = [[UILabel alloc] initWithFrame:kAdWhirlViewDefaultFrame];
	replacement.backgroundColor = [UIColor redColor];
	replacement.textColor = [UIColor whiteColor];
	replacement.textAlignment = UITextAlignmentCenter;
	replacement.text = @"Generic Notification";
	[adWhirlView replaceBannerViewWith:replacement];
	[replacement release];
	[self adjustAdSize];
	errStr = @"Generic Notification";
}

- (void)adWhirlReceivedNotificationAdsAreOff:(AdWhirlView *)adWhirlView {
	errStr = @"Ads are off";
}

- (void)adWhirlWillPresentFullScreenModal {
	SafeLog(@"SimpleView: will present full screen modal");
}

- (void)adWhirlDidDismissFullScreenModal {
	SafeLog(@"SimpleView: did dismiss full screen modal");
}

- (void)adWhirlDidReceiveConfig:(AdWhirlView *)adWhirlView {
	errStr = @"Received config. Requesting ad...";
}

- (BOOL)adWhirlTestMode {
	return NO;
}

- (CLLocation *)locationInfo {
	return [LocationManager sharedSingleton].userLocation;
}

#pragma mark event methods

- (void)performEvent {
	errStr = @"Event performed";
}

- (void)performEvent2:(AdWhirlView *)adWhirlView {
	UILabel *replacement = [[UILabel alloc] initWithFrame:kAdWhirlViewDefaultFrame];
	replacement.backgroundColor = [UIColor blackColor];
	replacement.textColor = [UIColor whiteColor];
	replacement.textAlignment = UITextAlignmentCenter;
	replacement.text = [NSString stringWithFormat:@"Event performed, view %x", adWhirlView];
	[adWhirlView replaceBannerViewWith:replacement];
	[replacement release];
	[self adjustAdSize];
	errStr = [NSString stringWithFormat:@"Event performed, view %x", adWhirlView];
}

#pragma mark multitasking methods

- (void)enterForeground:(NSNotification *)notification {
	AWLogDebug(@"SimpleView entering foreground");
	[[Settings sharedSettings] updateSettings];
	for (UIWindow* window in [UIApplication sharedApplication].windows) {
		NSArray* subviews = window.subviews;
		if ([subviews count] > 0)
			if ([[subviews objectAtIndex:0] isKindOfClass:[UIAlertView class]])
				[[subviews objectAtIndex:0] dismissWithClickedButtonIndex:5 animated:NO];
	}
	[self.adView updateAdWhirlConfig];
}

- (void)adjustLayoutToOrientation:(UIInterfaceOrientation)newOrientation {
	CGRect adFrame = [adView frame];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
	adFrame.origin.y = screenBounds.size.height - adFrame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
	[adView setFrame:adFrame];
}

- (void)adjustAdSize {
	CGRect adFrame = [adView frame];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
	adFrame.origin.y = screenBounds.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
	[adView setFrame:adFrame];
	[UIView beginAnimations:@"AdResize" context:nil];
	[UIView setAnimationDuration:0.7];
	CGSize adSize = [adView actualAdSize];
	CGRect newFrame = adView.frame;
	newFrame.size.height = adSize.height;
	newFrame.size.width = adSize.width;
	newFrame.origin.x = (self.view.frame.size.width - adSize.width)/2;
	newFrame.origin.y = self.view.frame.size.height - adSize.height;
	adView.frame = newFrame;	
	[UIView commitAnimations];
}

@end
