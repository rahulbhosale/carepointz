//
//  MyView.m
//  PageControlTest
//
//  Created by hemanth kp on 1/5/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import "MyView.h"
#import <safeObjC.h>

#define BTN_WIDTH 90
#define BTN_HEIGHT 90


@implementation MyView
@synthesize pageNumber;
@synthesize btnArray;

- (UIImage *) resizeImage: (UIImage *) img intoRect:(CGRect)imgrect
{
	UIImage *image = img;
    UIImage *resizedImg = nil;
	
    if (image.size.width != imgrect.size.width && image.size.height != imgrect.size.height)
	{
        CGSize itemSize = CGSizeMake(imgrect.size.width, imgrect.size.height);
		UIGraphicsBeginImageContext(itemSize);
		[image drawInRect:imgrect];
		resizedImg = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
    }
	return resizedImg;
}

-(id)initwithPageNumber:(int) pageNo andResponder: (UIViewController *) responderVC
{
    self = [super init];
    if (self) {
        self.pageNumber = pageNo;
        NSMutableArray * tempArr = [[NSMutableArray alloc] initWithCapacity:0];
        self.btnArray = tempArr; 
        [tempArr release];
        
        NSString * pathForPList = [[NSBundle mainBundle] pathForResource:@"ImageNameList" ofType:@"plist"];
        SafeLog(@"Path : %@", pathForPList);
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:pathForPList];
        NSArray * imageNameArray = [[NSArray alloc] initWithArray:[dict objectForKey:@"ImageArray"]];
        int noOfImg = 9;

        for (int i = 0; i < noOfImg; i++) {
            int newIndex = (self.pageNumber * 9) + i;
            NSString * imgNameStr = [NSString stringWithFormat:@"%@", [imageNameArray objectAtIndex:newIndex]];
            SafeLog(@"%@", imgNameStr);
            UIButton * temp = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [temp setFrame:CGRectZero];
			
			UIImage *iconImg = [UIImage imageNamed:imgNameStr];
            [temp setImage:iconImg forState:UIControlStateNormal];
			iconImg = nil;
            
			[temp addTarget:responderVC action:@selector(startSearchForOption:) forControlEvents:UIControlEventTouchUpInside];
            temp.tag = 300 + newIndex;
            [self addSubview:temp];
            [self.btnArray addObject:temp];
            self.backgroundColor = [UIColor clearColor];
        }
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    CGFloat offsetX = 10.0;
    CGFloat offsetY = 10.0;
    CGFloat inBetOffset = 0.0;
    CGRect btnRect = CGRectZero;
	
    int noOfImg = 9;

	for (int i = 0; i < noOfImg; i++) {
        UIButton * btn = [self.btnArray objectAtIndex:i];
        int modResult = i % 3;
        int divResult = i / 3;
        
        offsetX = offsetX + ((BTN_WIDTH * modResult) + (modResult * inBetOffset));
        offsetY = offsetY + ((BTN_HEIGHT * divResult) + (divResult * inBetOffset));
        
        btnRect = CGRectMake(offsetX, offsetY, BTN_WIDTH, BTN_WIDTH);
        [btn setFrame:btnRect];
        inBetOffset = 15.0;
        offsetX = 10.0;
        offsetY = 10.0;
    }
}

- (void)dealloc
{
    [super dealloc];
}


@end
