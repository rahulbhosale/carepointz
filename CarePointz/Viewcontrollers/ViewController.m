//
//  ViewController.m
//  TEST
//
//  Created by hemanth kp on 1/5/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import "ViewController.h"
#import "MapViewController.h"
#import "TimeAndMemStampLogger.h"
#import "CarePointzAppDelegate.h"
#import <safeObjC.h>
#import "MyView.h"
#import "AppConstants.h"
#import "Settings.h"

static NSUInteger kNumberOfPages = 4;

@implementation ViewController

@synthesize img;
@synthesize imgName;
@synthesize searchFor;
@synthesize homeScrollView;
@synthesize pageControl;
@synthesize pageControlUsed;
@synthesize viewArray;
@synthesize adView;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		self.title = @"CarePointz";
		self.searchFor = kSearchTypeHospital;
		UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu"
																	   style:UIBarButtonItemStylePlain target:nil action:nil];
		self.navigationItem.backBarButtonItem = backButton;
		[backButton release];
	}
	return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	/** AdWhril code **/
	self.adView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
	self.adView.autoresizingMask =
    UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
	CGRect adFrame = [adView frame];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
	adFrame.origin.y = screenBounds.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
	[adView setFrame:adFrame];
	if (getenv("ADWHIRL_FAKE_DARTS")) {
		// To make ad network selection deterministic
		const char *dartcstr = getenv("ADWHIRL_FAKE_DARTS");
		NSArray *rawdarts = [[NSString stringWithCString:dartcstr]
							 componentsSeparatedByString:@" "];
		NSMutableArray *darts
		= [[NSMutableArray alloc] initWithCapacity:[rawdarts count]];
		for (NSString *dartstr in rawdarts) {
			if ([dartstr length] == 0) {
				continue;
			}
			[darts addObject:[NSNumber numberWithDouble:[dartstr doubleValue]]];
		}
		self.adView.testDarts = darts;
	}
	
	UIDevice *device = [UIDevice currentDevice];
	if ([device respondsToSelector:@selector(isMultitaskingSupported)] &&
		[device isMultitaskingSupported]) {
		[[NSNotificationCenter defaultCenter]
		 addObserver:self
		 selector:@selector(enterForeground:)
		 name:UIApplicationDidBecomeActiveNotification
		 object:nil];
	}
	
	/** End: Adwhril Code **/
	
	NSMutableArray *_viewArray = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPages; i++) {
        [_viewArray addObject:[NSNull null]];
    }
    self.viewArray = _viewArray;
    [_viewArray release];
    
    // a page is the width of the scroll view
    UIScrollView * _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    
    _scrollView.pagingEnabled = YES;
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width * kNumberOfPages, _scrollView.frame.size.height);
    _scrollView.delegate = self;
    _scrollView.showsHorizontalScrollIndicator = NO;
    [_scrollView setBackgroundColor:[UIColor clearColor]];
    self.homeScrollView = _scrollView;
    ObjCRelease(_scrollView);
    
    [self.view addSubview:self.homeScrollView];
    
    UILabel * webLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 320, 320, 18)];
    webLabel.text = @"©2011-2012 Health5C Wellness Solutions (P) Ltd.";
    webLabel.textColor = [UIColor whiteColor];
    webLabel.backgroundColor = [UIColor clearColor];
    webLabel.textAlignment = UITextAlignmentCenter;
    webLabel.font = [UIFont fontWithName:@"Arial" size:11.0];
    [self.view addSubview:webLabel];
    
    UIPageControl * _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 338, 320, 36 )];
    _pageControl.numberOfPages = kNumberOfPages;
    _pageControl.currentPage = 0;
    [_pageControl addTarget:self action:@selector(changedPageControl:) forControlEvents:UIControlEventValueChanged];
    _pageControl.backgroundColor = [UIColor clearColor];
    self.pageControl = _pageControl;
    ObjCRelease(_pageControl);
    
    [self.view addSubview:self.pageControl];
	
	[self.view addSubview:self.adView];
    
    self.view.backgroundColor = [UIColor clearColor];
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
	//self.adView.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	//self.adView.delegate = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown
            || interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
                                         duration:(NSTimeInterval)duration {
	[self adjustLayoutToOrientation:interfaceOrientation];
}

#pragma mark - IBAction
- (IBAction) toggleSearchProviders:(id)sender
{
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *) [UIApplication sharedApplication].delegate;
	appDel.isGoogle = !appDel.isGoogle;
}

- (IBAction) startSearchForOption:(UIButton *)sender
{
	self.searchFor = [(UIButton *)sender tag];	
    if (self.searchFor == kMailTo)
	{
		Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
		if (mailClass != nil)
		{
			// We must always check whether the current device is configured for sending emails
			if ([mailClass canSendMail])
			{
				MFMailComposeViewController * mailComposer = [[MFMailComposeViewController alloc] init];
				mailComposer.mailComposeDelegate = self;
				
				NSString *subj = [NSString stringWithString:@"Check out CarePointz iOS app by Health5C!"];
				NSString *body = [NSString stringWithString:@"Hey pal, \n I'm using CarePointz iOS app by Health5C. Do check it out @ http://itunes.apple.com/us/app/carepointz/id522440830?mt=8 !\n Cheers,"];
				[mailComposer setSubject:subj];
				[mailComposer setMessageBody:body isHTML:YES];
				[self presentModalViewController:mailComposer animated:YES];
				ObjCRelease(mailComposer);
			}
			else
			{
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"AlertTitle_Error", @"")
																message:  NSLocalizedString(@"No_Email_Support", @"")
															   delegate: nil
													  cancelButtonTitle: @"OK"
													  otherButtonTitles: nil];
				[alert show];
				[alert release];
			}
		}
		else
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"AlertTitle_Error", @"")
															message: NSLocalizedString(@"No_Email_Support", @"")
														   delegate: nil
												  cancelButtonTitle: @"OK"
												  otherButtonTitles: nil];
			[alert show];
			[alert release];
		}
	}
	else
	{
		MapViewController *mapVC = [[MapViewController alloc] initWithNibName:nil bundle:nil andSearchType:self.searchFor];
		mapVC.view.frame = self.view.frame;
		[self.navigationController pushViewController:mapVC animated:YES];
		[mapVC release];
	}
}

#pragma mark - IBAction

-(void) changedPageControl: (id) sender
{
    int page = pageControl.currentPage;
	
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
	// update the scroll view to the appropriate page
    CGRect frame = homeScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [homeScrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}


- (void)loadScrollViewWithPage:(int)page {
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
    
    // replace the placeholder if necessary
    MyView * btnView = [self.viewArray objectAtIndex:page];
    if ((NSNull *)btnView == [NSNull null]) {
        btnView = [[MyView alloc] initwithPageNumber:page andResponder:self];
        [self.viewArray replaceObjectAtIndex:page withObject:btnView];
        [btnView release];
    }
    
    // add the controller's view to the scroll view
    if (nil == btnView.superview) {
        CGRect frame = homeScrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        btnView.frame = frame;
        [homeScrollView addSubview:btnView];
    }
}

#pragma mark - Scrollview Delegate methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (pageControlUsed) {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

#pragma mark - AdWhril delegate methods
- (NSString *)admobPublisherID {
	return kAdModPublisherID;
}

- (NSString *)adWhirlApplicationKey {
	return kAdWhrilAppKey;
}

- (UIViewController *)viewControllerForPresentingModalView {
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[[UIApplication sharedApplication] delegate];
	return appDel.window.rootViewController;
}

- (NSURL *)adWhirlConfigURL {
	return [NSURL URLWithString:kConfigURL];
}

- (NSURL *)adWhirlImpMetricURL {
	return [NSURL URLWithString:kImpMetricURL];
}

- (NSURL *)adWhirlClickMetricURL {
	return [NSURL URLWithString:kClickMetricURL];
}

- (NSURL *)adWhirlCustomAdURL {
	return [NSURL URLWithString:kCustomAdURL];
}

- (void)adWhirlDidReceiveAd:(AdWhirlView *)adWhirlView {
	errStr = [NSString stringWithFormat:
			  @"Got ad from %@, size %@",
			  [adWhirlView mostRecentNetworkName],
			  NSStringFromCGSize([adWhirlView actualAdSize])];
	[self adjustAdSize];
}

- (void)adWhirlDidFailToReceiveAd:(AdWhirlView *)adWhirlView usingBackup:(BOOL)yesOrNo {
	errStr = [NSString stringWithFormat:
			  @"Failed to receive ad from %@, %@. Error: %@",
			  [adWhirlView mostRecentNetworkName],
			  yesOrNo? @"will use backup" : @"will NOT use backup",
			  adWhirlView.lastError == nil? @"no error" : [adWhirlView.lastError localizedDescription]];
	[self adjustAdSize];
}

- (void)adWhirlReceivedRequestForDeveloperToFufill:(AdWhirlView *)adWhirlView {
	UILabel *replacement = [[UILabel alloc] initWithFrame:kAdWhirlViewDefaultFrame];
	replacement.backgroundColor = [UIColor redColor];
	replacement.textColor = [UIColor whiteColor];
	replacement.textAlignment = UITextAlignmentCenter;
	replacement.text = @"Generic Notification";
	[adWhirlView replaceBannerViewWith:replacement];
	[replacement release];
	[self adjustAdSize];
	errStr = @"Generic Notification";
}

- (void)adWhirlReceivedNotificationAdsAreOff:(AdWhirlView *)adWhirlView {
	errStr = @"Ads are off";
}

- (void)adWhirlWillPresentFullScreenModal {
	SafeLog(@"SimpleView: will present full screen modal");
}

- (void)adWhirlDidDismissFullScreenModal {
	SafeLog(@"SimpleView: did dismiss full screen modal");
}

- (void)adWhirlDidReceiveConfig:(AdWhirlView *)adWhirlView {
	errStr = @"Received config. Requesting ad...";
}

- (BOOL)adWhirlTestMode {
	return NO;
}

- (CLLocation *)locationInfo {
	return [LocationManager sharedSingleton].userLocation;
}

#pragma mark event methods

- (void)performEvent {
	errStr = @"Event performed";
}

- (void)performEvent2:(AdWhirlView *)adWhirlView {
	UILabel *replacement = [[UILabel alloc] initWithFrame:kAdWhirlViewDefaultFrame];
	replacement.backgroundColor = [UIColor blackColor];
	replacement.textColor = [UIColor whiteColor];
	replacement.textAlignment = UITextAlignmentCenter;
	replacement.text = [NSString stringWithFormat:@"Event performed, view %x", adWhirlView];
	[adWhirlView replaceBannerViewWith:replacement];
	[replacement release];
	[self adjustAdSize];
	errStr = [NSString stringWithFormat:@"Event performed, view %x", adWhirlView];
}

#pragma mark multitasking methods

- (void)enterForeground:(NSNotification *)notification {
	AWLogDebug(@"SimpleView entering foreground");
	[[Settings sharedSettings] updateSettings];
//	for (UIWindow* window in [UIApplication sharedApplication].windows) {
//		NSArray* subviews = window.subviews;
//		if ([subviews count] > 0)
//			if ([[subviews objectAtIndex:0] isKindOfClass:[UIAlertView class]])
//				[[subviews objectAtIndex:0] dismissWithClickedButtonIndex:5 animated:NO];
//	}
	[self.adView updateAdWhirlConfig];
}

- (void)adjustLayoutToOrientation:(UIInterfaceOrientation)newOrientation {
	CGRect adFrame = [adView frame];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
	adFrame.origin.y = screenBounds.size.height - adFrame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
	[adView setFrame:adFrame];
}

- (void)adjustAdSize {
	CGRect adFrame = [adView frame];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
	adFrame.origin.y = screenBounds.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
	[adView setFrame:adFrame];
	[UIView beginAnimations:@"AdResize" context:nil];
	[UIView setAnimationDuration:0.7];
	CGSize adSize = [adView actualAdSize];
	CGRect newFrame = adView.frame;
	newFrame.size.height = adSize.height;
	newFrame.size.width = adSize.width;
	newFrame.origin.x = (self.view.frame.size.width - adSize.width)/2;
	newFrame.origin.y = self.view.frame.size.height - adSize.height;
	adView.frame = newFrame;
	[UIView commitAnimations];
}

#pragma mark - mail Composer Delegate Methods

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
	NSString *msg = nil;
	switch (result)
	{
        case MFMailComposeResultCancelled:
			msg = @"The mail sending got cancelled!";
            break;
        case MFMailComposeResultFailed:
			msg = @"The mail sending failed!";
            break;
        case MFMailComposeResultSaved:
			msg = @"Your mail got saved!";
            break;
        case MFMailComposeResultSent:
			msg = @"Your mail have been sent successfully.";
            break;
        default:
            break;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Info"
													message: msg
												   delegate: nil
										  cancelButtonTitle: @"OK"
										  otherButtonTitles: nil];
	[alert show];
	[alert release];
    [self dismissModalViewControllerAnimated:YES];
}
@end
