//
//  MyView.h
//  PageControlTest
//
//  Created by hemanth kp on 1/5/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    kButtonTypeEye = 1,
	kButtonTypeDental,
	kButtonTypeHeart,
	kButtonTypeBone,
	kButtonTypeCancer,
	kButtonTypeHair,
	kButtonTypeEmergency,
	kButtonTypeHealth,
	kButtonTypeDiabetese,
	kButtonTypeKidney,
	kButtonTypeGeneral
}ButtonType;


@interface MyView : UIView {
    int pageNumber;
    NSMutableArray * btnArray;
}
@property (nonatomic) int pageNumber;
@property (nonatomic, retain) NSMutableArray * btnArray;
-(id)initwithPageNumber:(int) pageNo andResponder: (UIViewController *) responderVC;
@end
