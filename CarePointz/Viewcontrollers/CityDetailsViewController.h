//
//  CityDetailsViewController.h
//  TEST
//
//  Created by hemanth kp on 3/15/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectionManager.h"
#import "AppConstants.h"

@class SearchResult;
@class AdWhirlView;
@class MapViewController;

@interface CityDetailsViewController : UIViewController<ConnectionManagerDelegate, AdWhirlDelegate, UIAlertViewDelegate>
{
    UILabel *address;
	UILabel *name;
	UILabel *phoneNum;
	UILabel *internationPhoneNum;
	UILabel *website;
	UILabel *rating;
	UIAlertView *loadingAlert;
	NSString *refStr;
	NSString *placeID;
	
	SearchResult *selectedResult;
	AdWhirlView *adView;
	NSString *errStr;
	
	MapViewController *parentVC;
}

@property (nonatomic, retain) IBOutlet UILabel *name;
@property (nonatomic, retain) IBOutlet UILabel *address;
@property (nonatomic, retain) IBOutlet UILabel *phoneNum;
@property (nonatomic, retain) IBOutlet UILabel *internationPhoneNum;
@property (nonatomic, retain) IBOutlet UILabel *website;
@property (nonatomic, retain) IBOutlet UILabel *rating;
@property (nonatomic, retain) SearchResult *selectedResult;
@property (nonatomic, retain) AdWhirlView *adView;
@property (nonatomic, retain) MapViewController *parentVC;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andSelectedResult:(SearchResult *) selResult;
- (void)adjustAdSize;
- (void)adjustLayoutToOrientation:(UIInterfaceOrientation)newOrientation;
- (void)popToMap;

@end
