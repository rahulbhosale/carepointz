//
//  ConnectionManager.h
//
//  Created by hemanth kp on 03/03/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ConnectionManager;

@protocol ConnectionManagerDelegate<NSObject>

@required
-(void) connectionManager :(ConnectionManager *)connectionManager didGetData:(NSData *) data;
-(void) connectionManager :(ConnectionManager *)connectionManager didFailWithError:(NSError *) error;
@end


/*!
 @class ConnectionManager
 
 @discussion This class handles all the connection
 related operation. It perform checks for response code.
 and after receiving data it will inform to its delegate. 
 */



@interface ConnectionManager : NSObject {
	id<ConnectionManagerDelegate> _delegate;
	id _connectionID;
	NSMutableURLRequest * _request;
	NSURLConnection * _connectionObject;
	NSMutableData * _mutableData;
}

@property ( nonatomic, retain ) id<ConnectionManagerDelegate> delegate;
@property ( nonatomic, retain ) NSMutableURLRequest * request;
@property ( nonatomic, retain ) NSURLConnection * connectionObject;
@property ( nonatomic, retain ) NSMutableData * mutableData;
@property ( nonatomic, assign ) id connectionID;

- (id) initWithRequest:(NSMutableURLRequest *) clientRequest;
- (void) startConnection;

@end
