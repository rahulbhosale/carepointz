//
//  ConnectionManager.m
//
//  Created by hemanth kp on 03/03/11.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ConnectionManager.h"

@implementation ConnectionManager

@synthesize delegate = _delegate;
@synthesize request = _request ;
@synthesize connectionObject = _connectionObject;
@synthesize mutableData = _mutableData;
@synthesize connectionID = _connectionID;

-(id) initWithRequest:(NSMutableURLRequest *) clientRequest
{
	if (self = [super init]) {
		self.request = clientRequest;
	}
	return self;
}

-(void) startConnection
{
	NSURLConnection * connection = nil;
	
	// show network activity indicator
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	[self.request setTimeoutInterval:60];
	connection = [[NSURLConnection alloc] initWithRequest:self.request delegate:self];
	//schedule connection to the current run loop
	
	NSRunLoop * runloop = [NSRunLoop currentRunLoop];
	[connection scheduleInRunLoop:runloop forMode:nil];
	
 	self.connectionObject = connection;
	[connection release];
	
	NSMutableData * tempData = [[NSMutableData alloc] init];
	self.mutableData = tempData;
	[tempData release];
}

#pragma mark NSURLConnectionDelegate methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	// do respective check depends upon ressponse code
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	//append received data to the mutabledata object
	[self.mutableData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	// stop showing network activity indicator
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	// call didGetData of delegate	
	[self.delegate connectionManager:self didGetData:self.mutableData];	
	[connection cancel];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	// stop showing network activity indicator
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	//cancel connection
	[connection cancel];
	
	//inform to delegate that error has been occured	
	[self.delegate connectionManager:self didFailWithError:error];	
}


-(void) dealloc
{
	if (self.delegate) {
		self.delegate = nil;
	}
	if (self.request) {
		self.request = nil;
	}
	if (self.connectionObject) {
		self.connectionObject = nil;
	}
	self.connectionID = nil;
	[super dealloc];
}

@end
