//
//  LocationManager.m
//  city_zapper
//
//  Created by hemanthkp on 7/23/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "LocationManager.h"
#import <safeObjC.h>
#import "Settings.h"

#pragma mark 
@interface LocationManager (LocationManagerPrivate)

- (void) _startOrStopLocating;
- (CLLocationDistance)_getDistanceForLatitude:(double)latitude andLongitude:(double)longitude inMiles:(BOOL)isMiles;

@end

@implementation LocationManager

@synthesize locmanager = _locmanager;
@synthesize userLocation = _userLocation;
@synthesize cityLocation = _cityLocation;

@synthesize errMsg = _errMsg;

@synthesize isLocating = _isLocating;
@synthesize wasFound = _wasFound;

@synthesize delegate = _delegate;

#pragma mark CZSingleton Protocol
static LocationManager *sharedSingleton = nil;

+ (LocationManager *)sharedSingleton {
	@synchronized(self) {
		if (sharedSingleton == nil) {
			[[self alloc] init]; // assignment not done here
		}
	}
	return sharedSingleton;
}

+ (id)allocWithZone:(NSZone *)zone {
	@synchronized(self) {
		if (sharedSingleton == nil) {
			sharedSingleton = [super allocWithZone:zone];
			return sharedSingleton;  // assignment and return on first allocation
		}
	}
	return nil; // on subsequent allocation attempts return nil
}

//Overriding the NSObject's init method
- (id)init {
	if(self = [super init])
	{
		self.locmanager = [[CLLocationManager alloc] init];
		SafeLog(@"Retain count of locmanager is %d",[self.locmanager retainCount]);
		[self.locmanager setDelegate:self]; 
		[self.locmanager setDesiredAccuracy:kCLLocationAccuracyBest];
		self.locmanager.distanceFilter = [[[Settings sharedSettings] locUpdateMeters] doubleValue];
		
		self.isLocating = NO;
		self.errMsg = nil;
		
		if (!self.locmanager.locationServicesEnabled) 
		{
			self.errMsg = @"Location Services Are Not Enabled";
		}
		else{
			//Starting the location search now itself as it takes time to get the location
			[self _startOrStopLocating];
		}
	}
	return self;
}

- (void)setDelegate: (id)newDelegate
{
	if( _delegate != newDelegate )
	{
		_delegate = nil;
		_delegate = newDelegate;
	}
}

- (void) updateDistanceFilter
{
	self.locmanager.distanceFilter = [[[Settings sharedSettings] locUpdateMeters] doubleValue];
	
	self.isLocating = NO;
	self.errMsg = nil;
	
	if (!self.locmanager.locationServicesEnabled) 
	{
		self.errMsg = @"Location Services Are Not Enabled";
		if (self.delegate) {
			[self.delegate newError:self.errMsg];
		}
	}
	else
	{
		//Starting the location search now itself as it takes time to get the location
		[self restartLocating];
	}
}

#pragma mark LocationManagerPrivate methods implementation

// Perform a location request – either start or stop locating 
- (void) _startOrStopLocating 
{ 
	if (self.isLocating) 
	{
		self.wasFound = YES;
		SafeLog(@"Scanning ended by request.");
		[self.locmanager stopUpdatingLocation];
	} else { 
		self.wasFound = NO;
		SafeLog(@"Scanning for location...");
		[self.locmanager startUpdatingLocation]; 
	}
	self.isLocating = !(self.isLocating);
}

- (CLLocationDistance)_getDistanceForLatitude:(double)latitude andLongitude:(double)longitude inMiles:(BOOL)isMiles
{
	SafeLog(@"In _getDistanceForLatitude:");
	CLLocation *toLocation = nil;
	
	CLLocationDistance dist = 0.0;
	
	self.errMsg = nil;
	
	toLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
	
	if(toLocation == nil)
	{
		self.errMsg = @"No location found!!";
		dist = -1.0;
	}
	else
	{
		if(USE_STANDARD_DISTANCE_CALCULATOR)
		{
			@try{
				dist = [toLocation distanceFromLocation:self.userLocation];
			}
			@catch (NSException *e) {
				dist = -1.0;
				self.errMsg = [e description];
				if(self.delegate)
					[self.delegate newError:self.errMsg];
			}
		}
		else
		{
			@try{
				CLLocationCoordinate2D loc = [self.userLocation coordinate];
				CLLocationCoordinate2D loc1 = [toLocation coordinate];
				
				if(!isMiles)
				{
					dist = RADIUS_OF_EARTH_IN_KMS * acos(sin(loc.latitude * DEGREES_TO_RADIANS) * sin(loc1.latitude * DEGREES_TO_RADIANS) 
														 + cos(loc.latitude * DEGREES_TO_RADIANS) * cos(loc1.latitude * DEGREES_TO_RADIANS)
														 * cos((loc.longitude - loc1.longitude) * DEGREES_TO_RADIANS));
				}
				else 
				{
					dist = RADIUS_OF_EARTH_IN_MILES * acos(sin(loc.latitude * DEGREES_TO_RADIANS) * sin(loc1.latitude * DEGREES_TO_RADIANS) 
														   + cos(loc.latitude * DEGREES_TO_RADIANS) * cos(loc1.latitude * DEGREES_TO_RADIANS)
														   * cos((loc.longitude - loc1.longitude) * DEGREES_TO_RADIANS));
				}
			}
			@catch (NSException *e) {
				dist = -1.0;
				self.errMsg = [e description];
				SafeLog(@"ERRRORORORORORO: %@",self.errMsg);
				if(self.delegate)
					[self.delegate newError:self.errMsg];
			}
		}
	}
	
	[toLocation release];
	return dist;
}


// Called when the location is updated
- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation
{
	//NSMutableString *update = [[[NSMutableString alloc] init] autorelease];
	
	SafeLog(@"Got new location from the LocationManager!!");
	
	self.errMsg = nil;
	
	// Location has been found. Save it to the userLocation
	self.userLocation = newLocation;
	
	if(oldLocation != nil)
	{
		SafeLog(@"You have moved: %f meters",[newLocation getDistanceFrom:oldLocation]);
	}
	self.errMsg = nil;
	self.wasFound = YES;
	self.isLocating = NO;
	
	// Send the update to our delegate
	if(self.delegate)
		[self.delegate newLocationUpdate];
}


// Called when there is an error getting the location
- (void)locationManager:(CLLocationManager *)manager
	   didFailWithError:(NSError *)error
{
	SafeLog(@"ERROR Updating Location");
	NSMutableString *errorString = [[[NSMutableString alloc] init] autorelease];
	self.errMsg = nil;
	
	if ([error domain] == kCLErrorDomain) {
		
		// We handle CoreLocation-related errors here
		
		switch ([error code]) {
				// This error code is usually returned whenever user taps "Don't Allow" in response to
				// being told your app wants to access the current location. Once this happens, you cannot
				// attempt to get the location again until the app has quit and relaunched.
				//
				// "Don't Allow" on two successive app launches is the same as saying "never allow". The user
				// can reset this for all apps by going to Settings > General > Reset > Reset Location Warnings.
				//
			case kCLErrorDenied:
				[errorString appendFormat:@" Location Denied\n"];
				break;
				
				// This error code is usually returned whenever the device has no data or WiFi connectivity,
				// or when the location cannot be determined for some other reason.
				//
				// CoreLocation will keep trying, so you can keep waiting, or prompt the user.
				//
			case kCLErrorLocationUnknown:
				[errorString appendFormat:@"Location Unknown\n"];
				break;
				
				// We shouldn't ever get an unknown error code, but just in case...
				//
			default:
				[errorString appendFormat:@"Generic Location Error: %d\n",[error code]];
				break;
		}
	} else {
		// We handle all non-CoreLocation errors here
		// (we depend on localizedDescription for localization)
		[errorString appendFormat:@"Error domain: \"%@\"  Error code: %d\n", [error domain], [error code]];
		[errorString appendFormat:@"Description: \"%@\"\n", [error localizedDescription]];
	}
	self.isLocating = YES;
	self.wasFound = NO;
	self.errMsg =[NSString stringWithString:errorString];
	
	//Cancel the locating
	[self _startOrStopLocating];
	
	// Send the update to our delegate
	if(self.delegate)
		[self.delegate newError:self.errMsg];
}

#pragma mark LocationManager distance calculator methods

// Return the distance in kilometers for the latitude and longitude given
- (double) distanceForLatitude:(double)latitude andLongitude:(double)longitude inMiles:(BOOL)isMiles
{
	SafeLog(@"Finding distance");
	CLLocationDistance dist = 0.0;
	
	self.errMsg = nil;
	
	// If userLocation is not yet determined then send an error msg;
	if(self.userLocation == nil)
	{
		self.errMsg = @"User's location not found!!";
		//distance = nil;
		SafeLog(@"ERRRR: %@",self.errMsg);
		if(self.delegate)
			[self.delegate newError:self.errMsg];
	}
	else
	{
		dist = [self _getDistanceForLatitude:latitude andLongitude:longitude inMiles:isMiles];
		
		if(isMiles)
		{
			dist = dist * METERS_TO_MILES;
			//distance = [NSString stringWithFormat:@"%3.1lfMl",dist * METERS_TO_MILES];
		}
		else
		{
			dist = dist * METERS_TO_KMS;
			//distance = [NSString stringWithFormat:@"%3.1lfKM",dist * METERS_TO_KMS];
		}
		
		if ( dist <= 0.0 && self.errMsg != nil && [self.errMsg length] > 0 && ![self.errMsg isEqualToString:@"(null)"])
		{
			//distance = nil;
			SafeLog(@"ERRR: %@",self.errMsg);
			if(self.delegate)
				[self.delegate newError:self.errMsg];
		}
	}
	//return distance;
	return dist;
}

- (NSArray *) distancesForCategories:(NSArray *)categoryArray inMiles:(BOOL)isMiles {
	SafeLog(@"finding DISTANCEs for categories");
	self.errMsg = nil;
	
	// Create and return the sorted list only if userlocation was found.
	// If not the distance cannot be calculated. Return The appropriate error.
	if(self.userLocation != nil)
	{
		//[self _createTipsDistanceArrayForCategories:categoryArray inMiles:isMiles];
		//return [self _sortDistance]; //The caller should release this.
		return nil;
	}
	else
	{
		self.errMsg = @"User Location Not found";
		if(self.delegate)
			[self.delegate newError:self.errMsg];
		return nil;
	}
}

#pragma mark - Public Methods

- (void) stopLocating
{
	self.isLocating = YES;
	[self _startOrStopLocating];
}

- (void) resumeLocating
{
	self.isLocating = NO;
	[self _startOrStopLocating];
}

- (void) restartLocating
{
	self.isLocating = YES;
	[self _startOrStopLocating];
	self.isLocating = NO;
	[self _startOrStopLocating];
}

#pragma mark NSObject Methods
- (void) dealloc
{
	SafeLog(@"locationmanger dealloc");
	if(self.locmanager)
		[self.locmanager release];
	
	if(self.userLocation)
		[self.userLocation release];
	
	if(self.errMsg)
		self.errMsg = nil;
	
	if(self.cityLocation)
		[self.cityLocation release];
	
	if(self.delegate)
		self.delegate = nil;
	
	[super dealloc];
}

@end
