//
//  LocationManager.h
//  city_zapper
//
//  Created by hemanthkp on 7/23/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>

#define METERS_TO_MILES						0.0006214	//1 meter = 0.0006214 miles
#define METERS_TO_KMS						1/1000		//1 meter = 1/1000 kms

#define DEGREES_TO_RADIANS					M_PI/180	// 1˚ = 180/pi rads

#define RADIUS_OF_EARTH_IN_KMS				6378.7		//kilometers
#define RADIUS_OF_EARTH_IN_NMILES			3437.74677	//nautical miles
#define RADIUS_OF_EARTH_IN_MILES			3963.0		//statute miles

#define USE_STANDARD_DISTANCE_CALCULATOR	YES			//Switch between formula and built-in dist calculator
#define MINIMUM_DISTANCE_MOVED				500.0		//This is used for the location manager to notify the change in location only after the user has moved 100 meters
														// this can be kCLDistanceFilterNone also to get callback for every movement.


#pragma mark LocationManagerProtocol
// This protocol is used to send the text for location updates back to another view controller
@protocol LocationManagerDelegate <NSObject>

@required
-(void)newLocationUpdate;
-(void)newError:(NSString *)text;

@end


#pragma mark LocationManager singleton class
@interface LocationManager :  NSObject <CLLocationManagerDelegate> {
@private
	CLLocationManager   *_locmanager;
	CLLocation			*_userLocation;
	CLLocation			*_cityLocation;
	
	id					_delegate;
	
	NSString			*_errMsg;
	
	BOOL                _isLocating; 
	BOOL                _wasFound;
}

@property (nonatomic, assign) CLLocationManager *locmanager;
@property (nonatomic, retain) CLLocation *userLocation;
@property (nonatomic, retain) CLLocation *cityLocation;
@property (nonatomic,assign) id <LocationManagerDelegate> delegate;

@property (nonatomic, assign) NSString *errMsg;

@property (nonatomic) BOOL isLocating;
@property (nonatomic) BOOL wasFound;

+ (LocationManager *)sharedSingleton;

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation;

- (void)locationManager:(CLLocationManager *)manager
	   didFailWithError:(NSError *)error;

- (double) distanceForLatitude:(double)latitude andLongitude:(double)longitude inMiles:(BOOL)isMiles;

- (void) stopLocating;

- (void) resumeLocating;

- (void) restartLocating;

- (void) updateDistanceFilter;
@end
