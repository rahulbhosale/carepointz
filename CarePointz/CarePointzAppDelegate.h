//
//  CarePointzAppDelegate.h
//  CarePointz
//
//  Created by hemanth kp on 3/30/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationManager.h"

#define MAPS_DETAILS_URL [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?sensor=false&key=%@&reference=",kGoogleApiKey]

//#define MAP_BASE_URL @"https://maps.googleapis.com/maps/api/place/search/json?radius=2000&sensor=false&key=AIzaSyByIWSTZ1vS-XyKPm8ujqkwwFEc21v_UwI"
//#define MAP_BASE_URL @"http://local.yahooapis.com/LocalSearchService/V3/localSearch?appid=Ywx65dbV34G0z1IzsP.WyaFOTkoBytEzMKKmS_61tnK8BoWEtuFiEwx_iIhkKqtJ6ubN&results=20&output=json"

@class ViewController,SQLiteManager;
@interface CarePointzAppDelegate : NSObject <UIApplicationDelegate,LocationManagerDelegate>
{
	SQLiteManager *_mrMgr;
	NSMutableString *searchString;
	NSMutableString *searchDetailsURL;
	double currentLat;
	double currentLng;
	BOOL gotLoc;
	BOOL updateLocation;
	BOOL isGoogle;
	NSString *baseURL;
	UIWindow *window;
}

@property (retain, nonatomic) IBOutlet UIWindow *window;

@property (retain, nonatomic) ViewController *viewController;

@property (retain, nonatomic) NSMutableString *searchString;

@property (retain, nonatomic) NSMutableString *searchDetailsURL;

@property (nonatomic) double currentLat;

@property (nonatomic) double currentLng;

@property (nonatomic) BOOL updateLocation;

@property (nonatomic) BOOL isGoogle;

@property (nonatomic, retain) SQLiteManager *mrMgr;

@property (nonatomic, copy, setter = setBaseURL:, getter = baseURL) NSString *baseURL;

- (void) dynamicallyResizeLabel:(UILabel *)lbl withMaxSize:(CGSize) maxSize;

- (NSString *) searchURLForLocationWithLatitude:(double) lat logitude:(double) lng andName:(NSString *) name;

- (void) updateSearchURL:(NSString *)searchURLStr withName:(NSString *) name;

- (NSString *) updateSearchURL:(NSString *)searchURLStr withSearchRadius:(NSInteger) searchRadius andName:(NSString *)name;

- (void) showActivityWithMessage:(NSString *)activityStr onViewController:(UIViewController *)vc withSpinner:(BOOL) shouldShowSpinner;

- (void) updateOrCreateActivityWithMessage:(NSString *)newActivityStr onViewController:(UIViewController *)vc withSpinner:(BOOL) shouldShowSpinner;

- (void) hideActivityOnViewController:(UIViewController *)vc;

- (BOOL) isStatusError:(NSString *)statusStr;
@end