//
//  SearchResult.m
//  TEST
//
//  Created by hemanth kp on 3/29/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import "SearchResult.h"
#import "SQLiteManager.h"
#import "CarePointzAppDelegate.h"
#import <safeObjC.h>

@interface IntStorage : NSObject {
	NSInteger integerVal;
}
@property NSInteger integerVal;
@end

@implementation IntStorage

@synthesize integerVal;

@end

@implementation SearchResult

@synthesize searchResultID;
@synthesize placeID;
@synthesize category;
@synthesize lat;
@synthesize lng;
@synthesize name;
@synthesize rating;
@synthesize reference;
@synthesize vicinity;
@synthesize address;
@synthesize phoneNumber;
@synthesize internationalPhoneNumber;
@synthesize websiteURLString;
@synthesize lastAccessed;
@synthesize currentDistance;

- (void)dealloc
{
	self.placeID = nil;
	self.name = nil;
	self.reference = nil;
	self.vicinity = nil;
	self.address = nil;
	self.phoneNumber = nil;
	self.internationalPhoneNumber = nil;
	self.websiteURLString = nil;
	self.category = nil;
	[super dealloc];
}

- (NSInteger) lastInsertedRowFromTable:(NSString *) tblName {
    NSLogStartMethod;
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
	
    int lastInsertedRowID = (int)sqlite3_last_insert_rowid([appDel.mrMgr db_]);
    NSLogReturnValue(@"%d", lastInsertedRowID);
}

- (BOOL) executeSimpleRowWithQuery:(NSString *) queryStr {
    NSLogStartMethod;
	BOOL isSuccessFul = NO;
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
	SQLiteQuery *query = [appDel.mrMgr queryWithQueryString: queryStr
										withReuseIdentifier: nil
												 withFormat: nil];
	
	@try {
		[query executeWithHandler: nil] ;
		isSuccessFul = YES;
	}
	@catch (NSException *exception) {
		isSuccessFul = NO;
	}
    NSLogReturnValue(@"%d", isSuccessFul);
}

- (void) getID
{
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
	NSString *fetchSearchResultStr = [NSString stringWithFormat:@"select id from CPSearchResults where name = \"%@\" and lat=%lf and lng=%lf",self.name,self.lat,self.lng];
	SQLiteQuery *query1 = [appDel.mrMgr queryWithQueryString: fetchSearchResultStr
										 withReuseIdentifier: nil
												  withFormat: nil];
	SQLiteResultSimpleRowHandler *handler = [[SQLiteResultSimpleRowHandler alloc] init];
	handler.creator = NSClassFromString( @"IntStorage");
	handler.createItem = @selector(new);
	NSMutableArray *act = [[NSMutableArray alloc] initWithColumnActions: 1,
						   @"id", @selector(setIntegerVal:), NULL];
	handler.columnActionList = act;
	ObjCRelease(act);
	[query1 executeWithHandler: handler] ;
	IntStorage *ints = [handler.outItem retain];
	self.searchResultID = ints.integerVal;
	ObjCRelease(handler);
	NSLogStartMethodMsg(@"%@",self);
}

- (BOOL) addToDB
{
	BOOL isSuccessful = NO;
	NSMutableString *tempStr = [NSMutableString stringWithString:self.name];
	[tempStr replaceOccurrencesOfString:@"\"" withString:@"'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [self.name length])];
	[tempStr replaceOccurrencesOfString:@"'" withString:@"\'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [self.name length])];
	self.name = tempStr;
	tempStr = nil;
	
	if (self.vicinity)
	{
		tempStr = [NSMutableString stringWithString:self.vicinity];
		[tempStr replaceOccurrencesOfString:@"\"" withString:@"'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [self.vicinity length])];
		self.vicinity = tempStr;
		tempStr = nil;
	}
	
	if (self.address)
	{
		tempStr = [NSMutableString stringWithString:self.address];
		[tempStr replaceOccurrencesOfString:@"\"" withString:@"'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [self.address length])];
		self.address = tempStr;
		tempStr = nil;
	}
	
	NSString *newSearchResultStr = [NSString stringWithFormat:@"insert into CPSearchResults(placeID,lat,lng,name,rating,reference,vicinity,address,phoneNumber,internationalPhoneNumber,websiteURLString, categoryName,lastAccessed) values ('%@',%lf,%lf,\"%@\",%f,'%@',\"%@\",\"%@\",'%@','%@','%@','%@','%u');",self.placeID, self.lat, self.lng, self.name, self.rating, self.reference, self.vicinity, self.address, self.phoneNumber,self.internationalPhoneNumber, self.websiteURLString, self.category, self.lastAccessed];
	isSuccessful = [self executeSimpleRowWithQuery:newSearchResultStr];
	if (isSuccessful) {
		int remID = [self lastInsertedRowFromTable:@"CPSearchResults"];
		self.searchResultID = remID;
	}
	return isSuccessful;
}

- (BOOL) updateTimeStamp
{
	BOOL isSuccessful = NO;
		
	NSString *upSearchResultStr = [NSString stringWithFormat:@"update CPSearchResults set lastAccessed = %u where id = %d;",self.lastAccessed, self.searchResultID];
	isSuccessful = [self executeSimpleRowWithQuery:upSearchResultStr];
	
	return isSuccessful;	
}

- (BOOL) updateReferenceID
{
	BOOL isSuccessful = NO;
	NSMutableString *tempStr = [NSMutableString stringWithString:self.name];
	[tempStr replaceOccurrencesOfString:@"\"" withString:@"'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [self.name length])];
	[tempStr replaceOccurrencesOfString:@"\"" withString:@"'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [self.name length])];
	self.name = tempStr;
	tempStr = nil;
	
	if (self.vicinity)
	{
		tempStr = [NSMutableString stringWithString:self.vicinity];
		[tempStr replaceOccurrencesOfString:@"\"" withString:@"'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [self.vicinity length])];
		self.vicinity = tempStr;
		tempStr = nil;
	}
	self.address = self.phoneNumber = self.internationalPhoneNumber = self.websiteURLString = nil;
	
	NSString *upSearchResultStr = [NSString stringWithFormat:@"update CPSearchResults set reference = '%@', vicinity = \"%@\", address = \"%@\", phoneNumber = '%@', internationalPhoneNumber ='%@', websiteURLString = '%@', categoryName = '%@', lastAccessed = %u where id = %d",self.reference, self.vicinity, self.address, self.phoneNumber,self.internationalPhoneNumber, self.websiteURLString, self.category,self.lastAccessed, self.searchResultID];
	isSuccessful = [self executeSimpleRowWithQuery:upSearchResultStr];
	
	return isSuccessful;
}

- (BOOL) updateDB
{
	BOOL isSuccessful = NO;
	NSMutableString *tempStr = [NSMutableString stringWithString:self.name];
	[tempStr replaceOccurrencesOfString:@"\"" withString:@"'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [self.name length])];
	[tempStr replaceOccurrencesOfString:@"\"" withString:@"\"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [self.name length])];
	self.name = tempStr;
	tempStr = nil;
	
	if (self.vicinity)
	{
		tempStr = [NSMutableString stringWithString:self.vicinity];
		[tempStr replaceOccurrencesOfString:@"\"" withString:@"'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [self.vicinity length])];
		self.vicinity = tempStr;
		tempStr = nil;
	}
	
	if (self.address)
	{
		tempStr = [NSMutableString stringWithString:self.address];
		[tempStr replaceOccurrencesOfString:@"\"" withString:@"'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [self.address length])];
		self.address = tempStr;
		tempStr = nil;
	}
	
	NSString *upSearchResultStr = [NSString stringWithFormat:@"update CPSearchResults set placeID = '%@', lat = %lf, lng = %lf, name = \"%@\", rating = %f, reference = '%@', vicinity = \"%@\", address = \"%@\", phoneNumber = '%@', internationalPhoneNumber ='%@', websiteURLString = '%@', categoryName = '%@', lastAccessed = %u where id = %d;",self.placeID, self.lat, self.lng, self.name, self.rating, self.reference, self.vicinity, self.address, self.phoneNumber,self.internationalPhoneNumber, self.websiteURLString, self.category, self.lastAccessed, self.searchResultID];
	isSuccessful = [self executeSimpleRowWithQuery:upSearchResultStr];
	
	return isSuccessful;
}

- (void) fetchFromDBWhereName:(NSString *) placeName atLat:(double) lats andLongitude:(double) longi
{
	NSString *fetchSearchResultStr = [NSString stringWithFormat:@"select * from CPSearchResults where name = \"%@\" and lat=%lf and lng=%lf",placeName,lats,longi];
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
	SQLiteQuery *query1 = [appDel.mrMgr queryWithQueryString: fetchSearchResultStr
										 withReuseIdentifier: nil
												  withFormat: nil];
	SQLiteResultSimpleRowHandler *handler = [[SQLiteResultSimpleRowHandler alloc] init];
	handler.creator = NSClassFromString( @"SearchResult" );
	handler.createItem = @selector(new);
	NSMutableArray *act = [[NSMutableArray alloc] initWithColumnActions: 14,
						   @"id", @selector(setSearchResultID:), NULL,
						   @"placeID", @selector(setPlaceID:), NULL,
						   @"lat", @selector(setLat:), NULL,
						   @"lng", @selector(setLng:), NULL,
						   @"name", @selector(setName:), NULL,
						   @"rating", @selector(setRating:), NULL,
						   @"reference", @selector(setReference:), NULL,
						   @"vicinity", @selector(setVicinity:), NULL,
						   @"address", @selector(setAddress:), NULL,
						   @"phoneNumber", @selector(setPhoneNumber:), NULL,
						   @"internationalPhoneNumber", @selector(setInternationalPhoneNumber:), NULL,
						   @"websiteURLString", @selector(setWebsiteURLString:), NULL,
						   @"categoryName", @selector(setCategory:), NULL,
						   @"lastAccessed", @selector(setLastAccessed:), NULL];
	handler.columnActionList = act;
	ObjCRelease(act);
	[query1 executeWithHandler: handler] ;
	SearchResult *temp = [handler.outItem retain];
	self.searchResultID = temp.searchResultID;
	self.placeID = temp.placeID;
	self.lat = temp.lat;
	self.lng = temp.lng;
	self.name = temp.name;
	self.rating = temp.rating;
	self.vicinity = temp.vicinity;
	self.address = temp.address;
	self.phoneNumber = temp.phoneNumber;
	self.internationalPhoneNumber = temp.internationalPhoneNumber;
	self.websiteURLString = temp.websiteURLString;
	self.category = temp.category;
	ObjCRelease(handler);
	ObjCRelease(temp);
	NSLogStartMethodMsg(@"%@",self);
}

- (NSComparisonResult) compareDistanceWith:(SearchResult*)res
{
	NSComparisonResult compRes = NSOrderedSame;
	if (res.currentDistance > self.currentDistance)
	{
		compRes = NSOrderedAscending;
	}
	else if (res.currentDistance < self.currentDistance)
	{
		compRes = NSOrderedDescending;
	}
	return compRes;
}

@end
