//
//  SearchResult.h
//  TEST
//
//  Created by hemanth kp on 3/29/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchResult : NSObject
{
	NSInteger searchResultID;
	NSString *placeID;
	NSString *category;
	double lat;
	double lng;
	NSString *name;
	float rating;
	NSString *reference;
	NSString *vicinity;
	NSString *address;
	NSString *phoneNumber;
	NSString *internationalPhoneNumber;
	NSString *websiteURLString;
	NSUInteger lastAccessed;
	double currentDistance;
}

@property (nonatomic) NSInteger searchResultID;
@property (retain, nonatomic) NSString *placeID;
@property (retain, nonatomic) NSString *category;
@property (nonatomic) double lat;
@property (nonatomic) double lng;
@property (retain, nonatomic) NSString *name;
@property (nonatomic) float rating;
@property (retain, nonatomic) NSString *reference;
@property (retain, nonatomic) NSString *vicinity;
@property (retain, nonatomic) NSString *address;
@property (retain, nonatomic) NSString *phoneNumber;
@property (retain, nonatomic) NSString *internationalPhoneNumber;
@property (retain, nonatomic) NSString *websiteURLString;
@property (nonatomic) NSUInteger lastAccessed;
@property (nonatomic) double currentDistance;

- (BOOL) addToDB;
- (void) getID;
- (BOOL) updateDB;
- (BOOL) updateTimeStamp;
- (BOOL) updateReferenceID;
- (void) fetchFromDBWhereName:(NSString *) placeName atLat:(double) lats andLongitude:(double) longi;
- (NSComparisonResult) compareDistanceWith:(SearchResult*)res;

@end
