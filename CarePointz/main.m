//
//  main.m
//  CarePointz
//
//  Created by hemanth kp on 3/30/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CarePointzAppDelegate.h"

int main(int argc, char *argv[])
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
