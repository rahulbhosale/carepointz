/*
 *  SQLiteResultAbstractHandler.h
 *  Mummify
 *
 *  Created by Libin Varghese on 7/14/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#import "SQLiteResultHandler.h"

@interface SQLiteResultAbstractSimpleHandler : NSObject <SQLiteResultHandler>
{
	id _creator ;
	SEL _createItem ;
	NSMutableArray *_columnActionList ;
	id _cacheItem ;
	NSMutableDictionary *_cacheDictionary ;
}

@property (nonatomic, assign) id creator ;
@property (nonatomic, assign) SEL createItem ;
@property (nonatomic, retain) NSArray *columnActionList ;

@end
