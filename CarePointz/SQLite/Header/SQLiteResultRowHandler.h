//
//  SQLiteResultRowHandler.h
//  Mummify
//
//  Created by Libin Varghese on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQLiteResultAbstractHandler.h"

@interface SQLiteResultSimpleRowHandler : SQLiteResultAbstractSimpleHandler <SQLiteResultHandler>
{
	id _outItem ;
}

@property (nonatomic, retain, readonly) id outItem ;

@end

@interface SQLiteResultExistingRowHandler : SQLiteResultSimpleRowHandler <SQLiteResultHandler>
{
	id _item ;
	NSString *_primaryColumn ;
	SEL _getObject ;
}

@end



