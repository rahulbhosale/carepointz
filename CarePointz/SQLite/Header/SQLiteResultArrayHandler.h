//
//  SQLiteResultSimpleArrayHandler.h
//  Mummify
//
//  Created by Libin Varghese on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQLiteResultHandler.h"
#import "SQLiteResultAbstractHandler.h"

@class SQLiteQuery ;

typedef NSMutableArray ColumnActionList ;

@protocol SQLiteResultArrayHandlerDelegate <NSObject>

- (BOOL) query: (SQLiteQuery *) query
   withHandler: (id<SQLiteResultHandler>) handler
 shouldAddItem: (id) item
	 intoArray: (NSArray *) cacheArray ;

- (void) query: (SQLiteQuery *) query
   withHandler: (id<SQLiteResultHandler>) handler
   willAddItem: (id) item
	 intoArray: (NSArray *) cacheArray ;

- (void) query: (SQLiteQuery *) query
   withHandler: (id<SQLiteResultHandler>) handler
	didAddItem: (id) item
	 intoArray: (NSArray *) cacheArray ;

@end


@interface SQLiteResultSimpleArrayHandler : SQLiteResultAbstractSimpleHandler <SQLiteResultHandler>
{
@protected	
	id<SQLiteResultArrayHandlerDelegate> _delegate ;
	NSMutableArray *_cacheArray ;
	NSMutableArray *_outArray ;
}

@property (nonatomic, assign) id<SQLiteResultArrayHandlerDelegate> delegate ;
@property (nonatomic, retain, readonly) NSMutableArray *outArray ;

@end

@interface SQLiteResultExistingArrayHandler : SQLiteResultSimpleArrayHandler <SQLiteResultHandler>
{
	NSArray *_arr ;
	NSString *_primaryColumn ;
	SEL _objectFromArray ;
}

@end