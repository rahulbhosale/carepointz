//
//  SQLiteManager.h
//  Mummify
//
//  Created by Libin Varghese on 5/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

#import "SQLiteResultHandler.h"
#import "SQLiteResultArrayHandler.h"
#import "SQLiteResultRowHandler.h"


typedef NSMutableArray SQLiteReuseableQueryList ;

#define SQLITE_TYPEVALUE	0
#define SQLITE_TYPECOUNT	SQLITE_NULL + 1
#define SQLITE_TYPEOBJECT	SQLITE_TYPECOUNT + 1

@class SQLiteQuery ;
@class SQLiteScript ;
typedef NSArray SQLiteEncodingList ;

@interface SQLiteManager : NSObject
{
@protected
	NSString *_path ;
	sqlite3 *_db ;
	SQLiteReuseableQueryList *_reuseList ;
}

- (id) initWithDB: (NSString *) path ;
- (SQLiteQuery *) queryWithQueryString: (NSString *) query
				   withReuseIdentifier: (NSString *) identifier
							withFormat: (char *) var1format, ... NS_REQUIRES_NIL_TERMINATION ;
- (SQLiteQuery *) reusableQueryWithIdentifier: (NSString *) identifer ;
- (void) executeScriptFromString: (NSString *) script ;
- (void) executeScriptFromFile: (NSString *) file ;
- (void) closeDB ;
- (void) reopenDB ;

@end

@interface SQLiteQuery : NSObject
{
@protected
	SQLiteManager *_mgr ;
	NSString *_queryStr ;
	sqlite3_stmt *_query ;
	NSString *_reuseID ;
	SQLiteEncodingList *_format ;
}

- (void) executeWithHandler: (id<SQLiteResultHandler>) handler ;
- (void) executeWithHandlerAndArgs: (id<SQLiteResultHandler>) handler, ... ;
- (void) executeWithHandler: (id<SQLiteResultHandler>) handler
				  reuseArgs: (BOOL) shouldReuse ;

- (void) executeAsync: (id<SQLiteHandlerAsyncDelegate>) delegate
		  withHandler: (id<SQLiteResultHandler>) handler ;
- (void) executeAsync: (id<SQLiteHandlerAsyncDelegate>) delegate
		  withHandler: (id<SQLiteResultHandler>) handler
			 withArgs: (id) firstArg, ... ;

@end

