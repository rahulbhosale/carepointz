/*
 *  SQLiteResultHandler.h
 *  Mummify
 *
 *  Created by Libin Varghese on 6/30/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

@class SQLiteQuery ;

@protocol SQLiteResultHandler <NSObject>

@optional
- (void) didStartExecutingQuery: (SQLiteQuery *) query ;

- (void) query: (SQLiteQuery *) query didStartRow: (NSUInteger) row ;

- (void) query: (SQLiteQuery *) query gotNULLForColumn: (NSString *) name ;
- (void) query: (SQLiteQuery *) query gotColumn: (NSString *) name
	 withValue: (NSValue *) value ;
- (void) query: (SQLiteQuery *) query gotColumn: (NSString *) name
   withInteger: (NSInteger) integer ;
- (void) query: (SQLiteQuery *) query gotColumn: (NSString *) name
	  withReal: (double) real ;
- (void) query: (SQLiteQuery *) query gotColumn: (NSString *) name
	withString: (NSString *) string ;
- (void) query: (SQLiteQuery *) query gotColumn: (NSString *) name
	  withBlob: (NSData *) data ;

- (void) query: (SQLiteQuery *) query didEndRow: (NSUInteger) row ;

- (void) didEndExecutingQuery: (SQLiteQuery *) query ;

@end

@protocol SQLiteHandlerAsyncDelegate <NSObject>

- (void) didExecuteQuery: (SQLiteQuery *) query
			 withHandler: (id<SQLiteResultHandler>) handler ;
- (void) failedExecutingQuery: (SQLiteQuery *) query
				  withHandler: (id<SQLiteResultHandler>) handler
					withError: (NSError *) error ;

@end

@interface NSMutableArray (ColumnActionList)

- (id) initWithColumnActionsWithNULLAction: (NSInteger) count, ... ;
- (NSUInteger) indexOfColumnActionWithColumn: (NSString *) column ;
- (id) columnActionWithColumn: (NSString *) column ;

@end



