//
//  ColumnAction.m
//  Mummify
//
//  Created by Libin Varghese on 7/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ColumnAction.h"
#import <safeObjC.h>

#define ISODD(val)	((val) & 0x1 == 0x1)
#define ISEVEN(val)	(!ISODD(val))

@implementation ColumnAction

@synthesize column = _column ;
@synthesize action = _action ;
@synthesize nullAction = _nullAction ;

- (void) dealloc
{
	self.column = nil ;
	self.action = nil ;
	
	[super dealloc] ;
}

- (id) initWithColumn: (NSString *) column action: (SEL) action nullAction: (SEL) nullAction 
{
	self = [super init] ;
	if( self ) 
	{
		self.column = column ;
		self.action = action ;
		self.nullAction = nullAction ;
	}
	
	return self ;
}

@end

@implementation NSMutableArray (ColumnActionList)

- (id) initWithColumnActions: (NSInteger) count, ...
{
	NSMutableArray *retArr = nil ;
	void *pArg = NULL ;
	NSString *str = nil ;
	ColumnAction *colAct = nil ;
	NSInteger i = 0 ;
	SEL action ;
	SEL nullAction ;
	va_list argList ;
	
	CkValueInvalidArgEx( count, @"Should contain atleast one column, action" ) ;
	
	va_start( argList, count ) ;
	retArr = [[NSMutableArray alloc] init] ;

	pArg = va_arg( argList, NSString * ) ;
	
	while( i != (count * 3) )
	{
		switch( i % 3 )
		{
			case 0:		// Column Name
			{
				str = (NSString *) pArg ;
				pArg = va_arg( argList, SEL) ;
				
				break ;
			}
			
			case 1:		// Action Selector
			{
				action = (SEL) pArg ;
				pArg = va_arg( argList, SEL) ;
				
				break ;
			}
				
			case 2:
			{
				nullAction = (SEL) pArg ;
				pArg = va_arg( argList, NSString * ) ;				
				colAct = [[ColumnAction alloc] initWithColumn: str
													   action: action
												   nullAction: nullAction] ;
				[retArr addObject: colAct] ;
				ObjCRelease( colAct ) ;
			}
		}
		i ++ ;
	}
	
	va_end( argList ) ;
	
	count = [retArr count] ;
	if( count == 0 )
	{
		ObjCRelease( retArr ) ;
	}
	
    ObjCRelease(self);
	self = retArr ;
	
	return self ;
}

- (NSUInteger) indexOfColumnActionWithColumn: (NSString *) column
{
	ColumnAction *colAct = nil ;
	NSUInteger count = 0 ;
	NSUInteger i = 0 ;
	NSUInteger index = NSNotFound ;
	
	count = [self count] ;
	for( i = 0; i < count && index == NSNotFound; i ++ )
	{
		colAct = [self objectAtIndex: i] ;
		if( [colAct.column isEqualToString: column] )
		{
			index = i ;
		}
	}
	
	return index ;
}

- (id) columnActionWithColumn: (NSString *) column
{
	ColumnAction *colAct = nil ;
	ColumnAction *retColAct = nil ;
	NSUInteger count = 0 ;
	NSUInteger i = 0 ;
	
	count = [self count] ;
	for( i = 0; i < count && retColAct == nil; i ++ )
	{
		colAct = [self objectAtIndex: i] ;
		if( [colAct.column isEqualToString: column] )
		{
			retColAct = colAct ;
		}
	}
	
	return retColAct ;
}

@end

