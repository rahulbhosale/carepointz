//
//  SQLiteScript.m
//  Mummify
//
//  Created by Libin Varghese on 7/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SQLiteManager+Private.h"
#import <safeObjC.h>

@implementation SQLiteScript

@synthesize mgr_ = _mgr ;
@synthesize scriptStr_ = _scriptStr ;

- (void) dealloc
{
	self.mgr_ = nil ;
	self.scriptStr_ = nil ;
	
	[super dealloc] ;
}

- (id) _initFromString: (NSString *) script withManager: (SQLiteManager *) mgr
{
	CkObjInvalidArgEx( script, @"Script cannot be nil" ) ;
	CkObjInvalidArgEx( mgr, @"SQLite Manager cannot be nil" ) ;
	self = [super init] ;
	if( self )
	{
		self.scriptStr_ = script ;
		self.mgr_ = mgr ;
	}
	
	return self ;
}

- (id) _initFromFile: (NSString *) file withManager: (SQLiteManager *) mgr
{
	NSString *str = nil ;
	
	CkObjInvalidArgEx( file, @"Script File cannot be nil" ) ;
	CkObjInvalidArgEx( mgr, @"SQLite Manager cannot be nil" ) ;
	
	str = [[NSString alloc] initWithContentsOfFile: file] ;
	
	self = [self _initFromString: str withManager: mgr] ;
	
	return self ;
}

- (void) _execute
{
	const char *script = NULL ;
	char *errStr = NULL ;
	SQLiteError err ;
	
	script = [self.scriptStr_ cStringUsingEncoding: NSUTF8StringEncoding] ;
	err = sqlite3_exec( self.mgr_.db_, script, NULL, NULL, &errStr ) ;
	CkSQLiteErrEx( err, self.mgr_ ) ;
}

@end
