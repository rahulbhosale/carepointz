//
//  SQLiteReuseableQueryList.m
//  Mummify
//
//  Created by Libin Varghese on 6/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SQLiteManager+Private.h"


@implementation NSMutableArray (SQLiteReuseableQueryList)

- (SQLiteQuery *) queryWithIdentifer: (NSString *) identifier ;
{
	NSUInteger count = 0 ;
	NSUInteger i = 0 ;
	SQLiteQuery *q = nil ;
	
	count = [self count] ;
	for( i = 0 ; i < count && q == nil ; i ++ )
	{
		q = [self objectAtIndex: i] ;
		if( ![q.reuseID_ isEqualToString: identifier] )
		{
			q = nil ;
		}
	}
	
	return q ;
}

- (NSUInteger) indexOfQueryWithIdentifer: (NSString *) identifier
{
	NSUInteger count = 0 ;
	NSUInteger i = 0 ;
	SQLiteQuery *q = nil ;
	NSUInteger index = NSNotFound ;
	
	count = [self count] ;
	for( i = 0 ; i < count && index == NSNotFound ; i ++ )
	{
		q = [self objectAtIndex: i] ;
		if( [q.reuseID_ isEqualToString: identifier] )
		{
			index = i ;
		}
	}
	
	return index ;
}

@end
