//
//  SQLiteResultAbstractSimpleHandler.m
//  Mummify
//
//  Created by Libin Varghese on 7/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "SQLiteResultAbstractHandler.h"
#import "SQLiteResultAbstractHandler+Private.h"
#import "ColumnAction.h"
#import <safeObjC.h>

@implementation SQLiteResultAbstractSimpleHandler

#pragma mark -
#pragma mark Private

- (void) _query: (SQLiteQuery *) query gotColumn: (NSString *) name
	 withObject: (id) obj
{
	ColumnAction *colAct = nil ;
	NSInvocation *inv = nil ;
	NSMethodSignature *metSig = nil ;
	NSNumber *num = nil ;
	
	//NSLogStartMethodMsg( "Column %@ = %@", name, obj) ;
	
	union
	{
		NSInteger iVal ;
		double dVal ;
	} number ;
	
	colAct = [self.columnActionList_ columnActionWithColumn: name] ;
	if( self.cacheItem_ )
	{
		if( [self.cacheItem_ respondsToSelector: colAct.action] )
		{
			if( [obj isKindOfClass: [NSNumber class]] )
			{
				num = (NSNumber *) obj ;
				[num getValue: &number] ;
				metSig = [self.cacheItem_ methodSignatureForSelector: colAct.action] ;
				inv = [NSInvocation invocationWithMethodSignature: metSig] ;
				[inv setArgument: &number atIndex: 2] ;
				[inv setSelector: colAct.action] ;
				//SafeLog( @"%@", inv ) ;
				[inv invokeWithTarget: self.cacheItem_] ;
			}
			else
			{
				[self.cacheItem_ performSelector: colAct.action withObject: obj] ;
			}
		}
	}
	else
	{
		[self.cacheDictionary_ setObject: obj forKey: colAct.column] ;
	}
}


#pragma mark -
#pragma mark synthesize
@synthesize creator = _creator ;
@synthesize createItem = _createItem ;
#pragma mark Private
@synthesize columnActionList_ = _columnActionList ;
@synthesize cacheItem_ = _cacheItem ;
@synthesize cacheDictionary_ = _cacheDictionary ;

- (void) dealloc
{
	self.creator = nil ;
	self.createItem = NULL ;
	self.cacheItem_ = nil ;
	self.cacheDictionary_ = nil ;
	self.columnActionList_ = nil ;
	
	[super dealloc] ;
}

- (NSArray *) columnActionList
{
	NSArray *retArr  = nil ;
	
	retArr = [self.columnActionList_ copy] ;
	
	return [retArr autorelease] ;
}

- (void) setColumnActionList:(NSArray *) arr
{
    NSMutableArray *temp = nil;
    temp = [arr mutableCopy];
	self.columnActionList_ = temp;
    ObjCRelease(temp);
}

#pragma mark SQLiteResultHandler
- (void) query: (SQLiteQuery *) query didStartRow: (NSUInteger) row
{
	NSMutableDictionary *dict = nil ;
	id item = nil ;
	
	if( self.creator && self.createItem != NULL
	   && [self.creator respondsToSelector: self.createItem] )
	{
		item = [self.creator performSelector: self.createItem] ;
		if( item )
		{
			self.cacheItem_ = item ;
			ObjCRelease( item )  ;
		}
	}
    
	if( self.cacheItem_ == nil )
	{
		dict = [[NSMutableDictionary alloc] init] ;
		self.cacheDictionary_ = dict ;
		ObjCRelease( dict ) ;
	}
}

- (void) query: (SQLiteQuery *) query gotNULLForColumn: (NSString *) name
{
	ColumnAction *colAct = nil ;
	NSString *str = nil ;
	BOOL takesNilArgument = NO ;
	
	colAct = [self.columnActionList_ columnActionWithColumn: name] ;
	if( self.cacheItem_ )
	{
		if( [self.cacheItem_ respondsToSelector: colAct.nullAction] )
		{
			str = NSStringFromSelector( colAct.nullAction ) ;
			takesNilArgument = [str hasSuffix:@":"] ;
			if( takesNilArgument )
			{
				[self.cacheItem_ performSelector: colAct.nullAction withObject: nil] ;
			}
			else
			{
				[self.cacheItem_ performSelector: colAct.nullAction] ;
			}
		}
	}
	// For cacheDictionary_ dont add the key
}

- (void) query: (SQLiteQuery *) query gotColumn: (NSString *) name
   withInteger: (NSInteger) integer
{
	NSNumber *intObj = nil ;
	intObj = [[NSNumber alloc] initWithInteger: integer] ;
	[self _query: query gotColumn: name withObject: intObj] ;
	ObjCRelease( intObj ) ;
}

- (void) query: (SQLiteQuery *) query gotColumn: (NSString *) name
	  withReal: (double) real
{
	NSNumber *realObj = nil ;
	realObj = [[NSNumber alloc] initWithDouble: real] ;
	[self _query: query gotColumn: name withObject: realObj] ;
	ObjCRelease( realObj ) ;
}

- (void) query: (SQLiteQuery *) query gotColumn: (NSString *) name
	withString: (NSString *) string
{
	[self _query: query gotColumn: name withObject: string] ;
}

- (void) query: (SQLiteQuery *) query gotColumn: (NSString *) name
	  withBlob: (NSData *) data
{
	[self _query: query gotColumn: name withObject: data] ;
}

@end
