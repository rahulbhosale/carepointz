//
//  SQLiteResultSimpleArrayHandler.m
//  Mummify
//
//  Created by Libin Varghese on 7/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SQLiteResultArrayHandler.h"
#import "SQLiteResultAbstractHandler+Private.h"
#import <safeObjC.h>

@interface SQLiteResultSimpleArrayHandler ()

@property (nonatomic, retain) NSMutableArray *cacheArray_ ;
@property (nonatomic, retain, readwrite) NSMutableArray *outArray ;

@end


@implementation SQLiteResultSimpleArrayHandler

#pragma mark -
#pragma mark Private
@synthesize cacheArray_ = _cacheArray ;


#pragma mark -
#pragma mark synthesize
@synthesize delegate = _delegate ;
@synthesize outArray = _outArray ;

- (void) dealloc
{
	self.delegate = nil ;
	self.cacheArray_ = nil ;
	self.outArray = nil ;
	
	[super dealloc] ;
}

#pragma mark SQLiteResultHandler
- (void) didStartExecutingQuery: (SQLiteQuery *) query
{
	NSMutableArray *arr = nil ;
	arr = [[NSMutableArray alloc] init] ;
	
	self.cacheArray_ = arr ;
	
	ObjCRelease( arr ) ;
}

- (void) query: (SQLiteQuery *) query didEndRow: (NSUInteger) row
{
	id obj = nil ;
	
	obj = self.cacheDictionary_ ;
	if( self.cacheItem_ )
	{
		obj = self.cacheItem_ ;
	}
	
	[self.cacheArray_ addObject: obj] ;
	
	self.cacheItem_ = nil ;
	self.cacheDictionary_ = nil ;
}

- (void) didEndExecutingQuery: (SQLiteQuery *) query
{
	self.outArray = self.cacheArray_ ;
	self.cacheArray_ = nil ;
}

@end
