//
//  SQLiteResultSimpleRowHandler.m
//  Mummify
//
//  Created by Libin Varghese on 7/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "SQLiteResultRowHandler.h"
#import "SQLiteResultAbstractHandler+Private.h"
#import <safeObjC.h>

@interface SQLiteResultSimpleRowHandler ()

@property (nonatomic, retain, readwrite) id outItem ;

@end

@implementation SQLiteResultSimpleRowHandler

#pragma mark -
#pragma mark synthesize
@synthesize outItem = _outItem ;

- (void) dealloc
{
	self.outItem = nil ;
	
	[super dealloc] ;
}

#pragma mark SQLiteResultHandler
- (void) query: (SQLiteQuery *) query didStartRow: (NSUInteger) row
{
	CkConditionEx( Condition( row >= 1 ), NSInternalInconsistencyException,
				  @"Invalid handler for a query which produced more than one rows" ) ;

	[super query: query didStartRow: row] ;
}

- (void) query: (SQLiteQuery *) query didEndRow: (NSUInteger) row
{
	id obj = nil ;
	
	obj = self.cacheDictionary_ ;
	if( self.cacheItem_ )
	{
		obj = self.cacheItem_ ;
	}

	self.outItem = obj ;
	
	self.cacheItem_ = nil ;
	self.cacheDictionary_ = nil ;
}

@end
