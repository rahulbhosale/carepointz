//
//  SQLiteQuery.m
//  Mummify
//
//  Created by Libin Varghese on 5/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "SQLiteManager.h"
#import "SQLiteManager+Private.h"
#import <safeObjC.h>

#define SQLiteAllowedDataBinding	{	\
	@encode( NSInteger ) ,	\
	@encode( id ) ,	\
	@encode( double )	\
	}
#define SQLiteAllowedDataBindingCount	3


void _freePFieldInfoList( HandlerFieldInfo **pFieldInfoList ) ;

#pragma mark -
#pragma mark C Methods
void _freePFieldInfoList( HandlerFieldInfo **pFieldInfoList )
{
	NSInteger i = 0 ;
	
	for( i = 0 ; i < SQLITE_TYPECOUNT ; i ++ )
	{
		CFreeFn( pFieldInfoList[i], free ) ;
	}
	
	CFreeFn( pFieldInfoList, free ) ;
}

#pragma mark -
@implementation NSArray (SQLiteEncodingList)

- (SQLiteDataType) typeAtIndex: (NSUInteger) index
{
	NSString *str = nil ;
	NSInteger i = 0 ;
	NSInteger found = NSNotFound ;
	SQLiteDataType type = SQLITE_NULL ;
	const char *cStr = nil ;
	char *encodingList[SQLiteAllowedDataBindingCount] = SQLiteAllowedDataBinding ;
					   
	str = [self objectAtIndex: index] ;
	cStr = [str cStringUsingEncoding: NSUTF8StringEncoding] ;
					   
	for( i = 0 ; i < SQLiteAllowedDataBindingCount && found == NSNotFound ; i ++ )
	{
		if( strcmp( cStr, encodingList[i] ) == 0 )
		{
			found = i ;
		}
	}
	
	switch( found )
	{
		case 0:
		{
			type = SQLITE_INTEGER ;
			break;
		}
			
		case 1:
		{
			type = SQLITE_TYPEOBJECT ;
			break ;
		}			
			
		case 2:
		{
			type = SQLITE_FLOAT ;
			break ;
		}
						
		default:
		{
			CkSwitchCaseIIEx( @"Encoding type", found ) ;
			break;
		}
	}
	
	return type ;
}
					   
@end
					   

#pragma mark -
@implementation SQLiteQuery

#pragma mark -
#pragma mark Private

@synthesize mgr_ = _mgr ;
@synthesize queryStr_ = _queryStr ;
@synthesize query_ = _query ;
@synthesize reuseID_ = _reuseID ;
@synthesize format_ = _format ;

- (void) dealloc
{
	self.query_ = NULL ;
	self.mgr_ = nil ;
	self.queryStr_ = nil ;
	self.format_ = nil ;
	self.reuseID_ = nil ;
	
	[super dealloc] ;
}

- (void) setQuery_:(sqlite3_stmt *) pStmt
{
	SQLiteError err = SQLITE_OK ;
	
	if( pStmt != _query )
	{
		err = sqlite3_finalize( _query ) ;
		CkSQLiteErrLog( err, self.mgr_ ) ;
		_query = pStmt ;
	}
}

/* NSInteger double Object */
- (NSArray *) _createFormat: (va_list) format withFirstArg: (char *) firstArg
{
	NSMutableString *str = nil ;
	NSMutableArray *formatArr = nil ;
	NSArray *retArr = nil ;
	NSInteger len = 0 ;
	NSInteger i = 0 ;
	NSInteger isSame = NSOrderedAscending ;
	char *ptr = NULL ;
	char *allowedEncoding[SQLiteAllowedDataBindingCount] = SQLiteAllowedDataBinding ;
			
	formatArr = [[NSMutableArray alloc] init] ;
	
	ptr = firstArg ;
	while( ptr != NULL )
	{
		len = strlen( ptr ) ;
		CkConditionInvalidArgEx( Condition( len == 0 ),
								@"Format arguments passed are invalid" )  ;
		
		isSame = NSOrderedAscending ;
		for( i = 0 ;
			i < SQLiteAllowedDataBindingCount && isSame != NSOrderedSame ;
			i++ )
		{
			isSame = strcmp( ptr, allowedEncoding[i] ) ;
		}
		
		CkConditionInvalidArgEx( Condition( isSame != NSOrderedSame ),
								@"Does not support the given format" ) ;
		
		str = [[NSMutableString alloc] initWithCString: ptr 
											  encoding: NSUTF8StringEncoding] ;
		[formatArr addObject: str] ;
		ObjCRelease( str ) ;
		
		ptr = va_arg( format, char * ) ;
	}
										   
	len = [formatArr count] ;
	if( len == 0 )
	{
		ObjCRelease( formatArr ) ;
	}
	
	retArr = [formatArr copy] ;
	ObjCRelease( formatArr ) ;
	
	return retArr ;
}

- (id) _initWithQuery: (NSString *) query
		  withManager: (SQLiteManager *) mgr
  withReuseIdentifier: (NSString *) identifier
		 withFirstArg: (char *) firstArg
		   withFormat: (va_list) format
{
	NSUInteger index = NSNotFound ;
	NSArray *formatArr = nil ;
	
	self = [super init] ;
	if( self )
	{
		@try
		{
			CkObjSQLiteEx( mgr, @"Invalid Manager" ) ;
			CkPtrSQLiteEx( mgr.db_, @"Invalid Manager: Database not open" ) ;

			self.mgr_ = mgr ;
			formatArr = [self _createFormat: format withFirstArg: firstArg] ;
			[self _loadQuery: query withFormat: formatArr] ;
			ObjCRelease( formatArr ) ;
			
			self.reuseID_ = identifier ;

			index = [mgr.reuseList_ indexOfQueryWithIdentifer: identifier] ;
			if( index == NSNotFound )
			{
				index = [mgr.reuseList_ count] ;
			}
			[mgr.reuseList_ insertObject: self atIndex: index] ;
		}
		@catch( NSException *exp )
		{
			ObjCRelease( self ) ;
			@throw exp ;
		}
	}
	
	return self ;
}

- (void) _clearContext
{
	self.query_ = NULL ;
	self.mgr_ = nil ;
}

- (void) _clearQuery
{
	self.query_ = NULL ;
}

- (void) _setContextWithManager: (SQLiteManager *) mgr
{
	CkObjSQLiteEx( mgr, @"Invalid Manager" ) ;
	CkConditionSQLiteEx( ![mgr.reuseList_ containsObject: self],
						@"Cannot change context of a query created from one database to another" ) ; 
	CkPtrSQLiteEx( mgr.db_, @"Invalid Manager: Database not open" ) ;
	
	[self _loadQuery: self.queryStr_ withFormat: self.format_] ;
	
	self.mgr_ = mgr ;
}

- (void) _loadQuery: (NSString *) query withFormat: (SQLiteEncodingList *) format
{
	const char *qStr = NULL ;
	sqlite3_stmt *pStmt = NULL ;
	NSInteger len = 0 ;
	SQLiteError err = SQLITE_OK ;
	NSInteger count = 0 ;
	NSInteger formatCount = 0 ;

	CkObjSQLiteEx( query, @"Invalid Query" ) ;
	
	qStr = [query cStringUsingEncoding: NSUTF8StringEncoding] ;
	CkPtrSQLiteEx( qStr, @"Invalid Query" ) ;
	len = strlen( qStr ) ;
	CkValueSQLiteEx( len, @"Invalid Query" ) ;
	err = sqlite3_prepare( self.mgr_.db_, qStr, len, &pStmt, NULL ) ;
	CkSQLiteErrEx( err, self.mgr_ ) ;
	
	@try
	{
		count = sqlite3_bind_parameter_count( pStmt ) ;
		formatCount = [format count] ;
		CkConditionSQLiteEx( count != formatCount , 
							@"Invalid Query Format Binding" ) ;
	}
	@catch( NSException *exp )
	{
		err = sqlite3_finalize( pStmt ) ;
		CkSQLiteErrLog( err, self.mgr_ ) ;
		@throw exp ;
	}
	// Check if sqlite3_bind_parameter_count matches with format count
	
	self.query_ = pStmt ;
	self.queryStr_ = query ;
	self.format_ = format ;
}

- (void) _validateQuery
{
	SQLiteError err = SQLITE_OK ;
	
	CkObjSQLiteEx( self.mgr_, @"Invalid Manager" ) ;
	if( self.query_ == nil )
	{
		[self _loadQuery: self.queryStr_ withFormat: self.format_] ;
	}
	else
	{		
		err = sqlite3_reset( self.query_ ) ;
		CkSQLiteErrLog( err, self.mgr_ ) ;
	}
}

- (void *) _dataPtrForColumn: (NSInteger) column
					withType: (SQLiteDataType) type
{
	NSInteger iVal = 0 ;
	NSInteger size = 0 ;
	NSInteger objSize = 0 ;
	double dVal = 0.0 ;
	const unsigned char *cStr = NULL ;
	NSString *str = nil ;
	NSData *data = nil ;
	const Byte *blob = NULL ;
	void *ptr = NULL ;
	void *retPtr = nil ;

	switch( type )
	{
		case SQLITE_INTEGER:
		{
			objSize = sizeof( NSInteger ) ;
			iVal = sqlite3_column_int( self.query_, column ) ;
			ptr = &iVal ;
			
			break;
		}
			
		case SQLITE_FLOAT:
		{
			objSize = sizeof( double ) ;
			dVal = sqlite3_column_double( self.query_, column ) ;
			ptr = &dVal ;
			
			break;
		}
			
		case SQLITE_TEXT:
		{
			objSize = sizeof( NSString * ) ;
			cStr = sqlite3_column_text( self.query_, column ) ;
			// TODO : Check if releasing value will release string
			str = [[NSString  alloc] initWithCString: (char *)cStr encoding: NSUTF8StringEncoding] ;
			ptr = &str ;
			
			break;
		}
			
		case SQLITE_BLOB:
		{
			objSize = sizeof( NSData * ) ;
			blob = sqlite3_column_blob( self.query_, column ) ;
			size = sqlite3_column_bytes( self.query_, column ) ;
			// TODO : Check if releasing value will release Data
			data = [[NSData alloc] initWithBytes: blob length: size] ;
			ptr = &data ;
			
			break;
		}
		
		case SQLITE_NULL:
		{
			objSize = 0 ;
			ptr = NULL ;
			break ;
		}
			
		default:
		{
			CkSwitchCaseIIEx( @"columnType", type ) ;
			break;
		}
	}
	
	if(ptr)
	{
		retPtr = calloc( 1, objSize ) ;
		memcpy( retPtr, ptr, objSize ) ;
	}
	
	return retPtr ;
}

- (void) _executeRowWithHandler: (id<SQLiteResultHandler>) handler
					   withInfo: (HandlerFieldInfo **) pFieldInfoList
{
	NSInteger colCount = 0 ;
	NSInteger i = 0 ;
	NSInteger argCount = 0 ;
	const char *columnCStr = NULL ;
	NSString *column = nil ;
	NSValue *val = nil ;
	SQLiteDataType type = SQLITE_TYPEVALUE ;
	void *ptr = NULL ;
    void *dataPtr = NULL;
	HandlerFieldInfo *pFieldInfo = NULL ;
	NSMethodSignature *metSig = nil ;
	NSInvocation *inv = nil ;
	id obj = nil ;
	
	colCount = sqlite3_column_count( self.query_ ) ;
	for( i = 0 ; i < colCount; i ++ )
	{
		type = sqlite3_column_type( self.query_, i ) ;
		
		pFieldInfo = pFieldInfoList[type] ;
		
		ptr = [self _dataPtrForColumn: i withType: type] ;
        dataPtr = ptr;
//		if( type == SQLITE_TEXT || type == SQLITE_BLOB )
//		{
//			obj = *((id *)ptr) ;
//		}
				
		if( pFieldInfo == NULL || !pFieldInfo->respondsToType )
		{
			val = nil ;
			if(dataPtr)
			{
				val = [[NSValue alloc] initWithBytes: dataPtr objCType: pFieldInfo->type] ;
			}
			type = SQLITE_TYPEVALUE ;
 			dataPtr = &val ;
			pFieldInfo = pFieldInfoList[type] ;
		}
		
		if( pFieldInfo && pFieldInfo->respondsToType )
		{			
			columnCStr = sqlite3_column_name( self.query_, i ) ;
			column = [[NSString alloc] initWithCString: columnCStr encoding: NSUTF8StringEncoding] ;

			metSig = [handler methodSignatureForSelector: pFieldInfo->queryAction] ;
			
			inv = [NSInvocation invocationWithMethodSignature: metSig] ;
			[inv setSelector: pFieldInfo->queryAction] ;
			[inv setTarget: handler] ;
			[inv setArgument: &self atIndex: 2] ;
			[inv setArgument: &column atIndex: 3] ;
			
			argCount = [metSig numberOfArguments] ;
			if( argCount >= 5 )
			{
				[inv setArgument: dataPtr atIndex: 4] ;
			}
			
			[inv invoke] ;
		}
		if( type == SQLITE_TEXT || type == SQLITE_BLOB )
		{
			obj = *((id *)ptr) ;
            ObjCRelease(obj);
		}        

		if( type == SQLITE_TYPEVALUE )
        {
            ObjCRelease(val);
        }
        CFreeFn( ptr, free );
		ObjCRelease( val ) ;
		ObjCRelease( column ) ;
	}
}

- (HandlerFieldInfo **) _createHandlerFieldInfoForHandler: (id<SQLiteResultHandler>) handler
{
	HandlerFieldInfo **pFieldInfoList = NULL ;
	HandlerFieldInfo *pFieldInfo = NULL ;
	NSInteger i = 0 ;
	
	CkObjInvalidArgEx( handler, @"handler can't be NULL" ) ;

	pFieldInfoList = (HandlerFieldInfo **) calloc( SQLITE_TYPECOUNT,
												  sizeof( HandlerFieldInfo *) ) ;
	for( i = 0 ; i < SQLITE_TYPECOUNT ; i++ )
	{
		pFieldInfo = (HandlerFieldInfo *) calloc( 1,
												 sizeof( HandlerFieldInfo ) ) ;
		switch( i )
		{
			case SQLITE_INTEGER:
			{
				pFieldInfo->queryAction = @selector( query:gotColumn:withInteger: ) ;
				pFieldInfo->type = @encode( int ) ;

				break;
			}
				
			case SQLITE_FLOAT:
			{
				pFieldInfo->queryAction = @selector( query:gotColumn:withReal: ) ;
				pFieldInfo->type = @encode( double ) ;
				
				break;
			}
				
			case SQLITE_TEXT:
			{
				pFieldInfo->queryAction = @selector( query:gotColumn:withString: ) ;
				pFieldInfo->type = @encode( NSString * ) ;
				
				break;
			}
				
			case SQLITE_BLOB:
			{
				pFieldInfo->queryAction = @selector( query:gotColumn:withBlob: ) ;
				pFieldInfo->type = @encode( NSData * ) ;
				
				break ;
			}

			case SQLITE_TYPEVALUE:
			{
				pFieldInfo->queryAction = @selector( query:gotColumn:withValue: ) ;
				pFieldInfo->type = @encode( NSValue * ) ;
				
				break ;
			}
			
			case SQLITE_NULL:
			{
				pFieldInfo->queryAction = @selector( query:gotNULLForColumn: ) ;
				pFieldInfo->type = NULL ;
				break ;
			}
				
			default:
			{
				CkSwitchCaseIIEx( @"SQLiteDataType", i ) ;
				
				break ;
			}
		}
		
		pFieldInfo->respondsToType = [handler respondsToSelector: pFieldInfo->queryAction] ;

		pFieldInfoList[i] = pFieldInfo ;
	}
	
	return pFieldInfoList ;
}

#pragma mark -
#pragma mark Methods

- (void) executeWithHandler: (id<SQLiteResultHandler>) handler
				  reuseArgs: (BOOL) shouldReuse
{
	NSInteger err = SQLITE_OK ;
	NSInteger count = 0 ;
	HandlerFieldInfo **pFieldInfoList = NULL ;
	BOOL didStartRow = NO ;
	BOOL didEndRow = NO ;
	
	[self _validateQuery] ;
	
	if( !shouldReuse )
	{
		err = sqlite3_clear_bindings( self.query_ ) ;
		CkSQLiteErrLog( err, self.mgr_ ) ;
		
		count = sqlite3_bind_parameter_count( self.query_ ) ;
		CkConditionSQLiteEx( (count != 0), 
							@"Call [SQliteQuery executeIntoObject:withArgs:]" ) ;
	}
	
	if( handler )
	{
		pFieldInfoList = [self _createHandlerFieldInfoForHandler: handler] ;
		didStartRow = [handler respondsToSelector: @selector( query:didStartRow: )] ;
		didEndRow = [handler respondsToSelector: @selector( query:didEndRow: )] ;
	}
	
	err = sqlite3_step( self.query_ ) ;
	while( err == SQLITE_ROW )
	{
		if( handler )
		{
			if( count == 0 )
			{
				if( [handler respondsToSelector: @selector( didStartExecutingQuery: )] )
				{
					[handler didStartExecutingQuery: self] ;
				}
			}
			
			if( didStartRow )
			{
				[handler query: self didStartRow: count] ;
			}
			
			[self _executeRowWithHandler: handler withInfo: pFieldInfoList] ;
			
			if( didEndRow )
			{
				[handler query: self didEndRow: count] ;
			}
		}
		
		err = sqlite3_step( self.query_ ) ;
		count ++ ;
	}
	
	@try 
	{
		CkSQLiteConditionEx( err != SQLITE_DONE, self.mgr_ ) ;
		
		if( err == SQLITE_DONE && count != 0 )
		{
			if( [handler respondsToSelector: @selector( didEndExecutingQuery: )] )
			{
				[handler didEndExecutingQuery: self] ;
			}
		}
	}
	@catch (NSException * e) 
	{
		@throw e ;
	}
	@finally 
	{
		err = sqlite3_reset( self.query_ ) ;
		CkSQLiteErrLog( err, self.mgr_ ) ;
		
		CFreeFn( pFieldInfoList, _freePFieldInfoList ) ;
	}
}

- (void) executeWithHandler: (id<SQLiteResultHandler>) handler ;
{
	[self executeWithHandler: handler reuseArgs: NO] ;
}

- (void) executeWithHandlerAndArgs: (id<SQLiteResultHandler>) handler, ...
{
	double dVal = 0.0 ;
	id obj = nil ;
	NSString *str = nil ;
	NSData *data = nil ;
	const char *cStr = nil ;
	const void *bytes = nil ;
	SQLiteDataType type = SQLITE_NULL ;
	SQLiteError err = SQLITE_OK ;
	NSUInteger count = 0 ;
	NSInteger bindCount = 0 ;
	NSInteger i = 0 ;
	NSInteger iVal = 0 ;
	NSInteger length = 0 ;
	
	va_list argList ;

	[self _validateQuery] ;
	
	err = sqlite3_clear_bindings( self.query_ ) ;
	CkSQLiteErrLog( err, self.mgr_ ) ;
	
	va_start( argList, handler ) ;
	
	bindCount = sqlite3_bind_parameter_count( self.query_ ) ;
	count = [self.format_ count] ;
	
	for( i = 0 ; i < count ; i ++ )
	{
		type = [self.format_ typeAtIndex: i] ;
		if( type == SQLITE_TYPEOBJECT )
		{
			obj = va_arg( argList, id ) ;
			if( [obj isKindOfClass: [NSString class]] )
			{
				type = SQLITE_TEXT ;
			}
			else if( [obj isKindOfClass: [NSData class]] )
			{
				type = SQLITE_BLOB ;
			}
			else
			{
				ThrowEx( SQLiteException,
						([NSString stringWithFormat: @"Invalid Encoding Object Type = %@", 
						 NSStringFromClass( [obj class] )]) ) ;
			}
		}
		switch( type )
		{
			case SQLITE_INTEGER:
			{
				iVal = va_arg( argList, NSInteger ) ;
				err = sqlite3_bind_int( self.query_, i + 1, iVal) ;
				CkSQLiteErrEx( err, self.mgr_ ) ;
				
				break ;
			}
			
			case SQLITE_FLOAT:
			{
				dVal = va_arg( argList, double ) ;
				err = sqlite3_bind_double( self.query_, i + 1, dVal) ;
				CkSQLiteErrEx( err, self.mgr_ ) ;
				
				break ;
			}

			case SQLITE_TEXT:
			{
				str = obj ;
				SafeLog( @"%@", str ) ;
				cStr = [str cStringUsingEncoding: NSUTF8StringEncoding] ;
				length = strlen( cStr ) ;
				err = sqlite3_bind_text( self.query_, i + 1, cStr, length, NULL) ;
				CkSQLiteErrEx( err, self.mgr_ ) ;
				
				break ;
			}

			case SQLITE_BLOB:
			{
				data = obj ;
				bytes = [data bytes] ;
				length = [data length] ;
				err = sqlite3_bind_blob( self.query_, i + 1, bytes, length, NULL) ;
				CkSQLiteErrEx( err, self.mgr_ ) ;
				
				break ;
			}
				
			default:
			{
				CkSwitchCaseIIEx( @"Data type", type ) ;
				
				break ;
			}
		}
	}

	[self executeWithHandler: handler reuseArgs: YES] ;	
}

@end
