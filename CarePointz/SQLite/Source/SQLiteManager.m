//
//  SQLiteManager.m
//  Mummify
//
//  Created by Libin Varghese on 5/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SQLiteManager.h"
#import "SQLiteManager+Private.h"
#import <safeObjC.h>

NSString * const SQLiteException = @"SQLiteException" ;

@implementation SQLiteManager

#pragma mark -
#pragma mark Private

@synthesize db_ = _db ;
@synthesize path_ = _path ;
@synthesize reuseList_ = _reuseList ;

#pragma mark Destructor
- (void) dealloc
{
	self.db_ = nil ;
	self.reuseList_ = nil ;
	self.path_ = nil ;
	
	[super dealloc] ;
}

+ (NSException *) _exceptionWithCode: (NSInteger) err
{
	NSString *str = nil ;
	NSException *ex = nil ;
	if( SQLITE_ISFAILED( err ) )
	{
		str = [[NSString alloc] initWithFormat: @"[%d]", err] ;
		ex = [NSException exceptionWithName: SQLiteException
									 reason: str
								   userInfo: nil] ;
		ObjCRelease( str ) ;
	}
	
	return ex ;
}

- (NSException *) _exception
{
	NSString *str = nil ;
	NSString *errStr = nil ;
	NSException *ex = nil ;
	const char *errMsg = NULL ;
	SQLiteError err = SQLITE_OK ;
	SQLiteError exErr = SQLITE_OK ;
	
	CkPtrSQLiteEx( self.db_, @"Invalid Manager: Database not open" ) ;
	err = sqlite3_errcode( self.db_ ) ;
	
	if( SQLITE_ISFAILED( err ) )
	{
		exErr = sqlite3_extended_errcode( self.db_ ) ;
		errMsg = sqlite3_errmsg( self.db_ ) ;
		errStr = [[NSString alloc] initWithCString: errMsg
										  encoding: NSUTF8StringEncoding] ;
		str = [[NSString alloc] initWithFormat: @"[%d - %d] - %@",
			   err, exErr, errStr] ;
		ObjCRelease( errStr ) ;
		ex = [NSException exceptionWithName: SQLiteException
									 reason: str
								   userInfo: nil] ;
		ObjCRelease( str ) ;
	}
	
	return ex ;
}

#pragma mark -
#pragma mark Init

- (id) initWithDB: (NSString *) path
{
	const char *file = NULL ;
	sqlite3 *pDB = NULL ;
	SQLiteReuseableQueryList *list = nil ;
	SQLiteError err = SQLITE_OK ;
	
	self = [super init] ;
	if( self )
	{
		file = [path cStringUsingEncoding: NSUTF8StringEncoding] ;
		err = sqlite3_open( file, &pDB ) ;
		
		if( SQLITE_ISFAILED( err ) )
		{
			ObjCRelease( self ) ;
			@throw [SQLiteManager _exceptionWithCode: err] ;
		}
		
		self.db_ = pDB ;
		self.path_ = path ;
		
		list = [[SQLiteReuseableQueryList alloc] init] ;
		self.reuseList_ = list ;
		ObjCRelease( list ) ;
	}
	
	return self ;
}

#pragma mark Property
- (void) setDb_:(sqlite3 *) db
{
	SQLiteQuery *query = nil ;
	NSUInteger count = 0 ;
	NSInteger i = 0 ;
	SQLiteError err = SQLITE_OK ;
	
	if( _db != db ) 
	{
		if( _db != nil )
		{
			count = [self.reuseList_ count] ;
			for( i = 0 ; i < count; i ++ )
			{
				query = [self.reuseList_ objectAtIndex: i] ;
				[query _clearContext] ;
			}

			err = sqlite3_close( _db ) ;
			CkSQLiteErrGenericLog( err,
				@"!! For some reason sqlite3_close failed" ) ;
		}
		
		_db = db ;

		if( _db != NULL )
		{
			count = [self.reuseList_ count] ;
			for( i = 0 ; i < count; i ++ )
			{
				query = [self.reuseList_ objectAtIndex: i] ;
				[query _setContextWithManager: self] ;
			}
		}
	}
}

#pragma mark Methods
- (SQLiteQuery *) queryWithQueryString: (NSString *) query
				   withReuseIdentifier: (NSString *) identifier
							withFormat: (char *) var1format, ...
{
	SQLiteQuery *q = nil ;
	va_list argList ;
	
	va_start( argList, var1format ) ;
	
	SafeLog( @"%@", query );
	
	q = [[SQLiteQuery alloc] _initWithQuery: query 
								withManager: self
						withReuseIdentifier: identifier
							   withFirstArg: var1format
								 withFormat: argList] ;
	va_end( argList ) ;
	
	return [q autorelease] ;
}

- (SQLiteQuery *) reusableQueryWithIdentifier: (NSString *) identifer
{
	SQLiteQuery *q = nil ;
	
	q = [self.reuseList_ queryWithIdentifer: identifer] ;
	return q ;
}

- (void) executeScriptFromString: (NSString *) script
{
	SQLiteScript *sqlScript = nil ;
	
	sqlScript = [[SQLiteScript alloc] _initFromString: script withManager: self] ;
	[sqlScript _execute] ;
	
	ObjCRelease( sqlScript ) ;
}

- (void) executeScriptFromFile: (NSString *) file
{
	SQLiteScript *script = nil ;
	
	script = [[SQLiteScript alloc] _initFromFile: file withManager: self] ;
	[script _execute] ;
	
	ObjCRelease( script ) ;
}

- (void) closeDB
{
	self.db_ = NULL ;
}

- (void) reopenDB
{
	const char *file = NULL ;
	sqlite3 *pDB = NULL ;
	SQLiteError err = SQLITE_OK ;
	
	file = [self.path_ cStringUsingEncoding: NSUTF8StringEncoding] ;
	err = sqlite3_open( file, &pDB ) ;
	
	if( SQLITE_ISFAILED( err ) )
	{
		ObjCRelease( self ) ;
		@throw [SQLiteManager _exceptionWithCode: err] ;
	}
	
	self.db_ = pDB ;	
}

@end
