//
//  ColumnAction.h
//  Mummify
//
//  Created by Libin Varghese on 7/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ColumnAction : NSObject
{
@protected
	NSString *_column ;
	SEL _action ;
	SEL _nullAction ;
}

@property (nonatomic, retain) NSString *column ;
@property (nonatomic, assign) SEL action ;
@property (nonatomic, assign) SEL nullAction ;

- (id) initWithColumn: (NSString *) column action: (SEL) action nullAction: (SEL) nullAction ;

@end

