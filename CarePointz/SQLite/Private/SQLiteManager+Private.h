/*
 *  SQLiteManager+Private.h
 *  Mummify
 *
 *  Created by Libin Varghese on 5/31/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#import "SQLiteManager.h"
#import <safeObjC.h>

extern NSString * const SQLiteException ;

#define SQLITE_ISOK(err)	(err == SQLITE_OK)
#define SQLITE_ISFAILED(err)	(!SQLITE_ISOK( err ))
#define CkSQLiteErrGenericEx(err,exName,exReason)	\
	if( SQLITE_ISFAILED(err) ) \
	{	\
		ThrowEx(exName, exReason) ;	\
	}

#define CkSQLiteErrEx(err,mgr)	\
	if( SQLITE_ISFAILED(err) ) \
	{	\
		@throw [(mgr) _exception] ; \
	}

#define CkSQLiteErrGenericLog(err,exReason)	\
	if( SQLITE_ISFAILED(err) ) \
	{	\
		SafeLog( @"[%d] %@", (err), (exReason) ) ;	\
	}

#define CkSQLiteErrLog(err,mgr)	\
	if( SQLITE_ISFAILED(err) ) \
	{	\
		SafeLog( @"[%d] %@", (err), [mgr _exception] ) ; \
	}

#define CkSQLiteConditionEx(condition,mgr)	\
	if( condition ) \
	{	\
		@throw [mgr _exception] ; \
	}

#define CkSQLiteConditionLog(condition,mgr)	\
	if( condition ) \
	{	\
		SafeLog( @"[%d] %@", [mgr _exception] ) ; \
	}

#define CkObjSQLiteEx(obj,msg)	CkObjEx(obj,SQLiteException,msg)
#define CkPtrSQLiteEx(ptr,msg)	CkPtrEx(ptr,SQLiteException,msg)
#define CkValueSQLiteEx(value,msg)	CkValueEx(value,SQLiteException, msg)
#define CkConditionSQLiteEx(condition,msg)	\
	CkConditionEx(condition,SQLiteException, msg)

typedef NSInteger SQLiteError ;
typedef NSUInteger SQLiteDataType ;

@interface SQLiteManager ()

+ (NSException *) _exceptionWithCode: (NSInteger) err ;
- (NSException *) _exception;

@property (nonatomic, assign) sqlite3* db_ ;
@property (nonatomic, retain) NSString *path_ ;
@property (nonatomic, retain) SQLiteReuseableQueryList *reuseList_ ;

@end

@interface NSArray (SQLiteEncodingList)

- (SQLiteDataType) typeAtIndex: (NSUInteger) index ;

@end


@interface NSMutableArray (SQLiteReuseableQueryList)

- (NSUInteger) indexOfQueryWithIdentifer: (NSString *) identifier ;
- (SQLiteQuery *) queryWithIdentifer: (NSString *) identifier ;

@end

typedef struct
{
	char *type ;
	SEL queryAction ;
	BOOL respondsToType ;
} HandlerFieldInfo ;

@interface SQLiteQuery ()

- (id) _initWithQuery: (NSString *) query
		  withManager: (SQLiteManager *) mgr
  withReuseIdentifier: (NSString *) identifier
		 withFirstArg: (char *) firstArg
		   withFormat: (va_list) format ;
- (void) _validateQuery ;
- (void) _clearContext ;
- (void) _clearQuery ;
- (void) _setContextWithManager: (SQLiteManager *) mgr ;
- (void) _loadQuery: (NSString *) query withFormat: (SQLiteEncodingList *) format ;
- (void *) _dataPtrForColumn: (NSInteger) column
					withType: (SQLiteDataType) type ;
- (void) _executeRowWithHandler: (id<SQLiteResultHandler>) handler
					   withInfo: (HandlerFieldInfo **) pFieldInfoList ;

@property (nonatomic, retain) SQLiteManager *mgr_ ;
@property (nonatomic, retain) NSString *queryStr_ ;
@property (nonatomic, assign) sqlite3_stmt *query_ ;
@property (nonatomic, retain) NSString *reuseID_ ;
@property (nonatomic, retain) SQLiteEncodingList *format_ ;
//@property (nonatomic, assign) BOOL isDirty_ ;

@end

@interface SQLiteScript : NSObject
{
	SQLiteManager *_mgr ;
	NSString *_scriptStr ;
}

@property (nonatomic, retain) SQLiteManager *mgr_ ;
@property (nonatomic, retain) NSString *scriptStr_ ;

- (id) _initFromString: (NSString *) script withManager: (SQLiteManager *) mgr ;
- (id) _initFromFile: (NSString *) file withManager: (SQLiteManager *) mgr ;
- (void) _execute ;

@end




