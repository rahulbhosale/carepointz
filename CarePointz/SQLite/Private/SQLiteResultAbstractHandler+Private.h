/*
 *  SQLiteResultHandler+Private.h
 *  Mummify
 *
 *  Created by Libin Varghese on 7/15/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#import "SQLiteResultAbstractHandler.h"

@interface SQLiteResultAbstractSimpleHandler ()

@property (nonatomic, retain) NSMutableArray *columnActionList_ ; 
@property (nonatomic, retain) id cacheItem_ ;
@property (nonatomic, retain) NSMutableDictionary *cacheDictionary_ ;

- (void) _query: (SQLiteQuery *) query gotColumn: (NSString *) name
	 withObject: (id) obj ;

@end

