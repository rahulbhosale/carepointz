//
//  PlaceMarker.h
//  CityZapper
//
//  Created by Syed Yusuf on 4/24/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface PlaceMarker : NSObject <MKAnnotation> {

	CLLocationCoordinate2D coordinate;
	
	NSString *subtitle;
	NSString *title;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

-(id)initWithCoordinate:(CLLocationCoordinate2D) coordinate title:(NSString *)title subtitle:(NSString *)subtitle;
- (NSString *)subtitle;
- (NSString *)title;

@end
