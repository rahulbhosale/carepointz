//
//  PlaceMarker.m
//  CityZapper
//
//  Created by Syed Yusuf on 4/24/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PlaceMarker.h"
#import <safeObjC.h>

@implementation PlaceMarker

@synthesize coordinate;

- (NSString *)subtitle{
	return subtitle;
}

- (NSString *)title{
	return title;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c title:(NSString *)t subtitle:(NSString *)subt{
	coordinate=c;
	title = [t retain];
	subtitle = [subt retain];
	SafeLog(@" %@ \n %@ : %lf,%lf",t,subt,c.latitude,c.longitude);
	return self;
}

@end
