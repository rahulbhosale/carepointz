#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>
#import <MapKit/MKReverseGeocoder.h>
#import "ConnectionManager.h"
#import "AdWhirlDelegateProtocol.h"

typedef enum
{
	kSearchStateNormal,
	kSearchStateAlternate
}SearchState;

typedef enum
{
	kAlertTypeAlternate3Button,
	kAlertTypeAlternate2Button,
	kAlertTypeTimed,
	kAlertTypeAlternateToNormal,
	kAlertTypeGeneral
}AlertType;

@class AdWhirlView;
@class PlaceMarker;

@interface MapViewController : UIViewController <MKMapViewDelegate,MKReverseGeocoderDelegate,ConnectionManagerDelegate,UITableViewDataSource,UITableViewDelegate, AdWhirlDelegate, UIAlertViewDelegate>
{
	MKMapView *tipMapView;
	MKReverseGeocoder *geoCoder;
	MKPlacemark *mPlacemark;
	
	int categoryImageIdx;
	
	PlaceMarker *tipPlaceMarker;
	UIAlertView *loadingAlert;
	NSInteger tagValue;
	NSMutableArray *resultsArray;
	
	UITableView *resultTable;
	
	NSInteger numOfConnections;
	
	NSInteger searchType;
	
	BOOL isMap;
	
	NSMutableArray *searchArr;
	
	BOOL isErr;
	
	UIAlertView *alternate3Alert;
	UIAlertView *alternate2Alert;
	UIAlertView *timedAlert;
	UIAlertView *alternateToNormalAlert;
	
	AdWhirlView *adView;
	
	NSString *errStr;
	
	UIImageView *googleLogo;
	
	NSString *userLocality;
	
	BOOL isFrameSet;
	BOOL iadViewIsVisible;
	
	BOOL isFirstTime;
	BOOL isIncAltPromptShown;
	
	BOOL isViewVisible;
	BOOL isGoingToDetails;
	
	SearchState currentState;
	SearchState previousState;
	
	NSInteger alternateFailCount;
	NSString *alternateSearchString;
    NSInteger currentRadius;
    NSInteger noOfError;
	
	NSMutableArray *connectionArr;
}

@property (nonatomic) NSInteger searchType;
@property (nonatomic, retain) MKMapView *tipMapView;
@property (nonatomic, retain) UITableView *resultTable;
@property (nonatomic, retain) NSMutableArray *resultsArray;
@property (nonatomic, retain) NSMutableArray *searchArr;
@property (nonatomic, retain) AdWhirlView *adView;
@property (nonatomic, copy) NSString *userLocality;

@property (nonatomic) NSInteger alternateFailCount;
@property (nonatomic) SearchState previousState;
@property (nonatomic) SearchState currentState;
@property (nonatomic) BOOL isFirstTime;

@property (nonatomic, retain) NSMutableArray *connectionArr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andSearchType:(NSInteger) srchType;

- (void) reloadData;

- (void)adjustAdSize;
- (void)adjustLayoutToOrientation:(UIInterfaceOrientation)newOrientation;

- (void) selectPoint:(id) selectedPoint;

- (void) showAlert:(UIAlertView *)alert ofType:(AlertType) alertType;
@end