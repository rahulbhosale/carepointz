#import "MapViewController.h"
#import "PlaceMarker.h"
#import "CarePointzAppDelegate.h"
#import "ViewController.h"
#import "CityDetailsViewController.h"
#import "SearchResult.h"
#import "SQLiteManager.h"
#import "Settings.h"
#import "AppConstants.h"
#import "H5CToolBar.h"
#import <safeObjC.h>
#import <AudioToolbox/AudioToolbox.h>
#define RADIUS_OF_EARTH_IN_KMS				6378.7		//kilometers

#define RESULT_KEYS @"id",@"geometry",@"location",@"lat",@"lng",@"viewport",@"name",@"rating",@"reference",@"types",@"type",@"vicinity",nil

@interface MapViewController ()
- (void) createAndSearchURLs;
- (void) _loadAlternate;
- (void) connectWithName:(NSString *) searchName andConnectionID:(id)connectID;
@end

@implementation MapViewController

@synthesize searchType;
@synthesize searchArr;
@synthesize resultsArray;
@synthesize adView;
@synthesize userLocality;
@synthesize tipMapView;
@synthesize resultTable;
@synthesize alternateFailCount;
@synthesize currentState;
@synthesize previousState;
@synthesize isFirstTime;
@synthesize connectionArr;

- (double) degToRad:(double)deg
{
	double rads = (deg * M_PI)/180;
	return rads;
}

- (double) radToDeg:(double)rad
{
	double degs = (rad * 180)/M_PI;
	return degs;
}

- (double) maxLatitudeFor:(double) latitude forDistance:(NSInteger) dist
{
	double radLat = [self degToRad:latitude];
	double maxLat = 0.0;
	maxLat = asin((sin(radLat) * cos(dist/RADIUS_OF_EARTH_IN_KMS)) + (cos(radLat) * sin(dist/RADIUS_OF_EARTH_IN_KMS) * cos((3 * M_PI)/ 4)));
	maxLat = [self radToDeg:maxLat];
	return maxLat;
}

- (double) maxLongitudeFor:(double) longitude withCenterLatitude:(double)cLat andMaxLatitude:(double) maxLat forDistance:(NSInteger) dist
{
	double radLat = [self degToRad:cLat];
	double radLng = [self degToRad:longitude];
	double maxLng = 0.0;
	maxLng = radLng + atan2((sin((3 * M_PI)/ 4) * sin(dist/RADIUS_OF_EARTH_IN_KMS) * cos(radLat)), (cos(dist/RADIUS_OF_EARTH_IN_KMS) - sin(radLat) * sin(maxLat)));
	maxLng = [self radToDeg:maxLng];
	return maxLng;
}

- (double) minLatitudeFor:(double) latitude forDistance:(NSInteger) dist
{
	double radLat = [self degToRad:latitude];
	double minLat = 0.0;
	minLat = asin((sin(radLat) * cos(dist/RADIUS_OF_EARTH_IN_KMS)) + (cos(radLat) * sin(dist/RADIUS_OF_EARTH_IN_KMS) * cos((7 * M_PI)/ 4)));
	minLat = [self radToDeg:minLat];
	return minLat;
}

- (double) minLongitudeFor:(double) longitude withCenterLatitude:(double)cLat andMinLatitude:(double) minLat forDistance:(NSInteger) dist
{
	double radLat = [self degToRad:cLat];
	double radLng = [self degToRad:longitude];
	double minLng = 0.0;
	minLng = radLng + atan2((sin((7 * M_PI)/ 4) * sin(dist/RADIUS_OF_EARTH_IN_KMS) * cos(radLat)), (cos(dist/RADIUS_OF_EARTH_IN_KMS) - sin(radLat) * sin(minLat)));
	minLng = [self radToDeg:minLng];
	return minLng;
}

- (void) updateUserCurrentLocalityIsOffline:(BOOL) isOffline
{
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
	NSString *currentLoc = nil;
	if (isOffline)
	{
		currentLoc = [NSString stringWithFormat:@"%@ (Offline mode)",self.userLocality];
	}
	else
	{
		currentLoc = self.userLocality;
	}
	
	if (currentLoc == nil
		|| [currentLoc length] <= 0)
	{
		if (isOffline)
		{
			currentLoc = @"Locality unavailable(Offline mode)";
		}
		else
		{
			currentLoc = @"Locality unavailable";
		}
		
	}
	//	if (numOfConnections == [self.searchArr count])
	//	{
	isOffline = NO;
	[appDel updateOrCreateActivityWithMessage:currentLoc onViewController:self withSpinner:NO];
	//	}
}

- (void) toggleListOrMap
{
	if (isMap)
	{
		self.tipMapView.hidden = YES;
		self.resultTable.hidden = NO;
		[self.resultTable reloadData];
		H5CToolBar *tb = (H5CToolBar *)self.navigationItem.rightBarButtonItem.customView;
		NSArray *tbArr = tb.items;
		UIButton *myLocBtn = [tbArr objectAtIndex:0];
		myLocBtn.enabled = NO;
		UIBarButtonItem *mapListBtn = [tbArr objectAtIndex:1];
		mapListBtn.title = @"Map";
	}
	else
	{
		self.resultTable.hidden = YES;
		self.tipMapView.hidden = NO;
		H5CToolBar *tb = (H5CToolBar *)self.navigationItem.rightBarButtonItem.customView;
		NSArray *tbArr = tb.items;
		UIButton *myLocBtn = [tbArr objectAtIndex:0];
		myLocBtn.enabled = YES;
		UIBarButtonItem *mapListBtn = [tbArr objectAtIndex:1];
		mapListBtn.title = @"List";
		[self.tipMapView setNeedsDisplay];
	}
	isMap = !isMap;
}

- (void) centerMapToCurrentLocation
{
	MKCoordinateRegion region;
	MKCoordinateSpan span = self.tipMapView.region.span;
	region.span=span;
	region.center=[LocationManager sharedSingleton].userLocation.coordinate;
	if ([self.tipMapView respondsToSelector:@selector(setRegion:animated:)]) {
		[self.tipMapView setRegion:region animated:YES];
	}
	else {
		[self.tipMapView setRegion:region];
	}
}

- (NSString *) keyForURL:(NSString *) urlStr
{
	NSMutableString *keyStr = [NSMutableString stringWithCapacity:0];
	NSArray *objArr = [urlStr componentsSeparatedByString:@"&"];
	for (NSString *str in objArr)
	{
		if ([str hasPrefix:@"location"])
		{
			NSArray *tempArr = [str componentsSeparatedByString:@"="];
			NSString *rtStr = [tempArr objectAtIndex:1];
			[keyStr appendString:rtStr];
		}
		if ([str hasPrefix:@"name"])
		{
			NSArray *tempArr = [str componentsSeparatedByString:@"="];
			NSString *rtStr = [tempArr objectAtIndex:1];
			[keyStr appendString:rtStr];
		}
	}
	return keyStr;
}

- (NSString *) urlForKey:(NSString *) key
{
	//CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)
	return nil;
}

- (BOOL) cacheData: (id)data forURL:(NSString *) urlToCache
{
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:[self keyForURL:urlToCache]];
	
	BOOL isSucc = [[NSFileManager defaultManager] createFileAtPath: folderPath
														  contents: data
														attributes: NULL];
	SafeLog(@"%d",isSucc);
	return isSucc;
}

- (NSArray *) loadFromCacheForURL: (NSString *) urlStr
{
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:[self keyForURL:urlStr]];
	NSArray *arr = [NSArray arrayWithContentsOfFile:folderPath];
	SafeLog(@"******************************** \n %@ \n*****************************************",arr);
	return arr;
}
- (MKCoordinateSpan) currentMapSpan
{
	MKCoordinateSpan span;
	switch (currentRadius)
	{
		case 2000:
		{
			span.latitudeDelta=0.04;
			span.longitudeDelta=0.04;
		}
			break;
		case 3000:
		{
			span.latitudeDelta=0.06;
			span.longitudeDelta=0.06;
		}
			break;
		case 5000:
		{
			span.latitudeDelta=0.08;
			span.longitudeDelta=0.08;
		}
			break;
		case 10000:
		{
			span.latitudeDelta=0.2;
			span.longitudeDelta=0.2;
		}
			break;
		default:
			break;
	}
	return span;
}

-(void) currentSearchRadius
{
    MKCoordinateRegion region;
    CLLocationCoordinate2D location = [LocationManager sharedSingleton].userLocation.coordinate;
    
    switch (currentRadius) {
        case 2000:
            currentRadius = 3000;
            break;
        case 3000:
            currentRadius = 5000;
            break;
        case 5000:
            currentRadius = 10000;
            break;
        default:
            break;
    }

    region.span = [self currentMapSpan];
    region.center = location;
    if ([self.tipMapView respondsToSelector:@selector(setRegion:animated:)]) {
        [self.tipMapView setRegion:region animated:YES];
    }
    else
    {
        [self.tipMapView setRegion:region];
    }
}

- (BOOL) izzAllWell
{
	BOOL allIzzWell = YES;
	if ([self.resultsArray count] <= 0)
	{
		allIzzWell = NO;
	}
	return allIzzWell;
}

- (void) clearData
{
	[self.resultsArray removeAllObjects];
	[self.resultTable reloadData];
	[self.tipMapView removeAnnotations:self.tipMapView.annotations];
}


- (void) showData
{
	UILabel *lbl = (UILabel *)[self.resultTable viewWithTag:2001];
	if (lbl != nil)
	{
		[lbl removeFromSuperview];
		lbl = nil;
	}

    if(alternate3Alert)
    {
		isIncAltPromptShown = NO;
        [alternate3Alert dismissWithClickedButtonIndex:3 animated:YES];
    }
	else if(alternate2Alert)
    {
        [alternate2Alert dismissWithClickedButtonIndex:3 animated:YES];
    }
	[self updateUserCurrentLocalityIsOffline:NO];
	[self.resultsArray sortUsingSelector:@selector(compareDistanceWith:)];
	for (SearchResult *res in self.resultsArray)
	{
		CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(res.lat, res.lng);
		PlaceMarker * pm = [[PlaceMarker alloc] initWithCoordinate:locationCord title: res.name subtitle: res.vicinity];
		[self.tipMapView addAnnotation:pm];
		[pm release];
	}
    
    [self.tipMapView setNeedsDisplay];
	[self.resultTable reloadData];
}

- (void) handleAlternateToNormal
{
	NSString *msg = [NSString stringWithFormat:@"We have found %@ near you. Shall we ...",self.title];
	UIAlertView *noResultsAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"AlertTitle_Attention", @"") message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Load it", @"Load Alternate",nil];
	[self showAlert:noResultsAlert ofType:kAlertTypeAlternateToNormal];
	[noResultsAlert release];
	
	//Play sound and show alert
	CFURLRef soundFileURLRef;
	SystemSoundID soundFileObject;
	
	NSString *path = [[NSBundle mainBundle] pathForResource:@"tap" ofType:@"aif"];
	NSURL *tapSound   = [NSURL URLWithString:path];
	
	// Store the URL as a CFURLRef instance
	soundFileURLRef = (CFURLRef) [tapSound retain];
	
	// Create a system sound object representing the sound file.
	AudioServicesCreateSystemSoundID (
									  soundFileURLRef,
									  &soundFileObject
									  );
	
	AudioServicesPlayAlertSound(soundFileObject);
	AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

- (void) handleData
{
	// Handle all show conditions
	if ([self izzAllWell])
	{
        currentRadius = [[Settings sharedSettings].resultSetRadius intValue];
		if (self.isFirstTime)
		{
			self.isFirstTime = NO;
		}
		switch (self.currentState)
		{
			case kSearchStateNormal:
			{
				if (self.previousState == kSearchStateAlternate)
				{
					// Do alternate stuff
					[self handleAlternateToNormal];
				}
				else
				{
					[self showData];
				}
			}
				break;
			case kSearchStateAlternate:
			{
				[self showData];
			}
				break;	
			default:
				break;
		}
	}
	else if (self.currentState == kSearchStateNormal)
	{
		// Fucked in Normal flow.
		if (self.previousState == kSearchStateAlternate)
		{
			// Do alternate stuff without prompt
			[self _loadAlternate];
		}
		else
		{
			if (alternateSearchString != nil
				|| [alternateSearchString length] > 0)
			{
				if (currentRadius < 10000) 
				{
					// Show prompt asking for alternate, increase radius or cancel.
					if (!isIncAltPromptShown) {
						isIncAltPromptShown = YES;
						NSString *msg = [NSString stringWithFormat:@"There are no \" %@ \" CarePointz near by with this facility. Shall we ...",self.title];
						NSString *btnTlt = [NSString stringWithFormat:@"Load %@ Instead",alternateSearchString];
						UIAlertView *noResultsAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"AlertTitle_Alert", @"") message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Increase Radius and Search", btnTlt,nil];
						[self showAlert:noResultsAlert ofType:kAlertTypeAlternate3Button];
						[noResultsAlert release];
						
						UILabel *lbl = (UILabel *)[self.resultTable viewWithTag:2001];
						if (lbl != nil)
						{
							[lbl removeFromSuperview];
							lbl = nil;
						}
						
						lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, 320, 40)];
						lbl.textColor = [UIColor whiteColor];
						lbl.numberOfLines = 3;
						lbl.backgroundColor = [UIColor clearColor];
						lbl.textAlignment = UITextAlignmentCenter;
						lbl.tag =2001;
						lbl.text = [NSString stringWithFormat:@"No %@ facility found.", self.title];
						[self.resultTable addSubview:lbl];
						[lbl release];
					}
					else
					{
						[self currentSearchRadius];
						[self createAndSearchURLs];
					}
				}
				else
				{
					// Show prompt asking for alternate or not.
					isIncAltPromptShown = NO;
					MKCoordinateRegion region;
					CLLocationCoordinate2D location = [LocationManager sharedSingleton].userLocation.coordinate;
					currentRadius = [[Settings sharedSettings].resultSetRadius intValue];
					region.span = [self currentMapSpan];
					region.center = location;
					if ([self.tipMapView respondsToSelector:@selector(setRegion:animated:)]) {
						[self.tipMapView setRegion:region animated:YES];
					}
					else
					{
						[self.tipMapView setRegion:region];
					}
					NSString *msg = [NSString stringWithFormat:@"There are no \"%@\" CarePointz near by with this facility. Shall we search for nearest \"%@\" instead?",self.title,alternateSearchString];
					UIAlertView *noResultsAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"AlertTitle_Alert", @"") message:msg delegate:self cancelButtonTitle:@"Sure" otherButtonTitles:@"Nope",nil];
					[self showAlert:noResultsAlert ofType:kAlertTypeAlternate2Button];
					[noResultsAlert release];
					
					UILabel *lbl = (UILabel *)[self.resultTable viewWithTag:2001];
					if (lbl != nil)
					{
						[lbl removeFromSuperview];
						lbl = nil;
					}
					
					lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, 320, 40)];
					lbl.textColor = [UIColor whiteColor];
					lbl.numberOfLines = 3;
					lbl.backgroundColor = [UIColor clearColor];
					lbl.textAlignment = UITextAlignmentCenter;
					lbl.tag =2001;
					lbl.text = [NSString stringWithFormat:@"No %@ facility found.", self.title];
					[self.resultTable addSubview:lbl];
					[lbl release];
				}
			}
			else
			{
				// What to do?
				// Show prompt asking for alternate or not.
                // increase the radius and search again.
                if (currentRadius < 10000) {
					[self currentSearchRadius];
					[self createAndSearchURLs];
				}
				else
				{
					NSString *msg = [NSString stringWithFormat:@"There are no \" %@ \" CarePointz near by with this facility.",self.title];
					UIAlertView *noResultsAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"AlertTitle_Alert", @"") message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
					[self showAlert:noResultsAlert ofType:kAlertTypeTimed];
					[noResultsAlert release];
					[self updateUserCurrentLocalityIsOffline:NO];
					
					
					UILabel *lbl = (UILabel *)[self.resultTable viewWithTag:2001];
					if (lbl != nil)
					{
						[lbl removeFromSuperview];
						lbl = nil;
					}
					
					lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, 320, 40)];
					lbl.textColor = [UIColor whiteColor];
					lbl.numberOfLines = 3;
					lbl.backgroundColor = [UIColor clearColor];
					lbl.textAlignment = UITextAlignmentCenter;
					lbl.tag =2001;
					lbl.text = [NSString stringWithFormat:@"No %@ facility found.", self.title];
					[self.resultTable addSubview:lbl];
					[lbl release];
				}
                
			}
		}
	}
	else if (self.currentState == kSearchStateAlternate)
	{
		NSInteger currentDistance = [[Settings sharedSettings].resultSetRadius intValue];
		switch (currentDistance)
		{
			case 2000:
			{
				// See the alternateFinalCount
				if (alternateFailCount == 3)
				{
					UILabel *lbl = (UILabel *)[self.resultTable viewWithTag:2001];
					if (lbl != nil)
					{
						[lbl removeFromSuperview];
						lbl = nil;
					}
					
					lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, 320, 40)];
					lbl.textColor = [UIColor whiteColor];
					lbl.numberOfLines = 3;
					lbl.backgroundColor = [UIColor clearColor];
					lbl.textAlignment = UITextAlignmentCenter;
					lbl.tag =2001;
					lbl.text = [NSString stringWithFormat:@"No %@ facility found.", alternateSearchString];
					[self.resultTable addSubview:lbl];
					[lbl release];
                    [self updateUserCurrentLocalityIsOffline:NO];
				}
				else
				{
					alternateFailCount++;
                    [self currentSearchRadius];
					[self _loadAlternate];
				}
			}
				break;
			case 3000:
			{
				// See the alternateFinalCount
				if (alternateFailCount == 2)
				{
					UILabel *lbl = (UILabel *)[self.resultTable viewWithTag:2001];
					if (lbl != nil)
					{
						[lbl removeFromSuperview];
						lbl = nil;
					}
					
					lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, 320, 40)];
					lbl.textColor = [UIColor whiteColor];
					lbl.numberOfLines = 3;
					lbl.backgroundColor = [UIColor clearColor];
					lbl.textAlignment = UITextAlignmentCenter;
					lbl.tag =2001;
					lbl.text = [NSString stringWithFormat:@"No %@ facility found.", alternateSearchString];
					[self.resultTable addSubview:lbl];
					[lbl release];
                    [self updateUserCurrentLocalityIsOffline:NO];
				}
				else
				{
					alternateFailCount++;
                    [self currentSearchRadius];
					[self _loadAlternate];
				}
			}
				break;
			case 5000:
			{
				// See the alternateFinalCount
				if (alternateFailCount == 1)
				{
					UILabel *lbl = (UILabel *)[self.resultTable viewWithTag:2001];
					if (lbl != nil)
					{
						[lbl removeFromSuperview];
						lbl = nil;
					}
					
					lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, 320, 40)];
					lbl.textColor = [UIColor whiteColor];
					lbl.numberOfLines = 3;
					lbl.backgroundColor = [UIColor clearColor];
					lbl.textAlignment = UITextAlignmentCenter;
					lbl.tag =2001;
					lbl.text = [NSString stringWithFormat:@"No %@ facility found.", alternateSearchString];
					[self.resultTable addSubview:lbl];
					[lbl release];
					// Leave it blank by showing the user that nothing could not be found
					[self updateUserCurrentLocalityIsOffline:NO];
				}
				else
				{
					alternateFailCount++;
                    [self currentSearchRadius];
					[self _loadAlternate];
				}
			}
				break;
			case 10000:
			{
				UILabel *lbl = (UILabel *)[self.resultTable viewWithTag:2001];
				if (lbl != nil)
				{
					[lbl removeFromSuperview];
					lbl = nil;
				}
				lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, 320, 40)];
				lbl.textColor = [UIColor whiteColor];
				lbl.numberOfLines = 3;
				lbl.backgroundColor = [UIColor clearColor];
				lbl.textAlignment = UITextAlignmentCenter;
				lbl.tag =2001;
				lbl.text = [NSString stringWithFormat:@"No %@ facility found.", alternateSearchString];
				[self.resultTable addSubview:lbl];
				[lbl release];
				// Leave it blank by showing the user that nothing could not be found
				[self updateUserCurrentLocalityIsOffline:NO];
			}
				break;
			default:
				break;
		}
	}
}

- (void) loadFromCacheForCategory:(NSString *) catName
{
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
	
	int dist = currentRadius/1000;
	
	double maxLat = [self maxLatitudeFor:appDel.currentLat forDistance:dist];
	double maxLng = [self maxLongitudeFor:appDel.currentLng withCenterLatitude:appDel.currentLat andMaxLatitude:maxLat forDistance:dist];
	double minLat = [self minLatitudeFor:appDel.currentLat forDistance:dist];
	double minLng = [self minLongitudeFor:appDel.currentLng withCenterLatitude:appDel.currentLat andMinLatitude:minLat forDistance:dist];
	
	NSLog(@"MAX(%lf, %lf) and MIN(%lf, %lf) , distance max = %lf and distance min = %lf adn actual dist = %d",maxLat,maxLng,minLat,minLng,[[LocationManager sharedSingleton] distanceForLatitude:maxLat andLongitude:maxLng inMiles:NO],[[LocationManager sharedSingleton] distanceForLatitude:minLat andLongitude:minLng inMiles:NO],currentRadius);
	
	NSArray *resArr = nil;
	NSString *fetchSearchResultStr = [NSString stringWithFormat:@"select * from CPSearchResults where categoryName = '%@' and lat >= %lf and lat <= %lf and lng <= %lf and lng >= %lf;",catName, maxLat, minLat, maxLng, minLng];
	SQLiteQuery *query1 = [appDel.mrMgr queryWithQueryString: fetchSearchResultStr
										 withReuseIdentifier: nil
												  withFormat: nil];
	SQLiteResultSimpleArrayHandler *handler = [[SQLiteResultSimpleArrayHandler alloc] init];
	handler.creator = NSClassFromString( @"SearchResult" );
	handler.createItem = @selector(new);
	NSMutableArray *act = [[NSMutableArray alloc] initWithColumnActions: 14,
						   @"id", @selector(setSearchResultID:), NULL,
						   @"placeID", @selector(setPlaceID:), NULL,
						   @"lat", @selector(setLat:), NULL,
						   @"lng", @selector(setLng:), NULL,
						   @"name", @selector(setName:), NULL,
						   @"rating", @selector(setRating:), NULL,
						   @"reference", @selector(setReference:), NULL,
						   @"vicinity", @selector(setVicinity:), NULL,
						   @"address", @selector(setAddress:), NULL,
						   @"phoneNumber", @selector(setPhoneNumber:), NULL,
						   @"internationalPhoneNumber", @selector(setInternationalPhoneNumber:), NULL,
						   @"websiteURLString", @selector(setWebsiteURLString:), NULL,
						   @"categoryName", @selector(setCategory:), NULL,
						   @"lastAccessed", @selector(setLastAccessed:), NULL];
	handler.columnActionList = act;
	ObjCRelease(act);
	[query1 executeWithHandler: handler] ;
	resArr = [handler.outArray retain];
	ObjCRelease(handler);
	for (int i = 0; i < [resArr count]; i++)
	{
		SearchResult *res = [resArr objectAtIndex:i];
		BOOL isMiles = [Settings sharedSettings].isDistanceInMiles;
		double dist = [[LocationManager sharedSingleton] distanceForLatitude:res.lat andLongitude:res.lng inMiles:isMiles];
		res.currentDistance = dist;
	}
	[self.resultsArray addObjectsFromArray:resArr];
}

#pragma mark - Connection method
- (void) connectWithName:(NSString *) searchName andConnectionID:(id)connectID
{
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
	NSString *str = [appDel searchURLForLocationWithLatitude:appDel.currentLat logitude:appDel.currentLng andName:searchName];
    
    str = [appDel updateSearchURL:str withSearchRadius:currentRadius andName:searchName];
	str = [str stringByAddingPercentEscapesUsingEncoding:NSStringEncodingConversionAllowLossy];
	SafeLog(@"search string ::: %@",str);
	
	NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:str]];
	ConnectionManager *conn = [[ConnectionManager alloc] initWithRequest:req];
	conn.delegate = self;
	conn.connectionID = connectID;
    [self.connectionArr addObject:conn];
	[conn startConnection];
}

- (void) removeConnection:(ConnectionManager *) connection
{
	for (ConnectionManager *conn in self.connectionArr)
	{
		if ([(NSString *)conn.connectionID isEqualToString:(NSString *)connection.connectionID])
		{
			[conn setDelegate:nil];
			[self.connectionArr removeObject:conn];
			break;
		}
	}
}

-(void) connectionManager :(ConnectionManager *)connectionManager didGetData:(NSData *) data
{
	NSError *jsonError = nil;
	NSDictionary *resultDict = [[CJSONDeserializer deserializer] deserialize:data error:&jsonError];
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;    
	NSString *status = [resultDict objectForKey:@"status"];
    NSLog(@"******* %@ ******", status);
	if ([appDel isStatusError:status])
	{
		// handle error
		[appDel updateOrCreateActivityWithMessage:NSLocalizedString(@"Problem_With_Server", @"") onViewController:self withSpinner:YES];
	}
	else
	{
		NSArray *resultArr = [resultDict valueForKey:@"results"];
		for (int i =0; i <[resultArr count]; i++)
		{
			SearchResult *result = [[SearchResult alloc] init];
			NSUInteger timeStamp = [[NSDate date] timeIntervalSince1970];
			NSDictionary *arrDict = [resultArr objectAtIndex:i];
			
			result.placeID = [arrDict objectForKey:@"id"];
			NSString *latStr = [(NSDictionary *)[(NSDictionary *)[arrDict objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"];
			result.lat = [latStr doubleValue];
			NSString *longiStr = [(NSDictionary *)[(NSDictionary *)[arrDict objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"];
			result.lng = [longiStr doubleValue];
			
			result.name = [arrDict objectForKey:@"name"];
			result.vicinity = [arrDict objectForKey:@"vicinity"];
			result.reference = [arrDict objectForKey:@"reference"];
			result.lastAccessed = timeStamp;
			
			NSArray *typesArr = [arrDict objectForKey:@"types"];
			if ([typesArr containsObject:@"doctor"]
				|| [typesArr containsObject:@"health"]
				|| [typesArr containsObject:@"dentist"]
				|| [typesArr containsObject:@"hair_care"]
				|| [typesArr containsObject:@"hospital"]
				|| [typesArr containsObject:@"pharmacy"]
				|| [typesArr containsObject:@"local_government_office"]
				|| [typesArr containsObject:@"physiotherapist"]
				|| [typesArr containsObject:@"school"]
				|| [typesArr containsObject:@"veterinary_care"]
				|| [typesArr containsObject:@"pet_store"])
			{
				result.category = connectionManager.connectionID;
				result.currentDistance = [[LocationManager sharedSingleton] distanceForLatitude:result.lat andLongitude:result.lng inMiles:[Settings sharedSettings].isDistanceInMiles];
				//[self.resultsArray addObject:result];
				if (![result addToDB])
				{
					//[result fetchFromDBWhereName:result.name atLat:result.lat andLongitude:result.lng];
					[result getID];
					[result updateTimeStamp];
					if (![result.reference isEqualToString:[arrDict objectForKey:@"reference"]])
					{
						result.reference = [arrDict objectForKey:@"reference"];
						[result updateReferenceID];
						//TODO: Call another connection
					}
				}
			}
			else if ([typesArr containsObject:@"establishment"])
			{
				if ([typesArr count] == 1)
				{
					result.category = connectionManager.connectionID;
					result.currentDistance = [[LocationManager sharedSingleton] distanceForLatitude:result.lat andLongitude:result.lng inMiles:[Settings sharedSettings].isDistanceInMiles];
					//[self.resultsArray addObject:result];
					if (![result addToDB])
					{
						//[result fetchFromDBWhereName:result.name atLat:result.lat andLongitude:result.lng];
						[result getID];
						[result updateTimeStamp];
						if (![result.reference isEqualToString:[arrDict objectForKey:@"reference"]])
						{
							result.reference = [arrDict objectForKey:@"reference"];
							[result updateReferenceID];
							//TODO: Call another connection
						}
						
					}
				}
				else if ([typesArr containsObject:@"doctor"]
						 || [typesArr containsObject:@"health"]
						 || [typesArr containsObject:@"dentist"]
						 || [typesArr containsObject:@"hair_care"]
						 || [typesArr containsObject:@"hospital"]
						 || [typesArr containsObject:@"pharmacy"]
						 || [typesArr containsObject:@"local_government_office"]
						 || [typesArr containsObject:@"physiotherapist"]
						 || [typesArr containsObject:@"school"]
						 || [typesArr containsObject:@"veterinary_care"]
						 || [typesArr containsObject:@"pet_store"])
				{
					result.category = connectionManager.connectionID;
					result.currentDistance = [[LocationManager sharedSingleton] distanceForLatitude:result.lat andLongitude:result.lng inMiles:[Settings sharedSettings].isDistanceInMiles];
					//[self.resultsArray addObject:result];
					if (![result addToDB])
					{
						//[result fetchFromDBWhereName:result.name atLat:result.lat andLongitude:result.lng];
						[result getID];
						[result updateTimeStamp];
						if (![result.reference isEqualToString:[arrDict objectForKey:@"reference"]])
						{
							result.reference = [arrDict objectForKey:@"reference"];
							[result updateReferenceID];
							//TODO: Call another connection
						}
					}
					
				}
				else
				{
					// ignore
				}
			}
			else
			{
				//ignore
			}
			ObjCRelease(result);
		}
	}
    [self loadFromCacheForCategory:connectionManager.connectionID];
	[self removeConnection:connectionManager];
	++numOfConnections;
	if (numOfConnections == [self.searchArr count])
	{
		// Got data call
		[self handleData];
	}
	else if (numOfConnections > [self.searchArr count])
	{
		[self handleData];
	}
}

-(void) connectionManager :(ConnectionManager *)connectionManager didFailWithError:(NSError *) error
{
	SafeLog(@"ERROR %d: %@",[error code],[error localizedDescription]);
    ++numOfConnections;
    if ([error code] ==  -1009)
    {
        noOfError++;
        CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;    
        // handle error
		[appDel updateOrCreateActivityWithMessage:NSLocalizedString(@"Check_Internet_Connection", @"") onViewController:self withSpinner:YES];
        if(noOfError == [self.searchArr count])
        {
            UIAlertView *noResultsAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"AlertTitle_Connection_Error", @"") message:NSLocalizedString(@"No_Internet_Connection", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
            [self showAlert:noResultsAlert ofType:kAlertTypeTimed];
            [noResultsAlert release];
        }
    }
    [self loadFromCacheForCategory:connectionManager.connectionID];
	[self removeConnection:connectionManager];
    if (numOfConnections == [self.searchArr count])
    {
        [self handleData];
    }
    else if (numOfConnections > [self.searchArr count])
	{
		[self handleData];
	}
    NSLog(@"no of connection : %d", numOfConnections);
}

- (void) createAndSearchURLs
{
	numOfConnections = 0;
	NSString *pgTitle = nil;
	if (self.searchArr)
	{
		self.searchArr = nil;
	}
	NSMutableArray *srchArr = [[NSMutableArray alloc] initWithCapacity:0];
	switch (self.searchType) {
		case kSearchTypeHospital:
			pgTitle = @"Hospital";
			[srchArr addObject:@"hospital"];
			[srchArr addObject:@"clinic"];
			[srchArr addObject:@"doctor"];
            [srchArr addObject:@"speciality"];
			break;
		case kSearchTypePharmacy:
			pgTitle = @"Pharmacy";
			[srchArr addObject:@"health"];
			[srchArr addObject:@"chemist"];
			[srchArr addObject:@"pharma"];
			[srchArr addObject:@"pharmacy"];
			break;
        case kSearchTypeHeart:
			pgTitle = @"Heart";
			[srchArr addObject:@"heart"];
			[srchArr addObject:@"cardiology"];
			[srchArr addObject:@"cardio"];
            [srchArr addObject:@"pulmonary"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeEye:
			pgTitle = @"Eye";
			[srchArr addObject:@"eye"];
			[srchArr addObject:@"optometrist"];
			[srchArr addObject:@"ophthalmology"];
			[srchArr addObject:@"ophthalmologist"];
			alternateSearchString = @"hospital";
			break;
        case kSearchTypeGynecology:
			pgTitle = @"Gynecology";
			[srchArr addObject:@"gynecology"];
			[srchArr addObject:@"gynecologist"];
			[srchArr addObject:@"women clinic"];
			[srchArr addObject:@"obstetrics"];
			[srchArr addObject:@"maternity"];
			[srchArr addObject:@"pregnancy"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeDental:
			pgTitle = @"Dental";
			[srchArr addObject:@"dental"];
			[srchArr addObject:@"oral"];
			[srchArr addObject:@"dentist"];
			[srchArr addObject:@"orthodontist"];
			alternateSearchString = @"hospital";
			break;
        case kSearchTypeInternalMedicine:
			pgTitle = @"Family Medicine";
			[srchArr addObject:@"internal medicine"];
			[srchArr addObject:@"general medicine"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeChild:
			pgTitle = @"Child Specialist";
			[srchArr addObject:@"child"];
			[srchArr addObject:@"pediatric"];
			[srchArr addObject:@"pediatrician"];
			[srchArr addObject:@"kids"];
			[srchArr addObject:@"children hospital"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeDiagonosticCenter:
			pgTitle = @"Lab/Radiology";
			[srchArr addObject:@"diagnostic center"];
			[srchArr addObject:@"diagnostics"];
			[srchArr addObject:@"laboratory"];
			[srchArr addObject:@"radiology"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeBronchology:
			pgTitle = @"Lung/Chest";
			[srchArr addObject:@"lung"];
			[srchArr addObject:@"chest"];
			[srchArr addObject:@"respiratory"];
			alternateSearchString = @"hospital";
            break;
        case kSearchTypeUrology:
			pgTitle = @"Urology";
			[srchArr addObject:@"urology"];
			[srchArr addObject:@"kidney"];
			alternateSearchString = @"hospital";
			break;
        case kSearchTypeNephrology:
			pgTitle = @"Kidney";
			[srchArr addObject:@"kidney"];
			[srchArr addObject:@"nephrology"];
			[srchArr addObject:@"dialysis"];
			alternateSearchString = @"hospital";
			break;
        case kSearchTypeNeurology:
			pgTitle = @"Neurology";
			[srchArr addObject:@"brain"];
			[srchArr addObject:@"nerves"];
			[srchArr addObject:@"spine"];
			[srchArr addObject:@"neurology"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeBone:
			pgTitle = @"Bones";
			[srchArr addObject:@"bone"];
			[srchArr addObject:@"orthopaedic"];
			[srchArr addObject:@"ortho"];
			alternateSearchString = @"hospital";
			break;
        case kSearchTypeDiabetese:
			pgTitle = @"Diabetes";
			[srchArr addObject:@"diabetes"];
			[srchArr addObject:@"diabetologist"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeCancer:
			pgTitle = @"Cancer";
			[srchArr addObject:@"breast cancer"];
			[srchArr addObject:@"oncologist"];
			[srchArr addObject:@"cancer"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeHair:
			pgTitle = @"Skin/Hair";
			[srchArr addObject:@"skin"];
			[srchArr addObject:@"dermatology"];
			[srchArr addObject:@"dermatologist"];
			[srchArr addObject:@"hair"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeBlood:
			pgTitle = @"Blood Bank(s)";
			[srchArr addObject:@"blood"];
			alternateSearchString = @"hospital";
			break;
        case kSearchTypeENT:
			pgTitle = @"E.N.T";
			[srchArr addObject:@"ent"];
			[srchArr addObject:@"ear"];
			[srchArr addObject:@"nose"];
			[srchArr addObject:@"throat"];
			alternateSearchString = @"hospital";
			break;
        case kSearchTypeEndocrinology:
			pgTitle = @"Endocrinology";
			[srchArr addObject:@"endocrinology"];
			[srchArr addObject:@"endocrinologist"];
			alternateSearchString = @"hospital";
			break;
        case kSearchTypeAnesthesia:
			pgTitle = @"Anesthesiology";
			//[srchArr addObject:@"surgery"];
			[srchArr addObject:@"anesthesiology"];
			[srchArr addObject:@"anesthetist"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeCosmetology:
			pgTitle = @"Plastic Surgery";
			[srchArr addObject:@"plastic surgery"];
			[srchArr addObject:@"cosmetic"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeSexology:
			pgTitle = @"Sexology";
			[srchArr addObject:@"sexologist"];
			[srchArr addObject:@"fertility"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypePsychology:
			pgTitle = @"Psychology";
			[srchArr addObject:@"psychology"];
			[srchArr addObject:@"mental"];
			[srchArr addObject:@"psychiatrist"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeChiropractic:
			pgTitle = @"Pain Management";
			[srchArr addObject:@"chiropractic"];
			[srchArr addObject:@"massage"];
			alternateSearchString = @"yoga";
			break;
		case kSearchTypePhysiotherapy:
			pgTitle = @"Physiotherapy";
			[srchArr addObject:@"physio"];
			alternateSearchString = @"yoga";
			break;
		case kSearchTypeGas:
			pgTitle = @"Gastric";
			[srchArr addObject:@"gastroenterology"];
			[srchArr addObject:@"gastroen"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeIvfCenter:
			pgTitle = @"IVF Center";
			[srchArr addObject:@"ivf"];
			[srchArr addObject:@"fertility"];
			[srchArr addObject:@"invitro"];
			[srchArr addObject:@"sperm"];
			alternateSearchString = @"hospital";
			break;
		case kSearchTypeDiet:
			pgTitle = @"Dietitians";
			[srchArr addObject:@"diet"];
			[srchArr addObject:@"dietitian"];
			[srchArr addObject:@"dietric"];
			alternateSearchString = @"nutrition";
			break;
		case kSearchTypeRehabilitationCenters:
			pgTitle = @"Rehab Centers";
			[srchArr addObject:@"rehab"];
			[srchArr addObject:@"recovery center"];
			alternateSearchString = @"home care";
			break;
        case kSearchTypeAyurveda:
			pgTitle = @"Ayurveda";
			[srchArr addObject:@"ayurved"];
			[srchArr addObject:@"ayurvedic"];
			[srchArr addObject:@"ayurveda"];
			alternateSearchString = @"herbal";
			break;
		case kSearchTypeHomeopathy:
			pgTitle = @"Homeopathy";
			[srchArr addObject:@"homeopathy"];
			break;
		case kSearchTypeNaturopathyYoga:
			pgTitle = @"Yoga";
			[srchArr addObject:@"yoga"];
			[srchArr addObject:@"acupressure"];
			[srchArr addObject:@"naturopathy"];
			alternateSearchString = @"meditation";
			break;
		case kSearchTypeVeterinary:
			pgTitle = @"Veterinary";
			[srchArr addObject:@"veterinary"];
			[srchArr addObject:@"animal hospital"];
			alternateSearchString = @"pet";
			break;
		case kSearchTypeRestrooms:
			pgTitle = @"Restrooms";
			[srchArr addObject:@"toilet"];
			[srchArr addObject:@"urnial"];
			[srchArr addObject:@"restroom"];
			[srchArr addObject:@"rest room"];
			alternateSearchString = @"restaurant";
			break;
		default:
			break;
	}
	self.searchArr = srchArr;
	ObjCRelease(srchArr);
	self.title = pgTitle;
	
	for (int i = 0; i < [self.searchArr count]; i++)
	{
		NSString *srcStr = [self.searchArr objectAtIndex:i];
		[self connectWithName:srcStr andConnectionID:srcStr];
	}
}

- (void)initData
{
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
	[appDel updateOrCreateActivityWithMessage:NSLocalizedString(@"Wait_For_Searching_Places", @"") onViewController:self withSpinner:YES];
	[self createAndSearchURLs];
	//	loadingAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please wait while we find the places you need..." delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
	//	[loadingAlert show];
}

- (void) initTableView
{
	CGRect appRect = [UIScreen mainScreen].bounds;
	appRect.origin.y = 14.0f;
	appRect.size.height -= 14.0f;
	UITableView *tempTV = [[UITableView alloc] initWithFrame:appRect style:UITableViewStylePlain];
	tempTV.delegate = self;
	tempTV.dataSource = self;
    tempTV.separatorStyle = UITableViewCellSeparatorStyleNone;
	tempTV.backgroundColor = [UIColor clearColor];
	tempTV.hidden = YES;
	tempTV.multipleTouchEnabled = NO;
	[self.view addSubview:tempTV];
	self.resultTable = tempTV;
	ObjCRelease(tempTV);
}

- (void) initMapView
{
	CGRect appRect = [UIScreen mainScreen].bounds; 
	UIImageView *bgIV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"plainBg.png"]];
	bgIV.frame = appRect;
	[self.view addSubview:bgIV];
	ObjCRelease(bgIV);
	
	UIImageView *imgV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"powered-by-google-on-white@2x.png"]];
	imgV.frame = CGRectMake(216, 350, 104, 16);
	
	MKMapView *tempMV =[[MKMapView alloc] initWithFrame:appRect];
	tempMV.showsUserLocation=YES;
	tempMV.delegate=self;
	
	/*Region and Zoom*/
	MKCoordinateRegion region;
	
	LocationManager *locMgr = [LocationManager sharedSingleton];
	CLLocationCoordinate2D location = locMgr.userLocation.coordinate;
	tipPlaceMarker = [[PlaceMarker alloc] initWithCoordinate:location title:@"Map" subtitle:@"Near you"];
	
	region.span = [self currentMapSpan];
	region.center = location;
	
	/*Geocoder Stuff*/
	
	geoCoder=[[MKReverseGeocoder alloc] initWithCoordinate:location];
	geoCoder.delegate=self;
	[geoCoder start];
	
	googleLogo = imgV;
	[tempMV addSubview:googleLogo];
	[imgV release];
	
	if ([tempMV respondsToSelector:@selector(setRegion:animated:)]) {
		[tempMV setRegion:region animated:YES];
	}
	else {
		[tempMV setRegion:region];
	}
	[tempMV regionThatFits:region];
	tempMV.mapType = MKMapTypeStandard;
	[self.view addSubview:tempMV];
	self.tipMapView = tempMV;
	ObjCRelease(tempMV);
    
    NSArray * items = [NSArray arrayWithObjects:@"Map", @"Satellite", @"Hybrid", nil];
    UISegmentedControl * segControl = [[UISegmentedControl alloc] initWithItems:items];
    segControl.frame = CGRectMake(170, 15, 150, 30);
    segControl.segmentedControlStyle = UISegmentedControlStyleBar;
    [segControl setSelectedSegmentIndex:0];
    segControl.backgroundColor = [UIColor clearColor];
    segControl.tintColor = [UIColor darkGrayColor];
	segControl.alpha = 0.8F;
    [segControl setSelectedSegmentIndex:0];
    [segControl addTarget:self action:@selector(changeMaptype:) forControlEvents:UIControlEventValueChanged];   
    
    [self.tipMapView addSubview:segControl];
    ObjCRelease(segControl);
}

- (void) changeMaptype:(id) sender
{
    UISegmentedControl * _segmentControl = (UISegmentedControl *) sender;
    int index = _segmentControl.selectedSegmentIndex;
    
    switch (index) {
        case 0:
            self.tipMapView.mapType = MKMapTypeStandard;
            break;
        case 1:
            self.tipMapView.mapType = MKMapTypeSatellite;
            break;
        case 2:
            self.tipMapView.mapType = MKMapTypeHybrid;
            break;
        default:
            break;
    }
}

// Override initWithNibName:bundle: to load the view using a nib file then perform additional customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andSearchType:(NSInteger)srchType
{	
	SafeLog(@"MapViewController initWithNibName.");
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
		
		//self.title = @"Map";
		NSMutableArray *tempArr = [[NSMutableArray alloc] initWithCapacity:0];
		self.resultsArray = tempArr;
		ObjCRelease(tempArr);
    
        NSMutableArray *conArr = [[NSMutableArray alloc] initWithCapacity:0];
		self.connectionArr = conArr;
		ObjCRelease(conArr);
        // Custom initialization
		tagValue = 0;
		numOfConnections = 0;
		self.searchType = srchType;
		[self.view setAutoresizesSubviews:YES];
		
		// create a toolbar to have two buttons in the right
		H5CToolBar* tools = [[H5CToolBar alloc] initWithFrame:CGRectMake(200, 0, 90, 44.018)];
		tools.tintColor = [UIColor clearColor];
		tools.backgroundColor = [UIColor clearColor];
		
		// create the array to hold the buttons, which then gets added to the toolbar
		NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:2];
		
		// create a standard "refresh" button
		UIBarButtonItem* bi = [[UIBarButtonItem alloc]
							   initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(centerMapToCurrentLocation)];
		bi.style = UIBarButtonItemStyleBordered;
		[buttons addObject:bi];
		[bi release];
		
		// create a standard "add" button		
		bi = [[UIBarButtonItem alloc]
			  initWithTitle:@"List" style:UIBarButtonItemStyleDone target:self action:@selector(toggleListOrMap)];
		bi.style = UIBarButtonItemStyleBordered;
		[buttons addObject:bi];
		[bi release];
		
		// stick the buttons in the toolbar
		[tools setItems:buttons animated:NO];
		
		[buttons release];
		
		UIBarButtonItem *item = [[[UIBarButtonItem alloc] initWithCustomView:tools] autorelease];
		[tools release];
		
		// and put the toolbar in the nav bar
		[self.navigationItem setRightBarButtonItem:item animated:YES];
		
		UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back"
																	   style:UIBarButtonItemStylePlain target:nil action:nil];
		self.navigationItem.backBarButtonItem = backButton;
		[backButton release];
		
		self.previousState = self.currentState = kSearchStateNormal;
		
        currentRadius = [[Settings sharedSettings].resultSetRadius intValue];
		[self initMapView];
		[self initTableView];
		[self initData];
		isMap = YES;
		self.isFirstTime = YES;
    }
	
	return self;
}

- (void) reloadData
{
    for (UIWindow* window in [UIApplication sharedApplication].windows) {
		NSArray* subviews = window.subviews;
		if ([subviews count] > 0)
			if ([[subviews objectAtIndex:0] isKindOfClass:[UIAlertView class]])
				[[subviews objectAtIndex:0] dismissWithClickedButtonIndex:5 animated:NO];
	}

	UILabel *lbl = (UILabel *)[self.resultTable viewWithTag:2001];
	if (lbl != nil)
	{
		[lbl removeFromSuperview];
	}
	currentRadius = [[Settings sharedSettings].resultSetRadius intValue];
	if ([self izzAllWell])
	{
		[self clearData];
	}
	
	if (![self.tipMapView isHidden])
	{
		MKCoordinateRegion region;
        CLLocationCoordinate2D location = [LocationManager sharedSingleton].userLocation.coordinate;
		region.span = [self currentMapSpan];
		region.center = location;
        
		if ([self.tipMapView respondsToSelector:@selector(setRegion:animated:)]) {
			[self.tipMapView setRegion:region animated:YES];
		}
		else
		{
			[self.tipMapView setRegion:region];
		}
	}
    
	[self initData];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	tagValue = 0;
	isViewVisible = YES;
	isGoingToDetails = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	tagValue = 0;
	isViewVisible = NO;
	if (!isGoingToDetails)
	{
		for (ConnectionManager *conn in self.connectionArr)
		{
			[conn setDelegate:nil];
		}
		[self.connectionArr removeAllObjects];
	}
	//self.adView.delegate = nil;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	//[self handleAlternateToNormal];
	/** AdWhril code **/
	self.adView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
	self.adView.autoresizingMask =
    UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
	CGRect adFrame = [adView frame];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
	adFrame.origin.y = screenBounds.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
	[adView setFrame:adFrame];
	if (getenv("ADWHIRL_FAKE_DARTS")) {
		// To make ad network selection deterministic
		const char *dartcstr = getenv("ADWHIRL_FAKE_DARTS");
		NSArray *rawdarts = [[NSString stringWithCString:dartcstr]
							 componentsSeparatedByString:@" "];
		NSMutableArray *darts
		= [[NSMutableArray alloc] initWithCapacity:[rawdarts count]];
		for (NSString *dartstr in rawdarts) {
			if ([dartstr length] == 0) {
				continue;
			}
			[darts addObject:[NSNumber numberWithDouble:[dartstr doubleValue]]];
		}
		self.adView.testDarts = darts;
	}
	
	UIDevice *device = [UIDevice currentDevice];
	if ([device respondsToSelector:@selector(isMultitaskingSupported)] &&
		[device isMultitaskingSupported]) {
		[[NSNotificationCenter defaultCenter]
		 addObserver:self
		 selector:@selector(enterForeground:)
		 name:UIApplicationDidBecomeActiveNotification
		 object:nil];
	}
	[self.view addSubview:self.adView];
	/** End: Adwhril Code **/
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	//self.adView.delegate = self;
	[self adjustAdSize];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	self.adView = nil;
	NSLogStartMethod;
}


- (void)dealloc {
	NSLogStartMethod;
	SafeLog(@"Map view controller dealloced");
	
	if(self.tipMapView != nil)
	{
		self.tipMapView.delegate = nil;
		self.tipMapView = nil;
	}
	
	if(geoCoder != nil)
	{
		[geoCoder setDelegate:nil];
		[geoCoder cancel];
		[geoCoder release];
	}
	
	if(mPlacemark != nil)
		[mPlacemark release];
	
	if(tipPlaceMarker != nil)
		[tipPlaceMarker release];
	
	self.resultTable = nil;
	self.resultsArray = nil;
	self.searchArr = nil;
	self.adView.delegate = nil;
	self.adView = nil;
	self.userLocality = nil;
	self.connectionArr = nil;
    
	[super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown
            || interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
                                         duration:(NSTimeInterval)duration {
	[self adjustLayoutToOrientation:interfaceOrientation];
}

#pragma mark - MKReverseGeocoderDelegate methods

- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error{
	SafeLog(@"Reverse Geocoder Errored");
    [self updateUserCurrentLocalityIsOffline:NO];
}

- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark{
	SafeLog(@"Reverse Geocoder completed");
	NSMutableString *locality = [NSMutableString stringWithFormat:@"%@, %@", placemark.subLocality, placemark.locality];
	[locality replaceOccurrencesOfString:@"(null), " withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [locality length])];
	[locality replaceOccurrencesOfString:@"(null)" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [locality length])];
	self.userLocality = locality;
	[self updateUserCurrentLocalityIsOffline:NO];
}


#pragma mark - MKMapViewDelegate methods
- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation{
	if(annotation != mapView.userLocation) {
		MKPinAnnotationView *anv = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
		anv.canShowCallout = YES;
		[anv setPinColor:MKPinAnnotationColorGreen];
		
 		UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
		
		int i = 0;
		for (; i < [self.resultsArray count]; i++)
		{
			SearchResult *result = [self.resultsArray objectAtIndex:i];
			if ([result.name isEqualToString:[annotation title]])
			{
				break;
			}
		}
		rightButton.tag = i;
	    [rightButton addTarget:self action:@selector(annotationViewClick:) forControlEvents:UIControlEventTouchUpInside];
		anv.rightCalloutAccessoryView = rightButton;
		return anv;
	}
	
	// Return nil for default view for annotation - blue radar style
	return nil;
}

- (IBAction) annotationViewClick:(id) sender {
	isGoingToDetails = YES;
	UIButton *btn = (UIButton *)sender;
	NSInteger selectedIndex = btn.tag;
	SearchResult *result = [self.resultsArray objectAtIndex:selectedIndex];
	CityDetailsViewController *cityDet = [[CityDetailsViewController alloc] initWithNibName:@"CityDetailsViewController" bundle:nil andSelectedResult:result];
	cityDet.parentVC = self;
	[self.navigationController pushViewController:cityDet animated:YES];
	[cityDet release];
}

- (void) selectPoint:(id) selectedPoint
{
	isMap = NO;
	SearchResult *selectedRes = (SearchResult *) selectedPoint;
	NSArray *annArr = self.tipMapView.annotations;
	for (PlaceMarker *pm in annArr)
	{
		if ([selectedRes.name isEqualToString:pm.title])
		{
			[self.tipMapView selectAnnotation:pm animated:YES];
			break;
		}
	}
	[self toggleListOrMap];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger count = [self.resultsArray count];
	return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;
	static NSString *CellIdentifier = @"Cell";
	cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		//cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        UIImageView * imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carepointsListBgwithArrow.png"]];
        cell.backgroundView = imgView;
        ObjCRelease(imgView);
		
		cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		
		CGRect detRect = CGRectMake(5, 0, 320, 20);
		
		UILabel *detLbl = [[UILabel alloc] initWithFrame:detRect];
		detLbl.tag = 100;
		detLbl.font = [UIFont fontWithName:@"Arial-BoldMT" size:14.0];
		detLbl.numberOfLines = 3;
		detLbl.backgroundColor = [UIColor clearColor];
		detLbl.textColor = [UIColor whiteColor];
		[cell.contentView addSubview:detLbl];
		ObjCRelease(detLbl);
		
		detRect = CGRectMake(5, 20, 320, 56);
		detRect.size.width -= 85.0;
		detLbl = [[UILabel alloc] initWithFrame:detRect];
		detLbl.tag = 101;
		detLbl.font = [UIFont fontWithName:@"ArialMT" size:12.0];
		detLbl.numberOfLines = 3;
		detLbl.backgroundColor = [UIColor clearColor];
		detLbl.textColor = [UIColor whiteColor];
		
		[cell.contentView addSubview:detLbl];
		ObjCRelease(detLbl);
		
		detRect.origin.x += detRect.size.width;
		detRect.size.width = 80.0f;
		detRect.size.height = 20.0f;
		detLbl = [[UILabel alloc] initWithFrame:detRect];
		detLbl.tag = 102;
		detLbl.backgroundColor = [UIColor clearColor];
		detLbl.font = [UIFont fontWithName:@"Arial-BoldMT" size:12.0];
		detLbl.textColor = [UIColor whiteColor];
		
		[cell.contentView addSubview:detLbl];
		ObjCRelease(detLbl);
	}
	SearchResult *place = [self.resultsArray objectAtIndex:indexPath.row];
	UILabel *detLbl = (UILabel *)[cell.contentView viewWithTag:100];
	detLbl.text = place.name;
	detLbl = nil;
	
	detLbl = (UILabel *)[cell.contentView viewWithTag:101];
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[UIApplication sharedApplication].delegate;
	if (appDel.isGoogle)
	{
		detLbl.text = place.vicinity;
	}
	else
	{
		detLbl.text = place.address;
	}
	detLbl = nil;
	
	detLbl = (UILabel *)[cell.contentView viewWithTag:102];
	if ([Settings sharedSettings].isDistanceInMiles)
	{
		detLbl.text = [NSString stringWithFormat:@"~%.2lf Miles",[[LocationManager sharedSingleton] distanceForLatitude:place.lat andLongitude:place.lng inMiles:YES]];
	}
	else
	{
		detLbl.text = [NSString stringWithFormat:@"~%.2lf Kms",[[LocationManager sharedSingleton] distanceForLatitude:place.lat andLongitude:place.lng inMiles:NO]];
	}
	detLbl = nil;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	isGoingToDetails = YES;
	SearchResult *selectedResult = [self.resultsArray objectAtIndex:indexPath.row];
	CityDetailsViewController *cityDet = [[CityDetailsViewController alloc] initWithNibName:@"CityDetailsViewController" bundle:nil andSelectedResult:selectedResult];
	cityDet.parentVC = self;
	[self.navigationController pushViewController:cityDet animated:YES];
	[cityDet release];
}

#pragma mark - AdWhril delegate methods
- (NSString *)admobPublisherID {
	return kAdModPublisherID;
}

- (NSString *)adWhirlApplicationKey {
	return kAdWhrilAppKey;
}

- (UIViewController *)viewControllerForPresentingModalView {
	CarePointzAppDelegate *appDel = (CarePointzAppDelegate *)[[UIApplication sharedApplication] delegate];
	return appDel.window.rootViewController;
}

- (NSURL *)adWhirlConfigURL {
	return [NSURL URLWithString:kConfigURL];
}

- (NSURL *)adWhirlImpMetricURL {
	return [NSURL URLWithString:kImpMetricURL];
}

- (NSURL *)adWhirlClickMetricURL {
	return [NSURL URLWithString:kClickMetricURL];
}

- (NSURL *)adWhirlCustomAdURL {
	return [NSURL URLWithString:kCustomAdURL];
}

- (void)adWhirlDidReceiveAd:(AdWhirlView *)adWhirlView {
	errStr = [NSString stringWithFormat:
			  @"Got ad from %@, size %@",
			  [adWhirlView mostRecentNetworkName],
			  NSStringFromCGSize([adWhirlView actualAdSize])];
	[self adjustAdSize];
}

- (void)adWhirlDidFailToReceiveAd:(AdWhirlView *)adWhirlView usingBackup:(BOOL)yesOrNo {
	errStr = [NSString stringWithFormat:
			  @"Failed to receive ad from %@, %@. Error: %@",
			  [adWhirlView mostRecentNetworkName],
			  yesOrNo? @"will use backup" : @"will NOT use backup",
			  adWhirlView.lastError == nil? @"no error" : [adWhirlView.lastError localizedDescription]];
	[self adjustAdSize];
}

- (void)adWhirlReceivedRequestForDeveloperToFufill:(AdWhirlView *)adWhirlView {
	UILabel *replacement = [[UILabel alloc] initWithFrame:kAdWhirlViewDefaultFrame];
	replacement.backgroundColor = [UIColor redColor];
	replacement.textColor = [UIColor whiteColor];
	replacement.textAlignment = UITextAlignmentCenter;
	replacement.text = @"Generic Notification";
	[adWhirlView replaceBannerViewWith:replacement];
	[replacement release];
	[self adjustAdSize];
	errStr = @"Generic Notification";
}

- (void)adWhirlReceivedNotificationAdsAreOff:(AdWhirlView *)adWhirlView {
	errStr = @"Ads are off";
}

- (void)adWhirlWillPresentFullScreenModal {
	SafeLog(@"SimpleView: will present full screen modal");
}

- (void)adWhirlDidDismissFullScreenModal {
	SafeLog(@"SimpleView: did dismiss full screen modal");
}

- (void)adWhirlDidReceiveConfig:(AdWhirlView *)adWhirlView {
	errStr = @"Received config. Requesting ad...";
}

- (BOOL)adWhirlTestMode {
	return NO;
}

- (CLLocation *)locationInfo {
	return [LocationManager sharedSingleton].userLocation;
}

#pragma mark event methods

- (void)performEvent {
	errStr = @"Event performed";
}

- (void)performEvent2:(AdWhirlView *)adWhirlView {
	UILabel *replacement = [[UILabel alloc] initWithFrame:kAdWhirlViewDefaultFrame];
	replacement.backgroundColor = [UIColor blackColor];
	replacement.textColor = [UIColor whiteColor];
	replacement.textAlignment = UITextAlignmentCenter;
	replacement.text = [NSString stringWithFormat:@"Event performed, view %x", adWhirlView];
	[adWhirlView replaceBannerViewWith:replacement];
	[replacement release];
	[self adjustAdSize];
	errStr = [NSString stringWithFormat:@"Event performed, view %x", adWhirlView];
}

#pragma mark multitasking methods

- (void)enterForeground:(NSNotification *)notification {
	AWLogDebug(@"SimpleView entering foreground");
	[[Settings sharedSettings] updateSettings];
//	for (UIWindow* window in [UIApplication sharedApplication].windows) {
//		NSArray* subviews = window.subviews;
//		if ([subviews count] > 0)
//			if ([[subviews objectAtIndex:0] isKindOfClass:[UIAlertView class]])
//				[[subviews objectAtIndex:0] dismissWithClickedButtonIndex:5 animated:NO];
//	}
	if (![self.tipMapView isHidden]) {
		[self.tipMapView setNeedsDisplay];
	}
	[self.resultTable reloadData];
	[self.adView updateAdWhirlConfig];
}

- (void)adjustLayoutToOrientation:(UIInterfaceOrientation)newOrientation {
	CGRect adFrame = [adView frame];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
	adFrame.origin.y = screenBounds.size.height	- adFrame.size.height - self.navigationController.navigationBar.frame.size.height- [UIApplication sharedApplication].statusBarFrame.size.height;
	[adView setFrame:adFrame];
}

- (void)adjustAdSize {
	CGRect adFrame = [adView frame];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
	adFrame.origin.y = screenBounds.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height;
	[adView setFrame:adFrame];
	[UIView beginAnimations:@"AdResize" context:nil];
	[UIView setAnimationDuration:0.7];
	CGSize adSize = [adView actualAdSize];
	CGRect newFrame = adView.frame;
	newFrame.size.height = adSize.height;
	newFrame.size.width = adSize.width;
	newFrame.origin.x = (self.view.frame.size.width - adSize.width)/2;
	newFrame.origin.y = self.view.frame.size.height - adSize.height;
	adView.frame = newFrame;
	
	float posHeightNumMV = self.tipMapView.frame.origin.y + self.tipMapView.frame.size.height;
	float posHeightNumTV = self.resultTable.frame.origin.y + self.resultTable.frame.size.height;
	
	if (posHeightNumMV == posHeightNumTV)
	{
		if (posHeightNumMV > newFrame.origin.y)
		{
			CGRect newFrame1 = self.tipMapView.frame;
			newFrame1.size.height = posHeightNumMV - newFrame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height/2 + 3;
			self.tipMapView.frame = newFrame1;
			newFrame1.origin.y = 14.0f;
			newFrame1.size.height -= 14.0f;
			self.resultTable.frame = newFrame1;
		}
	}
	[UIView commitAnimations];
	if (!self.adView.adExists)
	{
		if (self.tipMapView.frame.size.height <= newFrame.origin.y)
		{
			CGRect newFrame1 = self.tipMapView.frame;
			newFrame1.size.height = posHeightNumMV;
			self.tipMapView.frame = newFrame1;
			newFrame1.origin.y = 14.0f;
			newFrame1.size.height -= 14.0f;
			self.resultTable.frame = newFrame1;
		}
	}
}

#pragma mark - Alternate Handling
- (void) _loadAlternate
{
	self.previousState = self.currentState;
	self.currentState = kSearchStateAlternate;
	[self connectWithName:alternateSearchString andConnectionID:alternateSearchString];
}

#pragma mark - UIAlertView delegate method
- (void) dismissTimedAlert
{
	[timedAlert dismissWithClickedButtonIndex:5 animated:YES];
	[timedAlert release];
	timedAlert = nil;
}

- (void) showAlert:(UIAlertView *)alert ofType:(AlertType) alertType
{
	if (isViewVisible)
	{
		// Check all conditions required before showing an alert
		switch (alertType)
		{
			case kAlertTypeAlternate2Button:
			{
				alternate2Alert = [alert retain];
				[alternate2Alert show];
			}
				break;
			case kAlertTypeAlternate3Button:
			{
				isIncAltPromptShown = YES;
				alternate3Alert = [alert retain];
				[alternate3Alert show];
			}
				break;
			case kAlertTypeTimed:
			{
				if (timedAlert)
				{
					[self dismissTimedAlert];
				}
				timedAlert = [alert retain];
				[NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(dismissTimedAlert) userInfo:nil repeats:NO];
				[timedAlert show];
			}
				break;
			case kAlertTypeAlternateToNormal:
			{
				alternateToNormalAlert = [alert retain];
				[alternateToNormalAlert show];
			}
				break;
			case kAlertTypeGeneral:
				[alert show];
				break;
			default:
				[alert show];
				break;
		}
	}
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alternate3Alert == alertView)
	{
		switch (buttonIndex)
		{
			case 1:
			{
				[self currentSearchRadius];
				[self createAndSearchURLs];
			}
				break;
			case 2:
			{
				// Now we need to load alternate category
				[self _loadAlternate];
			}
				break;
			default:
			{
				if (self.isFirstTime)
				{
					self.isFirstTime = NO;
					[self.navigationController popViewControllerAnimated:YES];
				}
				else
				{
					// Just show the previous data/ blank!
					[self showData];
				}
			}
				break;
		}
		[alternate3Alert release];
		alternate3Alert = nil;
	}
	else if (alternate2Alert == alertView)
	{
		isIncAltPromptShown = NO;
		switch (buttonIndex)
		{
			case 0:
			{
				// Now we need to load alternate category
				[self _loadAlternate];
			}
				break;
			default:
			{
				if (self.isFirstTime)
				{
					self.isFirstTime = NO;
					[self.navigationController popViewControllerAnimated:YES];
				}
				else
				{
					// Just show the previous data/ blank!
					[self showData];
				}
			}
				break;
		}
		[alternate2Alert release];
		alternate2Alert = nil;
	}
	else if (alternateToNormalAlert == alertView)
	{
		switch (buttonIndex)
		{
			case 1:
			{
				// Now we need to load the result
				[self showData];
			}
				break;
			case 2:
			{
				// Load Alternate now
				[self _loadAlternate];
			}
				break;
			default:
				break;
		}
		[alternateToNormalAlert release];
		alternateToNormalAlert = nil;
	}
}

@end
