//
//  Settings.m
//  CarePointz
//
//  Created by hemanth kp on 4/11/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import "Settings.h"
#import <safeObjC.h>

#define LocationUpdateKey @"locaton_update_preference"
#define ResultsRadiusKey @"results_radius_preference"
#define ClearCacheKey @"clear_cache_preference"
#define MilesKMKey @"distance_in_miles_preference"

static Settings *sharedSettings = nil;

@interface Settings ()
- (void) _readFromUserDefaults;
- (void) _synchronizeWithUserDefaults;
@end

@implementation Settings

@synthesize locUpdateMeters;
@synthesize resultSetRadius;
@synthesize clearCachePeriod;
@synthesize isDistanceInMiles;

- (void)dealloc
{
	self.locUpdateMeters = nil;
	self.resultSetRadius = nil;
	[super dealloc];
}

- (id)init
{
	self = [super init];
	if (self)
	{
		[self _readFromUserDefaults];
	}
	return self;
}

+ (Settings *) sharedSettings
{
	if (sharedSettings == nil)
	{
		sharedSettings = [[Settings  alloc] init];
	}
	return sharedSettings;
}

- (void) _synchronizeWithUserDefaults
{
	NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
	[defs synchronize];
	
	NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
	
	if(!settingsBundle)
	{
		SafeLog(@"Could not find Settings.bundle");
		return;
	}
	
	NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
	NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
	NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
	
	for (NSDictionary *prefSpecification in preferences)
	{
		NSString *key = [prefSpecification objectForKey:@"Key"];
		if (key)
		{
			// check if value readable in userDefaults
			id currentObject = [defs objectForKey:key];
			if (currentObject == nil)
			{
				// not readable: set value from Settings.bundle
				id objectToSet = [prefSpecification objectForKey:@"DefaultValue"];
				[defaultsToRegister setObject:objectToSet forKey:key];
				SafeLog(@"Setting object %@ for key %@", objectToSet, key);
			}
			else
			{
				// already readable: don't touch
				SafeLog(@"Key %@ is readable (value: %@), nothing written to defaults.", key, currentObject);
			}
		}
	}
	
	[defs registerDefaults:defaultsToRegister];
	[defaultsToRegister release];
	[defs synchronize];
}

- (void) _readFromUserDefaults
{
	NSNumber *tempNum = [[NSUserDefaults standardUserDefaults] objectForKey:LocationUpdateKey];
	if (tempNum == nil)
	{
		[self _synchronizeWithUserDefaults];
	}
	else
	{
		self.locUpdateMeters = tempNum;
		tempNum = nil;
	}
	
	tempNum = [[NSUserDefaults standardUserDefaults] objectForKey:ResultsRadiusKey];
	if (tempNum == nil)
	{
		[self _synchronizeWithUserDefaults];
	}
	else
	{
		self.resultSetRadius = tempNum;
		tempNum = nil;
	}
	
	tempNum = [[NSUserDefaults standardUserDefaults] objectForKey:ClearCacheKey];
	if (tempNum == nil)
	{
		[self _synchronizeWithUserDefaults];
	}
	else
	{
		self.clearCachePeriod = tempNum;
		tempNum = nil;
	}
	self.isDistanceInMiles = [[NSUserDefaults  standardUserDefaults] boolForKey:MilesKMKey];
}

- (void) updateSettings
{
	[self _readFromUserDefaults];
}

@end
