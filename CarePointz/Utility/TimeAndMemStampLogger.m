//
//  TimeAndMemStampLogger.m
//  TEST
//
//  Created by hemanth kp on 3/7/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import "TimeAndMemStampLogger.h"
#import <mach/mach.h>
#import <safeObjC.h>

#define ConvertToKB(bytesData) bytesData = bytesData/1024

@interface TimeAndMemStampLogger()
void report_memory(void);
void report_memory_with_leaks(void);
@end

@implementation TimeAndMemStampLogger
@synthesize logger;

void report_memory(void)
{
	struct task_basic_info info;
	mach_msg_type_number_t size = sizeof(info);
	kern_return_t kerr = task_info(mach_task_self(),
								   TASK_BASIC_INFO,
								   (task_info_t)&info,
								   &size);
	if( kerr == KERN_SUCCESS ) {
		SafeLog(@"Memory in use (in bytes): %u", info.resident_size);
	} else {
		SafeLog(@"Error with task_info(): %s", mach_error_string(kerr));
	}
}

void report_memory_with_leaks(void)
{
	static unsigned last_resident_size=0;
    static unsigned greatest = 0;
    static unsigned last_greatest = 0;
	
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(),
								   TASK_BASIC_INFO,
								   (task_info_t)&info,
								   &size);
    if( kerr == KERN_SUCCESS ) {
        int diff = (int)info.resident_size - (int)last_resident_size;
        unsigned latest = info.resident_size;
        if( latest > greatest   )   greatest = latest;  // track greatest mem usage
        int greatest_diff = greatest - last_greatest;
        int latest_greatest_diff = latest - greatest;
        SafeLog(@"Mem: %10u kb (%10d) : %10d :   greatest: %10u (%d)", ConvertToKB(info.resident_size), diff,
			  latest_greatest_diff,
			  greatest, greatest_diff  );
    } else {
        SafeLog(@"Error with task_info(): %s", mach_error_string(kerr));
    }
    last_resident_size = info.resident_size;
    last_greatest = greatest;
}

- (void) startTimer
{
	self.logger = [NSDate date];
}

- (void) endTimerAndLog
{
	NSDate *now = [NSDate date];
	SafeLog(@"%lf seconds",[now timeIntervalSinceDate:self.logger]);
}

- (void) logMemory
{
	report_memory();
}

- (void) logMemoryWithLeaks
{
	report_memory_with_leaks();
}

- (void)dealloc {
    self.logger = nil;
    [super dealloc];
}

@end
