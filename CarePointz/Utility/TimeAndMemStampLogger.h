//
//  TimeAndMemStampLogger.h
//  TEST
//
//  Created by hemanth kp on 3/7/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeAndMemStampLogger : NSObject
{
	NSDate *logger;
}

@property (retain, nonatomic) NSDate * logger;

- (void) startTimer;
- (void) endTimerAndLog;

- (void) logMemory;
- (void) logMemoryWithLeaks;

@end
