//
//  AppConstants.h
//  CarePointz
//
//  Created by hemanth kp on 4/12/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#ifndef CarePointz_AppConstants_h
#define CarePointz_AppConstants_h

#import "AdWhirlView.h"
#import "AdWhirlView+.h"
#import "AdWhirlLog.h"

#define kGoogleApiKey @"AIzaSyBHMZwLYmebI57OUD9l25YLwJdH3PPThWc"
#define kAdWhrilAppKey @"6caf366f505f40d0959ab28d487aa42b"
#define kAdModPublisherID @"a14f86681dce575"

#define kConfigURL @"http://mob.adwhirl.com/getInfo.php"
#define kImpMetricURL @"http://met.adwhirl.com/exmet.php"
#define kClickMetricURL @"http://met.adwhirl.com/exclick.php"
#define kCustomAdURL @"http://mob.adwhirl.com/custom.php"

#endif
