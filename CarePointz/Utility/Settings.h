//
//  Settings.h
//  CarePointz
//
//  Created by hemanth kp on 4/11/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Settings : NSObject
{
	NSNumber *locUpdateMeters;
	NSNumber *resultSetRadius;
	NSNumber *clearCachePeriod;
	BOOL isDistanceInMiles;
}

@property (nonatomic, retain) NSNumber *locUpdateMeters;
@property (nonatomic, retain) NSNumber *resultSetRadius;
@property (nonatomic, retain) NSNumber *clearCachePeriod;
@property (nonatomic) BOOL isDistanceInMiles;

+ (Settings *) sharedSettings;
- (void) updateSettings;

@end
