//
//  CarePointzAppDelegate.m
//  CarePointz
//
//  Created by hemanth kp on 3/30/12.
//  Copyright (c) 2012 team-mobi. All rights reserved.
//

#import "CarePointzAppDelegate.h"
#import "TimeAndMemStampLogger.h"
#import "ViewController.h"
#import "MapViewController.h"
#import "LocationManager.h"
#import "SQLiteManager.h"
#import "Settings.h"
#import "AppConstants.h"
#import <safeObjc.h>

#define k_SPLASH_VIEW_TAG 100
#define k_LOADING_INDICATOR_TAG 101

#define kLoadingAlertFrame			CGRectMake(50, 0, 180, 120)
#define kScreenCenter				CGPointMake(160,240)
#define kActivityIndicatorFrame		CGRectMake(45, 15, 40, 40)
#define kLoadingLabelFrame			CGRectMake(100,-5,180,80)
#define k_ACTIVITY_INDICATOR_TAG		2
#define k_LOADING_LABEL_TAG				3
#define HEADER_FONT_SIZE		14.0f
#define SYSTEM_BOLD_FONT		[UIFont fontWithName:@"Arial-BoldMT" size:14]
#define DB_PATH [[NSBundle mainBundle] pathForResource:@"searches" ofType:@"db"]

@implementation CarePointzAppDelegate

@synthesize window;
@synthesize viewController = _viewController;
@synthesize searchString;
@synthesize searchDetailsURL;
@synthesize currentLat;
@synthesize currentLng;
@synthesize updateLocation;
@synthesize isGoogle;
@synthesize mrMgr = _mrMgr;

- (void)dealloc
{
	[self.window release];
	[_viewController release];
	self.searchString = nil;
	self.searchDetailsURL = nil;
    [super dealloc];
}

#pragma mark - Private Methods
- (BOOL) deleteAllCaches
{
	BOOL isSuccessful = NO;
	NSUInteger weeksInterval = [[NSDate date] timeIntervalSince1970];
	NSInteger weeksVal = [[Settings sharedSettings].clearCachePeriod intValue];
	weeksInterval = weeksInterval - weeksVal;
	NSString *delStr = [NSString stringWithFormat:@"delete from CPSearchResults where lastAccessed < %u;",weeksInterval];
	
	SQLiteQuery *query = [self.mrMgr queryWithQueryString: delStr
									  withReuseIdentifier: nil
											   withFormat: nil];
	
	@try {
		[query executeWithHandler: nil] ;
		isSuccessful = YES;
	}
	@catch (NSException *exception) {
		isSuccessful = NO;
	}
	return isSuccessful;
}

- (void) _showSplash 
{
	UIView *splashView = nil ;
	CGRect rect = [[UIScreen mainScreen] bounds];
	splashView = [[UIView alloc] initWithFrame: rect ];
	splashView.frame = rect;
	splashView.tag = k_SPLASH_VIEW_TAG;
	
	NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Default" ofType:@"png"];
	UIImage *splashImage = [[UIImage alloc] initWithContentsOfFile:filePath];
	filePath = nil;
	
	UIImageView *splashImageView = [[UIImageView alloc] initWithFrame: splashView.bounds];
	splashImageView.image = splashImage;
	[splashImage release];
	splashImage = nil;
	
	[splashView addSubview:splashImageView];
	[splashImageView release];
	
	UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(80, 340, 180, 100)];
	lbl.text = @"Loading...";
	lbl.textAlignment = UITextAlignmentCenter;
	lbl.textColor = [UIColor whiteColor];
	lbl.tag = k_LOADING_LABEL_TAG;
	lbl.backgroundColor = [UIColor clearColor];
	lbl.numberOfLines = 0;
	lbl.adjustsFontSizeToFitWidth = YES;
	lbl.font = SYSTEM_BOLD_FONT;
	lbl.lineBreakMode = UILineBreakModeWordWrap;
	[splashView addSubview:lbl];
	[lbl release];
	
	UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc] 
												 initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	loadingIndicator.frame = CGRectMake(140,340, 40, 40);
	loadingIndicator.tag = k_LOADING_INDICATOR_TAG;	
	[loadingIndicator startAnimating];
	
	[self.window addSubview:splashView];
	[self.window addSubview:loadingIndicator];
	[self.window makeKeyAndVisible];
	
    [loadingIndicator release];
    [splashView release];
}

- (void) _hideSplash {
	UIView *splashControllerView = (UIView *)[self.window viewWithTag:k_SPLASH_VIEW_TAG];
	UIActivityIndicatorView *loadingIndicator = (UIActivityIndicatorView *) [self.window viewWithTag:k_LOADING_INDICATOR_TAG];
	
	[loadingIndicator stopAnimating];
	[loadingIndicator removeFromSuperview];
	loadingIndicator = nil;
	
	[splashControllerView removeFromSuperview];
	splashControllerView = nil;
}

#pragma mark - LocationManagerDelegate methods
-(void)newLocationUpdate
{
	LocationManager *locMgr = [LocationManager sharedSingleton];
	BOOL isMiles = [Settings sharedSettings].isDistanceInMiles;
	NSInteger distanceFilter = [[Settings sharedSettings].locUpdateMeters integerValue];
	
	if ((self.currentLat == 0
		&& self.currentLng == 0)
		|| [[LocationManager sharedSingleton] distanceForLatitude:self.currentLat andLongitude:self.currentLng inMiles:isMiles] >= distanceFilter)
	{
		self.currentLat = locMgr.userLocation.coordinate.latitude;
		self.currentLng = locMgr.userLocation.coordinate.longitude;
		
		SafeLog(@" Got location with lat = %lf and lng = %lf",self.currentLat,self.currentLng);
		if (self.window.rootViewController == nil)
		{
			[self _hideSplash];
			UIViewController *vc = [[[ViewController alloc] initWithNibName:nil bundle:nil] autorelease];
			UINavigationController *nactrl = [[[UINavigationController alloc] initWithRootViewController:vc] autorelease];
			nactrl.navigationBar.tintColor = [UIColor blackColor];
			self.window.rootViewController = nactrl;
			[self.window makeKeyAndVisible];
		}
		
		else
		{
			UINavigationController *rtVC = (UINavigationController *)self.window.rootViewController;
			if ([[rtVC viewControllers] count] > 1)
			{
				MapViewController *mapVc = [[rtVC viewControllers] objectAtIndex:1];
				mapVc.previousState = mapVc.currentState;
				mapVc.currentState = kSearchStateNormal;
				mapVc.alternateFailCount = 0;
				[mapVc reloadData];
			}
		}
	}
}

-(void)newError:(NSString *)text
{
	SafeLog(@" Location error: %@!!", text);
	NSString *msg = nil;
	msg = NSLocalizedString(@"No_Gps", @"");
	UILabel *splashLbl = (UILabel *)[self.window viewWithTag:k_LOADING_LABEL_TAG];	
	if (splashLbl != nil)
	{
		CGRect lblRect = splashLbl.frame;
		lblRect.origin.x = 10;
		lblRect.size.width = 300;
		lblRect.size.height = 80;
		splashLbl.frame = lblRect;
		UIActivityIndicatorView *loadingIndicator = (UIActivityIndicatorView *) [self.window viewWithTag:k_LOADING_INDICATOR_TAG];
		
		[loadingIndicator stopAnimating];
		[loadingIndicator removeFromSuperview];
		loadingIndicator = nil;	
		msg = NSLocalizedString(@"No_Gps_CheckSettings", @"");
		splashLbl.text = msg;
	}
	else
	{
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Error" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
	}
}

- (NSString *) baseURL
{
	NSString *str = nil;
	NSInteger resRad = [[[Settings sharedSettings] resultSetRadius] intValue];
	if (self.isGoogle)
	{
		[self setBaseURL:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?radius=%d&sensor=false&key=%@",resRad, kGoogleApiKey]];
	}
	else
	{
		[self setBaseURL:[NSString stringWithString:@"http://local.yahooapis.com/LocalSearchService/V3/localSearch?appid=Ywx65dbV34G0z1IzsP.WyaFOTkoBytEzMKKmS_61tnK8BoWEtuFiEwx_iIhkKqtJ6ubN&results=20&output=json"]];
	}
	str = baseURL;
	return str;
}

- (void) setBaseURL:(NSString *)newURL
{
	if (baseURL != newURL)
	{
		[baseURL release];
		baseURL = [newURL copy];
	}
}

#pragma mark - Helpers
- (BOOL) isStatusError:(NSString *)statusStr
{
	BOOL isErr = NO;
	if ( statusStr == nil || [statusStr length] <= 0 || ![statusStr isEqualToString:@"OK"])
	{
		isErr = YES;
	}
	return isErr;
}

- (void) showActivityWithMessage:(NSString *)activityStr onViewController:(UIViewController *)vc withSpinner:(BOOL) shouldShowSpinner
{
	CGRect loadingRect = CGRectZero;
	UILabel *lbl = [[UILabel alloc] initWithFrame:loadingRect];
	[UIView beginAnimations:@"Activity" context:nil];
	[UIView setAnimationDuration:01.0];
	
	loadingRect.size.width = vc.view.frame.size.width;
	loadingRect.size.height = 14.0f;
	
	lbl.frame = loadingRect;
	lbl.text = activityStr;
	lbl.textColor = [UIColor blueColor];
	lbl.backgroundColor = [UIColor lightGrayColor];
	lbl.textAlignment = UITextAlignmentCenter;
	lbl.font = [UIFont systemFontOfSize:12.0f];
	lbl.tag = 120001;
	lbl.alpha = 0.8f;
	
	if (shouldShowSpinner)
	{
		UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc] 
													 initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		loadingIndicator.frame = CGRectMake(vc.view.frame.size.width - 20,0, 20, 12);
		loadingIndicator.tag = 120002;
		[loadingIndicator startAnimating];
		[lbl addSubview:loadingIndicator];
	}
	
	[vc.view addSubview:lbl];
	ObjCRelease(lbl);
	[UIView commitAnimations];
}

- (void) updateOrCreateActivityWithMessage:(NSString *)newActivityStr onViewController:(UIViewController *)vc withSpinner:(BOOL) shouldShowSpinner
{
	UILabel *lbl = (UILabel *)[vc.view viewWithTag:120001];
	if (lbl == nil)
	{
		[self showActivityWithMessage:newActivityStr onViewController:vc withSpinner:shouldShowSpinner];
	}
	else
	{
		if (shouldShowSpinner)
		{
			UIActivityIndicatorView *acv = (UIActivityIndicatorView *)[lbl viewWithTag:120002];
			if (acv == nil)
			{
				UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc] 
															initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
				loadingIndicator.frame = CGRectMake(vc.view.frame.size.width - 20,0, 20, 12);
				loadingIndicator.tag = 120002;
				[loadingIndicator startAnimating];
				[lbl addSubview:loadingIndicator];
			}
		}
		else
		{
			UIActivityIndicatorView *acv = (UIActivityIndicatorView *)[lbl viewWithTag:120002];
			[acv stopAnimating];
			[acv removeFromSuperview];
		}
		lbl.text = newActivityStr;
	}
}

- (void) hideActivityOnViewController:(UIViewController *)vc
{
	[UIView beginAnimations:@"Activity" context:nil];
	[UIView setAnimationDuration:01.0];
	UILabel *lbl = (UILabel *)[vc.view viewWithTag:120001];
	UIActivityIndicatorView *acv = (UIActivityIndicatorView *)[lbl viewWithTag:120002];
	[acv stopAnimating];
	acv.frame = CGRectZero;
	lbl.frame = CGRectZero;
	[UIView commitAnimations];
	[acv removeFromSuperview];
	[lbl removeFromSuperview];
}

- (void) setIsGoogle:(BOOL)newIsGoogle
{
	isGoogle = newIsGoogle;
	NSInteger resRad = [[[Settings sharedSettings] resultSetRadius] intValue];
	if (isGoogle)
	{
		[self setBaseURL:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?radius=%d&sensor=false&key=%@",resRad, kGoogleApiKey]];
	}
	else
	{
		[self setBaseURL:[NSString stringWithString:@"http://local.yahooapis.com/LocalSearchService/V3/localSearch?appid=Ywx65dbV34G0z1IzsP.WyaFOTkoBytEzMKKmS_61tnK8BoWEtuFiEwx_iIhkKqtJ6ubN&results=20&output=json"]];
	}
}

- (void) dynamicallyResizeLabel:(UILabel *)lbl withMaxSize:(CGSize) maxSize
{
	CGSize maximumLabelSize = maxSize;
	
	CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font 
									constrainedToSize:maximumLabelSize 
										lineBreakMode:lbl.lineBreakMode];
	CGRect newFrame = lbl.frame;
	newFrame.size.height = expectedLabelSize.height;
	lbl.frame = newFrame;
}

- (NSString *) searchURLForLocationWithLatitude:(double) lat logitude:(double) lng andName:(NSString *) name
{
	NSString *requestStr = nil;	
	if (self.isGoogle)
	{
		requestStr = [NSString stringWithFormat:@"%@&location=%lf,%lf&name=%@",[self baseURL],lat,lng,name];
	}
	else
	{
		requestStr = [NSString stringWithFormat:@"%@&latitude=%lf&longitude=%lf&query=%@",[self baseURL],lat,lng,name];
	}
	return requestStr;
}

- (void) updateSearchURL:(NSString *)searchURLStr withName:(NSString *) name
{
	NSMutableString *requestStrUpd = nil;
	NSArray *subStrArr = nil;
	
	if (self.isGoogle)
	{
		subStrArr = [searchURLStr componentsSeparatedByString:@"&name="];
		requestStrUpd = [NSMutableString stringWithString:[subStrArr objectAtIndex:0]];
		[requestStrUpd appendFormat:@"&name=%@",name];
		searchURLStr = [requestStrUpd copy];
	}
	else
	{
		subStrArr = [requestStrUpd componentsSeparatedByString:@"&query="];
		requestStrUpd = [NSMutableString stringWithString:[subStrArr objectAtIndex:0]];
		[requestStrUpd appendFormat:@"&query=%@",name];
		searchURLStr = [requestStrUpd copy];
	}
}

- (NSString *) updateSearchURL:(NSString *)searchURLStr withSearchRadius:(NSInteger) searchRadius andName:(NSString *)name
{
	NSMutableString *requestStrUpd = nil;
	NSArray *subStrArr = nil;
	
    subStrArr = [searchURLStr componentsSeparatedByString:@"radius="];
	requestStrUpd = [NSMutableString stringWithString:[subStrArr objectAtIndex:0]];
	[requestStrUpd appendFormat:@"radius=%d&sensor=false&key=%@&location=%lf,%lf&name=%@", searchRadius, kGoogleApiKey ,self.currentLat, self.currentLng, name];
    
    return [requestStrUpd copy];
}



#pragma mark - Application flow

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#ifdef ADWHIRL_DEBUG
	AWLogSetLogLevel(AWLogLevelDebug);
#endif
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"searches.db"];
	if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        NSError *error = nil;
		[[NSFileManager defaultManager] copyItemAtPath:DB_PATH
												toPath:folderPath
												 error:&error];
	}
	
	SQLiteManager *tempMgr = [[SQLiteManager alloc] initWithDB:folderPath];
	self.mrMgr = tempMgr;
	ObjCRelease(tempMgr);
    
    NSString * pathForBgImg = [[NSBundle mainBundle] pathForResource:@"carepointsBlackBG" ofType:@"png"];
    UIImage * bgImg = [[UIImage alloc] initWithContentsOfFile:pathForBgImg];
    UIImageView * bgImgView = [[UIImageView alloc] initWithImage:bgImg];
    ObjCRelease(bgImg);
    [bgImgView setFrame:[UIScreen mainScreen].bounds];
    [self.window insertSubview:bgImgView atIndex:0];
	
	self.isGoogle = YES;
	[self _showSplash];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	/*
	 Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	 Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	 */
	LocationManager *locMgr = [LocationManager sharedSingleton];
	[locMgr stopLocating];
	locMgr.delegate = nil;
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	/*
	 Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	 If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	 */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	/*
	 Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	 */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	/*
	 Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	 */
	LocationManager *locMgr = [LocationManager sharedSingleton];
	locMgr.delegate = self;
	gotLoc = NO;
	UILabel *splashLbl = (UILabel *)[self.window viewWithTag:k_LOADING_LABEL_TAG];	
	if (splashLbl != nil)
	{
		CGRect lblRect = splashLbl.frame;
		lblRect.origin.x = 10;
		lblRect.size.width = 300;
		lblRect.size.height = 80;
		splashLbl.frame = lblRect;
		UIActivityIndicatorView *loadingIndicator = (UIActivityIndicatorView *) [self.window viewWithTag:k_LOADING_INDICATOR_TAG];
		
		[loadingIndicator stopAnimating];
		[loadingIndicator removeFromSuperview];
		loadingIndicator = nil;
		splashLbl.text = @"Loading...";
	}
	[[Settings sharedSettings] updateSettings];
	[locMgr updateDistanceFilter];
	if([self deleteAllCaches])
	{
		SafeLog(@"Cache deletion successful");
	}
	else
	{
		SafeLog(@"Cache deletion unsuccessful");
	}
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	/*
	 Called when the application is about to terminate.
	 Save data if appropriate.
	 See also applicationDidEnterBackground:.
	 */
}

@end
